<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CreateTicketAttachments extends Model
{
    
    protected $fillable = ['ticket_id', 'filename'];

    public function CreateTicket(){
        return $this->belongsTo('App\Models\CreateTicket');
    }
}


