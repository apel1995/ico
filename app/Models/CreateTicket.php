<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CreateTicket extends Model
{
    
    protected $fillable = ['customer', 'category', 'subject', 'Description'];
}
