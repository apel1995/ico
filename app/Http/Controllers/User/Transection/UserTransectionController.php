<?php

namespace App\Http\Controllers\User\Transection;

use App\Http\Controllers\Controller;
use App\Http\Middleware\User;
use App\Models\Token;
use App\Models\Transaction;
use App\Models\User as ModelsUser;
use Illuminate\Http\Request;

class UserTransectionController extends Controller
{
    public function report(Request $request){
        $op = $request->input('op');

        if ($op == "data_table") {
            return $this->transReportDT($request);
        } else if ($op == "export") {
            return $this->transReportExp($request);
        }
        return view('pages.user.report.transection_report');
    }

    public function transReportDT($request){
        $draw = $request->input('draw');
        $start = $request->input('start');
        $length = $request->input('length');
        $search =  $_GET['search']['value'];
        $order = $_GET['order'][0]["column"];
        $orderDir = $_GET["order"][0]["dir"];

        $columns = ['price', 'txid', 'type', 'status','created_at'];
        $orderby = $columns[$order];
        $result = Transaction::select('*');
        $price = $request->input('price');
        $txid = $request->input('txid');
        $type = $request->input('type');
        $status = $request->input('status');
        $from = $request->input('from');
        $to = $request->input('to');
        if ($price != "") {
            $result = $result->where('price', '=', $price);
        }

        if ($txid != "") {
            $result = $result->where('txid', '=', $txid);
        }

        if ($type != "") {
            $result = $result->where('type', '=', $type);
        }

        if ($status != "") {
            $result = $result->where('status', '=', $status);
        }
        if ($from != "") {
            $result = $result->whereDate("created_at", '>=', $from);
        }
        if ($to != "") {
            $result = $result->whereDate("created_at", '<=', $to);
        }
        // filter search script end

        if ($search != "") {
            $result = $result
                ->where('price', 'LIKE', '%' . $search . '%')
                ->orwhere('txid', 'LIKE', '%' . $search . '%')
                ->orwhere('type', 'LIKE', '%' . $search . '%')
                ->orwhere('price', 'LIKE', '%' . $search . '%')
                ->orwhere('status', 'LIKE', '%' . $search . '%')
                ->orWhere('created_at', 'LIKE', '%' . $search . '%');
               
                

        }

        $count = $result->count();
        $result = $result->orderby($orderby, $orderDir)->skip($start)->take($length)->get();
        $data = array();
        $i = 0;
        
 
      
        foreach ($result as $m_user) {
            $extra = '<b>From Address</b>: '.$m_user->from.'<br/><b>To Address</b>: '.$m_user->to;
            $user = ModelsUser::where('id', '=', $m_user->user_id)->first();
            $eturl="";
            $btcurl="";
            if($m_user->type == 'et'){
                $eturl="https://etherscan.io/tx/".$m_user->txid;
            }
            else if($m_user->type == 'buy'){
                $eturl="https://blockchair.com/litecoin/transaction/".$m_user->txid;
            }
            
            $data[$i]['DT_RowId'] = "row_" . $m_user->id;
            $data[$i]['name']   = $user->name;
            $data[$i]['email']   = $user->email;
            $data[$i]['price']   = $m_user->price;
            $data[$i]['txid'] = $m_user->txid;
            $data[$i]['type'] = $m_user->type;
            $data[$i]['status'] = $m_user->status;
            $data[$i]['created_at'] = $m_user->created_at;
            $data[$i]["extra"] 			= $extra;
            $data[$i]["eturl"] 			= $eturl;
            $data[$i]["btcurl"] 			= $btcurl;

            $i++;
        }

        $res['draw'] = $draw;
        $res['recordsTotal'] = $count;
        $res['recordsFiltered'] = $count;
        $res['data'] = $data;

        return json_encode($res);

    }


    public function transReportExp($request)
    {
        header("Content-Type: text/csv; charset=utf-8");
        header("Content-Disposition: attachment; filename=Btc-txn-report-" . time() . ".csv");

        $result = Transaction::select('*');

        $txid = $request->input('txid');
        $type = $request->input('type');
        $price = $request->input('price');
        $from = $request->input('from');
        $to = $request->input('to');
        if ($price != "") {
            $result = $result->where('price', '=', $price);
        }

        if ($txid != "") {
            $result = $result->where('txid', '=', $txid);
        }

        if ($type != "") {
            $result = $result->where('type', '=', $type);
        }
        if ($from != "") {
            $result = $result->whereDate("created_at", '>=', $from);
        }
        if ($to != "") {
            $result = $result->whereDate("created_at", '<=', $to);
        }
        $result = $result->get();

        $data = array();

        $i = 0;


        $output = fopen("php://output", "w");
        fputcsv($output, ['Transaction Hash', 'Transaction Address In', 'Transaction Address Out', 'Broadcast', 'Blockchain', 'Profit', 'Status', 'Ammount BTC', 'Ammount USD', 'IP Address', 'Created AT']);


        foreach ($result as $m_user) {
            $user = ModelsUser::where('id', '=', $m_user->user_id)->first();

            $data[$i]['DT_RowId'] = "row_" . $m_user->id;
            $data[$i]['name']   = $user->name;
            $data[$i]['email']   = $user->email;
            $data[$i]['txid'] = $m_user->txid;
            $data[$i]['type'] = $m_user->type;
            $data[$i]['price'] = $m_user->price;
            $data[$i]['created_at'] = $m_user->created_at;
            fputcsv($output, $data[$i]);
        }

        fclose($output);
    }

}
