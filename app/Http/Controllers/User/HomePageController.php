<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class HomePageController extends Controller
{
    //index function start
    public function index(){
        return view('pages.user.home');
    }
    //index function ends
}
