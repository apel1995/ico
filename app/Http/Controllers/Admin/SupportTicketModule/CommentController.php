<?php

namespace App\Http\Controllers\Admin\SupportTicketModule;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Comment;
use App\Models\CreateTicket;
use Laravel\Ui\Presets\React;

class CommentController extends Controller
{
    public function replyStore(Request $request)
    {
       
        //$u_id = $request->all();
        $text = $request->input('cmt');
        $user_id = auth('admin')->id();
        $parent_id = $request->route('id');
        $user_name = auth('admin')->user()->name;
        // $data=array('user_id'=>$user_id,"parent_id"=>$parent_id,"comment"=>$text);
        // DB::table('comments')->insert($data);
        $comment = new Comment;
        $comment->user_id = $user_id;
        $comment->parent_id = $parent_id;
        $comment->comment = $text;
        $comment->user_name = $user_name;
        $comment->save();
        return redirect()->back()->with('status','Replay Added Successfully');

        // dd($request->route('id'));
        // dd($parent_id);

    }

    public function comment_data_update(Request $request){
        $cmt_id =$request->input('usr_id_modify');
        $modify_cmt = $request -> input('cmt_modify');
        // dd($cmt_id);
        Comment::where('id', $cmt_id)->update(array('comment' => $modify_cmt));
        
        return redirect()->back()->with('status','Replay Update Successfully');

    }

    public function comment_data_delete(Request $request){
        DB::delete('delete from comments where id = ?',[$request->route('id')]);
    //   dd($request->route('id'));
        return redirect()->back()->with('status','Delete Successfully');

    }

    public function support_ticket_delete(Request $request){
        DB::delete('delete from create_ticket_attachments where ticket_id = ?',[$request->route('id')]);
        DB::delete('delete from create_tickets where id = ?',[$request->route('id')]);
       
        //echo($request->route('id'));
        // return response()->json(['message' => 'Password Reset Successful']); 
    }

    public function support_ticket_assign(Request $request){
        $get_status =$request->input('status');
        $get_agent =$request->input('agent_name');
        $get_category = $request->input('category_name');
        $id = $request->route('id');
        CreateTicket::where('id', $id)->update(array('category' => $get_category));
        CreateTicket::where('id', $id)->update(array('agent_name' => $get_agent));
        CreateTicket::where('id', $id)->update(array('status' => $get_status));
        
        return redirect()->back()->with('status','New Assign Sucessfully');

    }

        

    
}
