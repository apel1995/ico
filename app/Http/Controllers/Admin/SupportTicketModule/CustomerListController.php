<?php

namespace App\Http\Controllers\Admin\SupportTicketModule;
use App\Models\CreateTicket;
use App\Models\Admin;
use App\Models\Comment;
use App\Models\CreateTicketAttachments;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CustomerListController extends Controller
{
    public function customer(){
        return view('pages/admin/modules/support_ticket/help-customer')
        ->with('create_tickets', CreateTicket::all());
    }

    public function fatch_customer_details($id){
        if(CreateTicket::where('id', $id)->exists()){
            $details = CreateTicket::where('id', $id)->first();
            $cmt_info = Comment::where('parent_id', $id)->get();
            $attachments = CreateTicketAttachments::select('filename')->where('ticket_id', $id)->get();
            $support_agent = Admin::orderBy('id', 'DESC')->get();
            $categories = CreateTicket::distinct()->get(['category']);
            // dd($attachments);
            return view('pages/admin/modules/support_ticket/help-ticket-details', compact('details', 'cmt_info', 'attachments', 'support_agent', 'categories'));
        }
        else{
            return redirect('/')->with('status', "The link was broken");
        }
    }
}
