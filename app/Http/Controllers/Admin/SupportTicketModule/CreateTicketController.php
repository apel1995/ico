<?php

namespace App\Http\Controllers\Admin\SupportTicketModule;

use App\Models\CreateTicket;
use App\Models\CreateTicketAttachments;

use App\Http\Controllers\Controller;
use App\Models\Admin;
use App\Models\Comment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Pagination\Paginator;

class CreateTicketController extends Controller
{
    public function uploadForm()
    {
        return view('pages/admin/modules/support_ticket/help-create-ticket');
    }

    // public function getCategory(){

    //     $categories = DB::table('create_tickets')->select('category')->distinct()->get();
    //     return view('pages/admin/modules/support_ticket/help-ticket', compact('categories'));
               
    // }

    // public function getPost(){
    //     return view('pages/admin/modules/support_ticket/help-ticket')
    //         ->with('create_tickets', CreateTicket::all());
    // }

    

    public function ticketList(Request $request){
        Paginator::useBootstrap();
        $search =  $request->input('q');
        if($search!=""){
            $create_tickets = CreateTicket::where(function ($query) use ($search){
                $query->where('customer', 'like', '%'.$search.'%')
                    ->orWhere('email', 'like', '%'.$search.'%');
            })
            ->paginate(5);
            $create_tickets->appends(['q' => $search]);
            $categories = CreateTicket::distinct()->get(['category']);
            $support_agent = Admin::orderBy('id', 'DESC')->get();
            $total_comment = Comment::where('parent_id', 'id')->count();
        }
        else{
            $create_tickets = CreateTicket::orderBy('id', 'DESC')->paginate(8);
            $categories = CreateTicket::distinct()->get(['category']);
            $support_agent = Admin::orderBy('id', 'DESC')->get();
            $total_comment = Comment::where('parent_id', 'id')->count();
        
            return view('pages/admin/modules/support_ticket/help-ticket', compact('create_tickets', 'categories', 'support_agent', 'total_comment'));

        }
        return view('pages/admin/modules/support_ticket/help-ticket', compact('create_tickets', 'categories', 'support_agent', 'total_comment'));



        
    }

    public function support_ticket_delete(Request $request){
        DB::delete('delete from create_tickets where id = ?',[$request->route('id')]);
        //echo($request->route('id'));
        // return response()->json(['message' => 'Password Reset Successful']); 
    }

    public function uploadSubmit(Request $request)
    {
        $this->validate($request, [
            'photos' => 'required',
            'customer' => 'required',
            'category' => 'required',
            'subject' => 'required',
            'Description' => 'required',
            'photos.*' => 'mimes:pdf,jpg,png'
        ]);
        if ($request->hasFile('photos')) {
            $allowedfileExtension = ['pdf', 'jpg', 'png'];
            $files = $request->file('photos');
            foreach ($files as $file) {
                $filename[] = $file->getClientOriginalName();
                $extension = $file->getClientOriginalExtension();
                $check = in_array($extension, $allowedfileExtension);
            }

            if ($check) {
                $items = CreateTicket::create($request->all());


                foreach ($request->photos as $photo) {

                    $name = $photo->getClientOriginalName();
                    $photo->move(public_path().'/files/', $name);
                    $filename[] = $name;
                }

                $file = new CreateTicketAttachments();
                $file->ticket_id = $items->id;
                $file->filename = json_encode($filename);
                $file->save();


                return back()->with('success', 'Data Your files has been successfully added');
            } else {
                echo '<div class="alert alert-warning"><strong>Warning!</strong> Sorry Only Upload png , jpg </div>';
            }
        }
    }

    // public function uploadSubmit(Request $request){
    //     $this->validate($request, [
    //                 'photos'=>'required',
    //                 'customer'=>'required',
    //                 'category'=>'required',
    //                 'subject' => 'required',
    //                 'Description' => 'required'
    //                 ]);

    //     $items= CreateTicket::create($request->all());

    //     if($request->hasfile('photos'))
    //     {
    //         foreach($request->photos as $photo){
    //             $name = time().'.'.$photo->extension();
    //             $data[] = $name;
    //         }
    //     }
    //     dd($data);
    //     $file= new CreateTicketAttachments();
    //     $file->ticket_id = $items->id;
    //     $file->filename=json_encode($data);
    //     // dd($file);
    //     $file->save();
    //     return back()->with('success', 'Data Your files has been successfully added');
    // }
}
