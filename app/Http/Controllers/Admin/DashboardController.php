<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    //index function start
    public function index()
    {
        
        if( auth('admin')->check() ){          

            return view('pages.admin.dashboard');
        }else{
            return view("errors.404");
        }
    }

    //index function ends

}
