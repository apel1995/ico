<?php

namespace App\Http\Controllers\admin\TransactionModule;

use App\Http\Controllers\Controller;
use App\Models\Transaction;
use Illuminate\Http\Request;

class TransectionController extends Controller
{
    public function transReport(Request $request){
        $op = $request->input('op');

        if ($op == "data_table") {
            return $this->transReportDT($request);
        } else if ($op == "export") {
            return $this->transReportExp($request);
        }
        //  return view('admin.report.transection_report');
         return view('pages.admin.modules.transection_module.transection_report');
    }

    public function transReportDT($request){
        $draw = $request->input('draw');
        $start = $request->input('start');
        $length = $request->input('length');
        $search =  $_GET['search']['value'];
        $order = $_GET['order'][0]["column"];
        $orderDir = $_GET["order"][0]["dir"];

        $columns = ['price', 'txid', 'type', 'status','created_at'];
        $orderby = $columns[$order];
        $result = Transaction::select('*');
        $price = $request->input('price');
        $txid = $request->input('txid');
        $type = $request->input('type');
        $status = $request->input('status');
        $from = $request->input('from');
        $to = $request->input('to');
        if ($price != "") {
            $result = $result->where('price', '=', $price);
        }

        if ($txid != "") {
            $result = $result->where('txid', '=', $txid);
        }

        if ($type != "") {
            $result = $result->where('type', '=', $type);
        }

        if ($status != "") {
            $result = $result->where('status', '=', $status);
        }
        if ($from != "") {
            $result = $result->whereDate("created_at", '>=', $from);
        }
        if ($to != "") {
            $result = $result->whereDate("created_at", '<=', $to);
        }
        // filter search script end

        if ($search != "") {
            $result = $result
                ->where('price', 'LIKE', '%' . $search . '%')
                ->orwhere('txid', 'LIKE', '%' . $search . '%')
                ->orwhere('type', 'LIKE', '%' . $search . '%')
                ->orwhere('price', 'LIKE', '%' . $search . '%')
                ->orWhere('created_at', 'LIKE', '%' . $search . '%');
        }

        $count = $result->count();
        $result = $result->orderby($orderby, $orderDir)->skip($start)->take($length)->get();
        $data = array();
        $i = 0;

        foreach ($result as $m_user) {
            // $extra = 'Name: '.$m_user->user.'<br/>Method: '.$m_user->method.'<br/>Transaction ID: '.$m_user->txn_hash;

            $data[$i]['DT_RowId'] = "row_" . $m_user->id;
            $data[$i]['price']   = $m_user->price;
            $data[$i]['txid'] = $m_user->txid;
            $data[$i]['type'] = $m_user->type;
            $data[$i]['status'] = $m_user->status;
            $data[$i]['created_at'] = $m_user->created_at;
            // $data[$i]["extra"] 			= $extra;
            $i++;
        }

        $res['draw'] = $draw;
        $res['recordsTotal'] = $count;
        $res['recordsFiltered'] = $count;
        $res['data'] = $data;

        return json_encode($res);

    }
}
