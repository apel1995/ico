<?php

namespace App\Http\Controllers\Admin\KycModule;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Exception;
use App\Models\KycRequest;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

class KycRequestController extends Controller
{
    public function kycReport(Request $request)
    {  
        // $result =DB::table('kyc_requests')->get();
        // dd($result);
        $op = $request->input('op');
        
        if ($op == "data_table") {
            return $this->kycReportDT($request);
        } else if ($op == "export") {
            return $this->btcTxnReportExp($request);
        }


        
        return view('pages.admin.modules.kyc_report_module.kyc_request');
    }

    public function kycReportDT($request)
    {
        $draw = $request->input('draw');
        $start = $request->input('start');
        $length = $request->input('length');
        $search =  $_GET['search']['value'];
        $order = $_GET['order'][0]["column"];
        $orderDir = $_GET["order"][0]["dir"];

        $columns = ['client_name', 'client_type', 'doc_type', 'issu_date','exp_date', 'status', 'created_at', 'email'];
        $orderby = $columns[$order];
        $result = KycRequest::select('*');
        // $result =DB::table('kyc_requests')->get();
        $client_name = $request->input('client_name');
        $email = $request->input('email');
        $client_type = $request->input('client_type');
        $doc_type = $request->input('doc_type');
        $issu_date_srt = $request->input('issu_date_srt');
        $issu_date_end = $request->input('issu_date_end');

        $exp_date = $request->input('exp_date');
        $status = $request->input('status');
        $created_at = $request->input('created_at');
        if ($client_name != "") {
            $result = $result->where('client_name', '=', $client_name)->orwhere('email', '=', $client_name); //name and email
        }

        if ($client_type != "") {
            $result = $result->where('client_type', '=', $client_type);
        }

        if ($doc_type != "") {
            $result = $result->where('doc_type', '=', $doc_type);
        }

        if ($issu_date_srt != "") {
            $result = $result->whereDate('issu_date', '>=', $issu_date_srt);
        }
        if($issu_date_end != ""){
                 $result = $result->whereDate('issu_date', '<=', $issu_date_end);
        }
        if ($exp_date != "") {
            $result = $result->whereDate("exp_date", '<=', $exp_date);
        }
        if ($status != "") {
            $result = $result->where("status", '=', $status);
        }
        if ($created_at != "") {
            $result = $result->where("created_at", '=', $created_at);
        }
        if ($email != ""){
            $result = $result->where('email', '=', $email);
        }
        
        // filter search script end
        // data-id='".$m_user->id."'
        if ($search != "") {
            $result = $result
                ->where('client_name', 'LIKE', '%' . $search . '%')
                ->orwhere('client_type', 'LIKE', '%' . $search . '%')
                ->orwhere('doc_type', 'LIKE', '%' . $search . '%')
                ->orwhere('issu_date', 'LIKE', '%' . $search . '%')
                ->orwhere('exp_date', 'LIKE', '%' . $search . '%')
                ->orwhere('status', 'LIKE', '%' . $search . '%')
                ->orWhere('email', 'LIKE', '%' . $search . '%')
                ->orWhere('created_at', 'LIKE', '%' . $search . '%');
        }

        $count = $result->count();
        $result = $result->orderby($orderby, $orderDir)->skip($start)->take($length)->get();
        $data = array();
        $i = 0;
// Rony Bhai
// '<button id = "edit_button" data-json = "'.$row_json.'" class = "prettyclass" onclick="update_profile_oc(this.client_name)">Edit</button>';
// 
        foreach ($result as $m_user) {
            $row_json = htmlspecialchars(json_encode($m_user->client_name));
            $data[$i]['DT_RowId'] = "row_" . $m_user->id;
            $data[$i]['client_name']   = $m_user->client_name;
            $data[$i]['client_type'] = $m_user->client_type;
            $data[$i]['doc_type'] = $m_user->doc_type;
            $data[$i]['issu_date'] = $m_user->issu_date;
            $data[$i]['exp_date'] = $m_user->exp_date;
            $data[$i]['status'] = $m_user->status;
            $data[$i]['created_at'] = $m_user->created_at;
            $data[$i]['action'] = '  <button style="margin-bottom: 5px;"   data-id="'.((isset($m_user->id)) ? $m_user->id : 'ID Not Found').'" data-images="'.((isset($m_user->images)) ? $m_user->images : 'Document Not Found').'"  data-doc_type="'.((isset($m_user->doc_type)) ? $m_user->doc_type : 'Document Type Not Found').'"  data-email="'.((isset($m_user->email)) ? $m_user->email : 'Name Not Found').'" data-country="'.((isset($m_user->country)) ? $m_user->country : 'country Not Found').'"  data-address="'.((isset($m_user->address)) ? $m_user->address : 'Address Not Found').'"  data-city="'.((isset($m_user->city)) ? $m_user->city : 'City Not Found').'"  data-state="'.((isset($m_user->state)) ? $m_user->state : 'State Not Found').'" data-phone="'.((isset($m_user->phone)) ? $m_user->phone : 'Phone Not Found').'"  data-zip="'.((isset($m_user->zip)) ? $m_user->zip : 'Zip Not Found').'"  data-bithday="'.((isset($m_user->bithday)) ? $m_user->bithday : 'Bithday Not Found').'"  data-status="'.((isset($m_user->status)) ? $m_user->status : 'Status Not Found').'"  data-issu_date="'.((isset($m_user->issu_date)) ? $m_user->issu_date : 'issue date Not Found').'"  data-exp_date="'.((isset($m_user->exp_date)) ? $m_user->exp_date : 'Exp date Not Found').'"  data-email="'.((isset($m_user->email)) ? $m_user->phone : 'Phone Not Found').'"  data-client_type="'.((isset($m_user->client_type)) ? $m_user->client_type : 'Name Not Found').'"  data-client_name="'.((isset($m_user->client_name)) ? $m_user->client_name : 'Name Not Found').'"  onclick="user_details_view(this)" class="btn btn-sm btn-primary"  >View </button>
                                     <button style="margin-bottom: 5px;"   data-id="'.((isset($m_user->id)) ? $m_user->id : 'ID Not Found').'"  data-images="'.((isset($m_user->images)) ? $m_user->images : 'Document Not Found').'"  data-doc_type="'.((isset($m_user->doc_type)) ? $m_user->doc_type : 'Document Type Not Found').'"  data-email="'.((isset($m_user->email)) ? $m_user->email : 'Name Not Found').'" data-country="'.((isset($m_user->country)) ? $m_user->country : 'country Not Found').'"  data-address="'.((isset($m_user->address)) ? $m_user->address : 'Address Not Found').'"  data-city="'.((isset($m_user->city)) ? $m_user->city : 'City Not Found').'"  data-state="'.((isset($m_user->state)) ? $m_user->state : 'State Not Found').'" data-phone="'.((isset($m_user->phone)) ? $m_user->phone : 'Phone Not Found').'"  data-zip="'.((isset($m_user->zip)) ? $m_user->zip : 'Zip Not Found').'"  data-bithday="'.((isset($m_user->bithday)) ? $m_user->bithday : 'Bithday Not Found').'"  data-status="'.((isset($m_user->status)) ? $m_user->status : 'Status Not Found').'"  data-issu_date="'.((isset($m_user->issu_date)) ? $m_user->issu_date : 'issue date Not Found').'"  data-exp_date="'.((isset($m_user->exp_date)) ? $m_user->exp_date : 'Exp date Not Found').'"  data-email="'.((isset($m_user->email)) ? $m_user->phone : 'Phone Not Found').'"  data-client_type="'.((isset($m_user->client_type)) ? $m_user->client_type : 'Name Not Found').'"  data-client_name="'.((isset($m_user->client_name)) ? $m_user->client_name : 'Name Not Found').'"  onclick="update_profile(this)" class="btn btn-sm btn-info"  > Edit </button>';
            $i++;
        }

        $res['draw'] = $draw;
        $res['recordsTotal'] = $count;
        $res['recordsFiltered'] = $count;
        $res['data'] = $data;

        return json_encode($res);
    }

    public function user_data_update(Request  $request){     
        
     //   dd($id);
        $validator =  Validator::make($request->all(), [
           "up_name"           => "required",
           "up_country"           => "required",
           "up_zip"           => "required",
           "up_state"           => "required",
           "up_issu_date"           => "required",
           "up_exp_date"           => "required",
           "up_city"           => "required",
           "up_bithday"           => "required",
           "up_address"        =>  "required",
            
        ]);

        if($validator->fails()){
            return  response()->json(['errors' => $validator->errors()],422);
        }else{
                try {
                    $u_id = $request->post('user_id');
                
                    $user  = KycRequest::find($u_id);
                    $user->client_name  = $request->up_name;
                    $user->country  = $request->up_country;
                    $user->zip  = $request->up_zip;
                    $user->state  = $request->up_state;
                    $user->issu_date  = $request->up_issu_date;
                    $user->exp_date  = $request->up_exp_date;
                    $user->city  = $request->up_city;
                    $user->bithday  = $request->up_bithday;
                    $user->address  = $request->up_address;
            
            
                    if($user->save()){
                        return  response()->json(['success'  =>  'User Data Updated'],200);
                        
                    }
                }catch (Exception $e){
                    return response()->json(['error' => $e->getMessage()], 200);
              
                    }
        
         }
        
    }

   

    public function update_status_approve(Request $request){
        $u_id = $request->post('c_id');
        $user = KycRequest::find($u_id);
        
        $user->status = 'approved';
 

        if($user->save()){
            return  response()->json(['success'  =>  'true', 'message' => 'Kyc Approved'],200);
            
            
        }
    }

    public function update_status_decline(Request $request){
        $u_id = $request->post('c_d_id');
        $user = KycRequest::find($u_id);
        
        $user->status = 'decline';
 

        if($user->save()){
            return  response()->json(['success'  =>  'User Decline'],200);
            
        }
    }
}
