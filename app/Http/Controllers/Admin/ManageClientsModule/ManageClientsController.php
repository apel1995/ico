<?php

namespace App\Http\Controllers\Admin\ManageClientsModule;
use App\Http\Controllers\Controller;
use App\Mail\SendMail;
use App\Mail\ResetPassword;
use App\Mail\PasswordChange;
use App\Mail\TransactionPassReset;
use App\Mail\TransPinChange;
use App\Models\Comment;
use App\Models\User;
use Exception;
use Illuminate\Contracts\Session\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;



class ManageClientsController extends Controller
{
    public function manageClient(Request $request)
    {  
        $op = $request->input('op');

        if ($op == "data_table") {
            return $this->tokenReportDT($request);
        } else if ($op == "export") {
            return $this->tokenReportExp($request);
        }
      
        return view('pages.user.manage_client');
    }

    public function tokenReportDT($request){
        $draw = $request->input('draw');
        $start = $request->input('start');
        $length = $request->input('length');
        $search =  $_GET['search']['value'];
        $order = $_GET['order'][0]["column"];
        $orderDir = $_GET["order"][0]["dir"];

        $columns = ['name', 'email', 'phone', 'join_date','country'];
        $orderby = $columns[$order];
        $result = User::select('*');
        $name = $request->input('name');

        $join_date = $request->input('join_date');
        $from = $request->input('from');
        $to = $request->input('to');
        if ($name != "") {
            $result = $result->where('name', '=', $name)->orwhere('email', '=', $name)->orwhere('country', '=', $name)->orwhere('phone', '=', $name);
        }
        if ($join_date != "") {
            $result = $result->where('join_date', '=', $join_date);
        }
        if ($from != "") {
            $result = $result->whereDate("join_date", '>=', $from);
        }
        if ($to != "") {
            $result = $result->whereDate("join_date", '<=', $to);
        }
        // filter search script end

        if ($search != "") {
            $result = $result
                ->where('name', 'LIKE', '%' . $search . '%')
                ->orwhere('email', 'LIKE', '%' . $search . '%')
                ->orwhere('country', 'LIKE', '%' . $search . '%')
                ->orwhere('phone', 'LIKE', '%' . $search . '%')
                ->orWhere('join_date', 'LIKE', '%' . $search . '%');
        
        }

        $count = $result->count();
        $result = $result->orderby($orderby, $orderDir)->skip($start)->take($length)->get();
        $data = array();
        $i = 0;

        foreach ($result as $m_user) {
            $comments=DB::table('comments')
            ->where('user_id','=',$m_user->id)
            ->get('comment');
          
        
             $button='<div class="col-xl-8">
             <div class="nav-tabs-top mb-4">
                 <ul class="nav nav-tabs">
                     <li class="nav-item">
                         <a class="nav-link active" data-toggle="tab" href="#navs-top-home">Security Settings</a>
                     </li>
                     <li class="nav-item">
                         <a class="nav-link" data-toggle="tab" href="#navs-top-profile">Admin Comments</a>
                     </li>
                     <li class="nav-item">
                         <a class="nav-link" data-toggle="tab" href="#navs-top-messages">KYC</a>
                     </li>
                 </ul>
                 <div class="tab-content">
                     <div class="tab-pane fade show active" id="navs-top-home">
                         <div class="card-body">
                         <table width="100%">
                         <tr>
                           <td>
                                <label class="switch">
                                    <input type="checkbox">
                                    <span class="slider round"></span>
                                </label>
                            <strong>Block/Unblock</strong>
                            </td>
                         <td> <button   data-type="button"   class="btn btn-info btn-block ressetbtn"  data-loading="processing..."  data-id="'.((isset($m_user->id)) ? $m_user->id : 'ID Not Found').'" data-email="'.((isset($m_user->email)) ? $m_user->email : 'ID Not Found').'"   onclick="password_reset(this)">Password Reset</button></td>                         
                         </tr>

                         <tr>
                         <td>
                         <label class="switch">
                             <input type="checkbox">
                             <span class="slider round"></span>
                            </label>
                            <strong>Google 2 Step Authentication</strong>
                            </td>
                        <td> <button id="trans_pin" type="button" class="btn btn-info btn-block transpin_reset" data-loading="processing..." data-id="'.((isset($m_user->id)) ? $m_user->id : 'ID Not Found').'" onclick="transaction_pin_reset(this)">Transaction PIN Reset</button> </td>        
                          
                         </tr>
                         <tr>
                         <td>
                         <label class="switch">
                             <input type="checkbox">
                             <span class="slider round"></span>
                            </label>
                            <strong>Email Authentication</strong>
                            </td>
                            <td>
                         <button id="p_change" type="button"  class="btn btn-info btn-block" data-id="'.((isset($m_user->id)) ? $m_user->id : 'ID Not Found').'" data-toggle="modal" data-target="#client_password_update" onclick="client_password_changed_id(this)"  >Password Change</button> 
                         </td>    
                         
                         </tr>
                         <tr>
                         <td>
                         <label class="switch">
                             <input type="checkbox">
                             <span class="slider round"></span>
                            </label>
                            <strong>Email Verify</strong>
                            </td>
                        <td> <button id="pin_changed" type="button"  class="btn btn-info btn-block" data-id="'.((isset($m_user->id)) ? $m_user->id : 'ID Not Found').'" data-toggle="modal" data-target="#trans_pin_update" onclick="transaction_pin_changed(this)">Transaction PIN Change</button> </td>   
                         </tr>
                      
                       </table>
                         </div>
                     </div>
                     <div class="tab-pane fade" id="navs-top-profile">
                         <div class="card-body">
                            <div class="well" style="max-height: 300px;overflow: auto; text-align: left; ">
                            <a style=""  type="button" class="btn btn-sm btn-success"  data-toggle="modal" data-target="#comment-modal" onclick="admin_do_comment(this)"  data-id="'.((isset($m_user->id)) ? $m_user->id : 'ID Not Found').'"><i style="border-left: none !important;" class="glyphicon glyphicon-plus" ></i> + Add Comment</a>
                            <table class="table table-bordered">
                                <tbody>
                                    <tr>
                                        <td style="border-left: none !important;">Comment</td>
                                        <td>Action</td>
                                    </tr>
                                    <tr>
                                    <td>"'.$comments.'"<br></td>
                                    <td> 
                                    <button type="button" class="btn btn-danger commentDelete" data-id="'.((isset($m_user->id)) ? $m_user->id : 'ID Not Found').'" onclick="comment_delete(this)" >Delete</button></td>
                                    </tr>
                                </tbody>
                            </table>
                       </div>
                    </div>
                </div>
                     <div class="tab-pane fade" id="navs-top-messages">
                         <div class="card-body">
                             <p>know your clients here</p>
                         </div>
                     </div>
                 </div>
             </div>
         </div>';
         

            

         

             $extra='<div class="col-10">
             <div class="row">
                 <div class="col-md-2">
                     <button id="update"  type="submit" class="btn btn-info btn-block" data-id="'.((isset($m_user->id)) ? $m_user->id : 'ID Not Found').'" data-name="'.((isset($m_user->name)) ? $m_user->name : 'Name Not Found').'" data-post_code="'.((isset($m_user->post_code)) ? $m_user->post_code : 'Post Code Not Found').'"  data-email="'.((isset($m_user->email)) ? $m_user->email : 'Email Not Found').'" data-city="'.((isset($m_user->city)) ? $m_user->city : 'City Not Found').'" data-phone="'.((isset($m_user->phone)) ? $m_user->phone : 'Phone Not Found').'" data-country="'.((isset($m_user->country)) ? $m_user->country : 'Country Not Found').'" data-address="'.((isset($m_user->address)) ? $m_user->address : 'Address Not Found').'"  data-toggle="modal" data-target="#client-update" onclick="update_profile(this)" >Update Profile</button>
                 </div><br>
                 <div class="col-md-2">
                     <button id="finance" type="button" class="btn btn-info btn-block"   >Finance</button>
                 </div>
                 <div class="col-md-2">
                     <button id="desk_manager" type="button" class="btn btn-info btn-block">Assign To Desk M</button>
                 </div>
                 <div class="col-md-2">
                     <button id="acc_manager" type="button" class="btn btn-info btn-block">Assign To Account</button>
                 </div>
                 <div class="col-md-2">
                 <button id=""  type="submit" class="btn btn-info btn-block" data-id="'.((isset($m_user->id)) ? $m_user->id : 'ID Not Found').'"  data-email="'.((isset($m_user->email)) ? $m_user->email : 'Email Not Found').'"  data-toggle="modal" data-target="#send-mail" onclick="send_mail(this)" >Send Mail</button>
                </div>
              


             </div>
         </div>';
         
    
        
            $data[$i]['DT_RowId'] = "row_" . $m_user->id;
            $data[$i]['name']   = $m_user->name;
            $data[$i]['email'] = $m_user->email;
            $data[$i]['phone'] = $m_user->phone;
            $data[$i]['join_date'] = $m_user->join_date;
            $data[$i]['country'] = $m_user->country;
            $data[$i]["extra"] 			= $extra;
            $data[$i]["button"] 			= $button;
            $i++;
        }

        $res['draw'] = $draw;
        $res['recordsTotal'] = $count;
        $res['recordsFiltered'] = $count;
        $res['data'] = $data;

        return json_encode($res);

    }


    public function tokenReportExp($request)
    {
        header("Content-Type: text/csv; charset=utf-8");
        header("Content-Disposition: attachment; filename=Btc-txn-report-" . time() . ".csv");

        $result = User::select('*');

        $name = $request->input('name');
        $email = $request->input('email');
        $phone = $request->input('phone');
        $join_date = $request->input('join_date');
        $country = $request->input('country');
        $from = $request->input('from');
        $to = $request->input('to');
        if ($name != "") {
            $result = $result->where('name', '=', $name);
        }

        if ($email != "") {
            $result = $result->where('email', '=', $email);
        }

        if ($phone != "") {
            $result = $result->where('phone', '=', $phone);
        }

        if ($join_date != "") {
            $result = $result->where('join_date', '=', $join_date);
        }
        if ($country != "") {
            $result = $result->where('country', '=', $country);
        }

        if ($from != "") {
            $result = $result->whereDate("join_date", '>=', $from);
        }
        if ($to != "") {
            $result = $result->whereDate("join_date", '<=', $to);
        }
        $result = $result->get();

        $data = array();

        $i = 0;


        $output = fopen("php://output", "w");
        fputcsv($output, ['Name', 'Email', 'Phone','Join Date', 'Country']);


        foreach ($result as $m_user) {

            $data[$i]['DT_RowId'] = "row_" . $m_user->id;
            $data[$i]['name']   = $m_user->name;
            $data[$i]['email'] = $m_user->email;
            $data[$i]['phone'] = $m_user->phone;
            $data[$i]['join_date'] = $m_user->join_date;
            $data[$i]['country'] = $m_user->country;
            fputcsv($output, $data[$i]);
        }

        fclose($output);
    }

    public function user_profile_update(Request  $request){
        $validator =  Validator::make($request->all(), [
            "name"           => "required",
            "country"           => "required",
            "post_code"           => "required",
            "city"           => "required",
            "phone"           => "required",
            "address"           => "required",
            "email"           => "required",
             
         ]);


         if($validator->fails()){
            return  response()->json(['errors' => $validator->errors()],422);
        }else{
                try {
                    $u_id = $request->post('user_id');
                    $user  = User::find($u_id);
                    $user->name  = $request->name;
                    $user->country  = $request->country;
                    $user->post_code  = $request->post_code;
                    $user->city  = $request->city;
                    $user->phone  = $request->phone;
                    $user->address  = $request->address;
                    $user->email  = $request->email;
            
            
                    if($user->save()){
                        //return  response()->json(['success'  =>  'User Data Updated'],200);
                        return view('pages.user.manage_client');
                        
                    }
                }catch (Exception $e){
                    return response()->json(['error' => $e->getMessage()], 200);
              
                    }
                   
         }

         
    }

    public function send_mail(Request  $request){
        $validator =  Validator::make($request->all(), [
            "email_mail"           => "required",           
         ]);

         if($validator->fails()){
            return  response()->json(['errors' => $validator->errors()],422);
            }else{
                try {
                    $user_id = $request->post('user_id_update');
                    $userID  = User::find($user_id);
                    $userID->email  = $request->email_mail;
                    $result = DB::table('users')->get();
                    
                    foreach($result as $info){
                        $name=$info->name;
                        $email=$info->email;
                        $pass=$info->password;
                        $id=$info->id;
                         if( $user_id==$id){
                             $data = array(
                            'email'      => $email,
                            'password'   => $pass,
                            'name'       =>$name
                            );
                            //  dd($data);
                            Mail::to($request->email_mail )->send(new SendMail($data));                     
                         }
                    }
                    return view('pages.user.manage_client');                       
                    
                }catch (Exception $e){
                    return response()->json(['error' => $e->getMessage()], 200);
              
                    }
                   
         }
    }

    public function password_reset(Request $request){
        $res['success'] = false;
        $res['message'] = "Internal Error";
        $user_id  = $request->route('id');
        $pass = Str::random(8);
        $result = DB::table('users')->find( $user_id );
      
        if($result){
            $update= User::where('id', $user_id)->update(['password'=>$pass]);
            $data = array(
            'email'      => $result->email,
            'password'   =>  $pass,
            'name'       =>$result->name
            );
            Mail::to($result->email )->send(new ResetPassword($data));
            if ( $update ) {
                $res['success'] = true;
                $res['message'] = "Password Reset Successful";
            }                     
             
        }

       return response()->json($res);    

    }

    public function password_changed(Request $request){     
        $res['success'] = false;
        $res['message'] = "Internal Error";
        $user_id = $request->input('client_pass_change');
        $client_password = $request->input('client_password');
        $result = DB::table('users')->find( $user_id );
        if($result){
            $update = User::where('id', $user_id)->update(['password'=>$client_password]);
            $data = array(
            'email'      => $result->email,
            'password'   =>  $client_password,
            'name'       => $result->name
            );
            Mail::to( $result->email )->send(new PasswordChange($data));                     
            if ( $update ) {
                $res['success'] = true;
                $res['message'] = "Password updated successfully";
            }
        }

        return response()->json($res);    
    }

    public function transaction_pin_reset(Request $request){
        // return "test panel";
        $res['success']=false;
        $res['message']="Internal Error";
        $user_id=$request->route('id');
        $pin=rand(100000,999999);
        $result=DB::table('users')->find($user_id);

        if($result){
            $update=User::where('id',$user_id)->update(['t_pin'=>$pin]);
            $data=array(
                'email' =>$result->email,
                't_pin' =>$pin,
                'name' =>$result->name
            );

            Mail::to($result->email)->send(new TransactionPassReset($data));
            if($update){
                $res['success']=true;
                $res['message']="Transaction PIN Reset Successful";
            }
        }
        return response()->json($res);    
    }

    public function transaction_pin_change(Request $request){
        $res['success'] = false;
        $res['message'] = "Internal Error";
        $user_id = $request->input('transaction_pin');
        $client_pin = $request->input('transaction_pin_c');
        $result=DB::table('users')->find($user_id);
        if($result){
            $update=User::where('id',$user_id)->update(['t_pin'=>$client_pin]);
            $data = array(
                'email'      => $result->email,
                't_pin'   =>  $client_pin,
                'name'       => $result->name
                );
                Mail::to( $result->email )->send(new TransPinChange($data));                     
                if ( $update ) {
                    $res['success'] = true;
                    $res['message'] = "Transaction PIN updated successfully";
                }
        }
        return response()->json($res);  
       
    }


    public function do_comment(Request $request){
        $res['success'] = false;
        $res['message'] = "Internal Error";
        $admin_comment=$request->input('admin_comment');
        $user_id=$request->input('admin_comment_kor');
        $user_name = auth('admin')->user()->name;       
        $admin_id = auth('admin')->id();

        $comment_insert= Comment::create([
            'user_id'=>$user_id,
            'user_name'=>$user_name,
            'parent_id'=>$admin_id,
            'comment'=> $admin_comment,
       ]);

    //    $show_comment=Comment::where();

       if($comment_insert){
        $res['success'] = true;
        $res['message'] = "Comment Added successfully";
       }
       
       return response()->json($res);  
    }


    public function comment_delete(Request $request){
        $res['success'] = false;
        $res['message'] = "Internal Error";
        $user_id  = $request->route('id');
        $result = DB::table('comments')->where( 'user_id',$user_id)->delete();
        if($result){
            $res['success'] = true;
            $res['message'] = "Comment deleted successfully";
        }
        
        return response()->json($res); 
    }

}
