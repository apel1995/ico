<?php

namespace App\Http\Controllers\Admin\TokenPurchaseModule;

use App\Http\Controllers\Controller;
use App\Models\Token;
use Illuminate\Http\Request;

class TokenPurchaseController extends Controller
{
    public function tokenReport(Request $request)
    {  
        $op = $request->input('op');

        if ($op == "data_table") {
            return $this->tokenReportDT($request);
        } else if ($op == "export") {
            return $this->tokenReportExp($request);
        }
        return view('pages.admin.modules.token_report_module.token-report');
    }

    public function tokenReportDT($request)
    {
        $draw = $request->input('draw');
        $start = $request->input('start');
        $length = $request->input('length');
        $search =  $_GET['search']['value'];
        $order = $_GET['order'][0]["column"];
        $orderDir = $_GET["order"][0]["dir"];

        $columns = ['user', 'txn_hash', 'method', 'amount','created_at'];
        $orderby = $columns[$order];
        $result = Token::select('*');
        $user = $request->input('user');
        $txn_hash = $request->input('txn_hash');
        $method = $request->input('method');
        $amount = $request->input('amount');
        $from = $request->input('from');
        $to = $request->input('to');
        if ($user != "") {
            $result = $result->where('user', '=', $user);
        }

        if ($txn_hash != "") {
            $result = $result->where('txn_hash', '=', $txn_hash);
        }

        if ($method != "") {
            $result = $result->where('method', '=', $method);
        }

        if ($amount != "") {
            $result = $result->where('amount', '=', $amount);
        }
        if ($from != "") {
            $result = $result->whereDate("created_at", '>=', $from);
        }
        if ($to != "") {
            $result = $result->whereDate("created_at", '<=', $to);
        }
        // filter search script end

        if ($search != "") {
            $result = $result
                ->where('user', 'LIKE', '%' . $search . '%')
                ->orwhere('txn_hash', 'LIKE', '%' . $search . '%')
                ->orwhere('method', 'LIKE', '%' . $search . '%')
                ->orwhere('amount', 'LIKE', '%' . $search . '%')
                ->orWhere('created_at', 'LIKE', '%' . $search . '%');
        }

        $count = $result->count();
        $result = $result->orderby($orderby, $orderDir)->skip($start)->take($length)->get();
        $data = array();
        $i = 0;

        foreach ($result as $m_user) {
            // $extra = 'Name: '.$m_user->user.'<br/>Method: '.$m_user->method.'<br/>Transaction ID: '.$m_user->txn_hash;

            $data[$i]['DT_RowId'] = "row_" . $m_user->id;
            $data[$i]['user']   = $m_user->user;
            $data[$i]['txn_hash'] = $m_user->txn_hash;
            $data[$i]['method'] = $m_user->method;
            $data[$i]['amount'] = $m_user->amount;
            $data[$i]['created_at'] = $m_user->created_at;
            // $data[$i]["extra"] 			= $extra;
            $i++;
        }

        $res['draw'] = $draw;
        $res['recordsTotal'] = $count;
        $res['recordsFiltered'] = $count;
        $res['data'] = $data;

        return json_encode($res);
    }

    public function tokenReportExp($request)
    {
        header("Content-Type: text/csv; charset=utf-8");
        header("Content-Disposition: attachment; filename=Btc-txn-report-" . time() . ".csv");

        $result = Token::select('*');

        $user = $request->input('user');
        $txn_hash = $request->input('txn_hash');
        $method = $request->input('method');
        $amount = $request->input('amount');
        $from = $request->input('from');
        $to = $request->input('to');
        if ($user != "") {
            $result = $result->where('user', '=', $user);
        }

        if ($txn_hash != "") {
            $result = $result->where('txn_hash', '=', $txn_hash);
        }

        if ($method != "") {
            $result = $result->where('method', '=', $method);
        }

        if ($amount != "") {
            $result = $result->where('amount', '=', $amount);
        }
        if ($from != "") {
            $result = $result->whereDate("created_at", '>=', $from);
        }
        if ($to != "") {
            $result = $result->whereDate("created_at", '<=', $to);
        }
        $result = $result->get();

        $data = array();

        $i = 0;


        $output = fopen("php://output", "w");
        fputcsv($output, ['Transaction Hash', 'Transaction Address In', 'Transaction Address Out', 'Broadcast', 'Blockchain', 'Profit', 'Status', 'Ammount BTC', 'Ammount USD', 'IP Address', 'Created AT']);


        foreach ($result as $m_user) {

            $data[$i]['DT_RowId'] = "row_" . $m_user->id;
            $data[$i]['user']   = $m_user->user;
            $data[$i]['txn_hash'] = $m_user->txn_hash;
            $data[$i]['method'] = $m_user->method;
            $data[$i]['amount'] = $m_user->amount;
            $data[$i]['created_at'] = $m_user->created_at;
            fputcsv($output, $data[$i]);
        }

        fclose($output);
    }



}
