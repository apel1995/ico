-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Mar 07, 2022 at 05:40 AM
-- Server version: 5.7.36
-- PHP Version: 8.0.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ico`
--

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

DROP TABLE IF EXISTS `admins`;
CREATE TABLE IF NOT EXISTS `admins` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `admins_email_unique` (`email`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`id`, `name`, `email`, `image`, `phone`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Admin', 'admin@gmail.com', NULL, '1915985336', NULL, '$2y$10$C/FaN3Q8zDDgaJMh5eULJeUp4fMaMD6QXaZL8ICPpEssFmgZcUg8u', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

DROP TABLE IF EXISTS `failed_jobs`;
CREATE TABLE IF NOT EXISTS `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `uuid` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=45 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(37, '2014_10_12_000000_create_users_table', 1),
(38, '2014_10_12_100000_create_password_resets_table', 1),
(39, '2019_08_19_000000_create_failed_jobs_table', 1),
(40, '2019_12_14_000001_create_personal_access_tokens_table', 1),
(42, '2022_02_13_081856_create_admins_table', 2);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `personal_access_tokens`
--

DROP TABLE IF EXISTS `personal_access_tokens`;
CREATE TABLE IF NOT EXISTS `personal_access_tokens` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `tokenable_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `token_reports`
--

DROP TABLE IF EXISTS `token_reports`;
CREATE TABLE IF NOT EXISTS `token_reports` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `txn_hash` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `method` enum('BTC','TRON','Solana','Etherum') COLLATE utf8mb4_unicode_ci NOT NULL,
  `amount` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `token_reports`
--

INSERT INTO `token_reports` (`id`, `user`, `txn_hash`, `method`, `amount`, `created_at`, `updated_at`) VALUES
(1, 'buyer', 'lkjdsfsakjd98sdfsdkjfds', 'BTC', '200', '2022-03-09 11:58:40', '2022-03-02 11:58:40'),
(2, 'buyer', 'lkjdsfsakjd98sdfsdkjfds', 'BTC', '200', '2022-03-09 11:58:40', '2022-03-02 11:58:40');

-- --------------------------------------------------------

--
-- Table structure for table `transactions`
--

DROP TABLE IF EXISTS `transactions`;
CREATE TABLE IF NOT EXISTS `transactions` (
  `id` bigint(19) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(19) NOT NULL,
  `from_coin` varchar(10) DEFAULT NULL,
  `to_coin` varchar(10) DEFAULT NULL,
  `from` varchar(100) DEFAULT NULL,
  `to` varchar(100) DEFAULT NULL,
  `from_amount` decimal(19,8) DEFAULT NULL,
  `to_amount` decimal(19,8) DEFAULT NULL,
  `price` decimal(19,2) DEFAULT NULL,
  `txid` varchar(255) DEFAULT NULL,
  `type` enum('buy','bfw','tt','et') DEFAULT NULL,
  `status` varchar(20) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `transactions`
--

INSERT INTO `transactions` (`id`, `user_id`, `from_coin`, `to_coin`, `from`, `to`, `from_amount`, `to_amount`, `price`, `txid`, `type`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, 'FXEP', 'FXEP', '0xeFF16f6A353A8FE4522A4F2cFacaE46778E03dB9', '0x294B1df19e3c23C38363E182A5B87184462dA7F0', '10.00000000', '10.00000000', '1.00', '0x452d48a6ddf6fd63377eb031c473173c6acc839cecc59fb39c29219b13e165d5', 'tt', 'pending', '2018-09-28 08:06:28', '2018-09-28 15:06:28'),
(2, 1, 'FXEP', 'FXEP', '0xeFF16f6A353A8FE4522A4F2cFacaE46778E03dB9', '0x294B1df19e3c23C38363E182A5B87184462dA7F0', '100.00000000', '100.00000000', '1.00', '0xe5839b1078d4ed9dce569d2548e0baf3668deba8c5ac3a299707b71005401afa', 'tt', 'pending', '2018-09-28 12:39:02', '2018-09-28 19:39:02'),
(3, 2, 'ETH', 'ETH', '0x294B1df19e3c23C38363E182A5B87184462dA7F0', '0x87C53b2b30da09257738832390c08b36c28B765D', '0.00503800', '0.00503800', '2381.93', '0xadd599f381f9e01d82a107ea7699473965ce26094a285453bcb9e922fb2cd238', 'et', 'pending', '2021-05-30 14:12:34', '2021-05-30 08:12:34'),
(4, 2, 'ETH', 'ETH', '0xeFF16f6A353A8FE4522A4F2cFacaE46778E03dB9', '0x87C53b2b30da09257738832390c08b36c28B765D', '0.03786400', '0.03786400', '2356.29', '0x8d6af3e09a44a5c35a9627fecdd644acdaf9cf2f0c123ee21cd40b1df066a9b0', 'et', 'pending', '2021-05-30 14:26:10', '2021-05-30 08:26:10'),
(5, 2, 'ETH', 'ETH', '0x3F907EE67261CA301D19c7DC276E894a5D0EC431', '0x87C53b2b30da09257738832390c08b36c28B765D', '0.02835200', '0.02835200', '2364.89', '0x2b11bc39fdfd239b6704604fbab2cc885d368f218cf55656aadf75e942bae668', 'et', 'pending', '2021-05-30 14:42:23', '2021-05-30 08:42:23'),
(6, 2, 'ETH', 'ETH', '0xB9E5dAf0C0B35A8CcF213902E71B127e4b93855D', '0x87C53b2b30da09257738832390c08b36c28B765D', '0.07317000', '0.07317000', '2861.31', '0xf126c44bfcbf4db8cebcfd028da432e92344c2541f7aba314ea2402ba25701d9', 'et', 'pending', '2021-06-03 09:27:15', '2021-06-03 03:27:15');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `join_date` date NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `company_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `language` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `post_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `security` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `twitter` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `facebook` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `google_plus` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `linkedin` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `instagram` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `auth_type` enum('no_auth','email_auth','google_auth') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'no_auth',
  `google2fa_secret` text COLLATE utf8mb4_unicode_ci,
  `google2fa_url` text COLLATE utf8mb4_unicode_ci,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=MyISAM AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `phone`, `join_date`, `image`, `password`, `company_name`, `language`, `address`, `country`, `city`, `post_code`, `security`, `twitter`, `facebook`, `google_plus`, `linkedin`, `instagram`, `auth_type`, `google2fa_secret`, `google2fa_url`, `is_active`, `email_verified_at`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Alif Hossain', 'user@gmail.com', '1915985330', '1980-12-12', 'user.png', '$2y$10$jdOVBoPGpLp.WILX2e8I/.IZ6WJzTIyIN0fVZJLA6jHrvBjz1J2OW', 'itconcern', 'English', 'USA', 'Bangladesh', 'Dhaka', '1200', '1', 'https://twitter.com/', 'https://facebook.com/', 'https://googleplus.com/', 'https://linkedin.com/', 'https://instagram.com/', 'no_auth', NULL, 'https://chart.googleapis.com/chart?chs=150x150&chld=M|0&cht=qr&chl=otpauth://totp/ICO:user%40gmail.com?secret=6LUCEFX3ZPU5SOYO&issuer=ICO&algorithm=SHA1&digits=6&period=30', 1, NULL, NULL, '2022-03-02 00:22:29', '2022-03-06 06:02:06'),
(2, 'Altaf Hossain', 'altaf@gmail.com', '1915985300', '1980-12-12', 'user.png', '$2y$10$IVlwBZJYeFP3/HfkftRLDORtxuTfQW1jsh4PkH4UogGW2rDtGvfCi', 'itconcern', 'English', 'Khilgaoan', 'India', 'Dhaka', '1200', '1', 'https://twitter.com/', 'https://facebook.com/', 'https://googleplus.com/', 'https://linkedin.com/', 'https://instagram.com/', 'no_auth', NULL, NULL, 1, NULL, NULL, '2022-03-02 00:22:29', '2022-03-02 00:22:29'),
(3, 'Shishir  Rahman', 'shishir@gmail.com', '1915985339', '1996-01-02', 'user.png', '$2y$10$xoVqOLCSghaGtWNwUwLD4ufkoFHtdXJ90EzoqRwxCazVRQlA5eyeK', 'itconcern', 'English', 'Khilgaoan', 'Bhutan', 'Dhaka', '1200', '1', 'https://twitter.com/', 'https://facebook.com/', 'https://googleplus.com/', 'https://linkedin.com/', 'https://instagram.com/', 'no_auth', NULL, NULL, 1, NULL, NULL, '2022-03-02 00:22:29', '2022-03-02 00:22:29'),
(4, 'Adnan Hossain', 'adnan@gmail.com', '1915985336', '1999-02-06', 'user.png', '$2y$10$XYvi5KkOgrEDZFpCOQqpQukPTQnlpN8n1mGp.FmWuXX/fl3oNmHxa', 'itconcern', 'English', 'Khilgaoan', 'Pakistan', 'Dhaka', '1200', '1', 'https://twitter.com/', 'https://facebook.com/', 'https://googleplus.com/', 'https://linkedin.com/', 'https://instagram.com/', 'no_auth', NULL, NULL, 1, NULL, NULL, '2022-03-02 00:22:29', '2022-03-02 00:22:29'),
(5, 'Faruk Hossain', 'faruk@gmail.com', '1915985336', '1968-12-12', 'user.png', '$2y$10$pFHrn9yAoviKlxxwgW6D8uJdF4r01AV9VrpLtKf.P1z2Tz6kJYdQK', 'itconcern', 'English', 'Khilgaoan', 'Srilanka', 'Dhaka', '1200', '1', 'https://twitter.com/', 'https://facebook.com/', 'https://googleplus.com/', 'https://linkedin.com/', 'https://instagram.com/', 'no_auth', NULL, NULL, 1, NULL, NULL, '2022-03-02 00:22:29', '2022-03-02 00:22:29'),
(6, 'mahfuza  khatun', 'mahfuza@gmail.com', '1915985000', '1980-11-11', 'user.png', '$2y$10$Aff6I5CUGyiAfTWzE2rGb.jWDKm1AGTTUJLjsAdj.Usl5/irJfeg.', 'itconcern', 'English', 'Khilgaoan', 'Thailand', 'Dhaka', '1200', '1', 'https://twitter.com/', 'https://facebook.com/', 'https://googleplus.com/', 'https://linkedin.com/', 'https://instagram.com/', 'no_auth', NULL, NULL, 1, NULL, NULL, '2022-03-02 00:22:29', '2022-03-02 00:22:29'),
(7, 'Jhon Graham', 'graham@gmail.com', '1915985698', '1998-12-12', 'user.png', '$2y$10$yq1x.TEJw3kZ9jrtILJLjeJ7FEVIYAfzwnZe9t8W5bbuBMdy3gFt6', 'itconcern', 'English', 'Khilgaoan', 'Nepal', 'Dhaka', '1200', '1', 'https://twitter.com/', 'https://facebook.com/', 'https://googleplus.com/', 'https://linkedin.com/', 'https://instagram.com/', 'no_auth', NULL, NULL, 1, NULL, NULL, '2022-03-02 00:22:29', '2022-03-02 00:22:29'),
(8, 'Khalid  Hossain', 'khalid@gmail.com', '1915985369', '1990-06-05', 'user.png', '$2y$10$6sOVwQg.RC8hpEcH0kFPV.6XBUpVI32/9J4Fb87v//CxHlfW/e5ZG', 'itconcern', 'English', 'Khilgaoan', 'Bangladesh', 'Dhaka', '1200', '1', 'https://twitter.com/', 'https://facebook.com/', 'https://googleplus.com/', 'https://linkedin.com/', 'https://instagram.com/', 'no_auth', NULL, NULL, 1, NULL, NULL, '2022-03-02 00:22:29', '2022-03-02 00:22:29'),
(9, 'Noor Hossain', 'noor@gmail.com', '1915985032', '1984-11-12', 'user.png', '$2y$10$GT8WynvW0mNIhOVi606n9OeLtGwiZNv0PdhTx6OGedLWX8aBxt6qm', 'itconcern', 'English', 'Khilgaoan', 'Bangladesh', 'Dhaka', '1200', '1', 'https://twitter.com/', 'https://facebook.com/', 'https://googleplus.com/', 'https://linkedin.com/', 'https://instagram.com/', 'no_auth', NULL, NULL, 1, NULL, NULL, '2022-03-02 00:22:29', '2022-03-02 00:22:29'),
(10, 'Polash  Hossain', 'polash@gmail.com', '1915985399', '1987-11-12', 'user.png', '$2y$10$Gv7lRJ.cloNvfchEkn9/2eJ/mJe5DrPwYNlpTr.M10tw3SIxIDjjO', 'itconcern', 'English', 'Khilgaoan', 'Uk', 'Dhaka', '1200', '1', 'https://twitter.com/', 'https://facebook.com/', 'https://googleplus.com/', 'https://linkedin.com/', 'https://instagram.com/', 'no_auth', NULL, NULL, 1, NULL, NULL, '2022-03-02 00:22:29', '2022-03-02 00:22:29'),
(11, 'Samim Hossain', 'samim@gmail.com', '1915985336', '1999-01-05', 'user.png', '$2y$10$Lus1d1vfhKZfclPUvkLMcuhwFVTvqj1ZOvBTAb8QLMdf46eD88aMO', 'itconcern', 'English', 'Khilgaoan', 'Bangladesh', 'Dhaka', '1200', '1', 'https://twitter.com/', 'https://facebook.com/', 'https://googleplus.com/', 'https://linkedin.com/', 'https://instagram.com/', 'no_auth', NULL, NULL, 1, NULL, NULL, '2022-03-02 00:22:29', '2022-03-02 00:22:29'),
(12, 'Linia', 'liniya@gmail.com', '1915985258', '1985-03-01', 'user.png', '$2y$10$NOqMmN0ncmyg..jFXRJxqugYlv2cYESSOiH2UFbLQy0P1Nm0v/BxK', 'itconcern', 'English', 'Khilgaoan', 'India', 'Dhaka', '1200', '1', 'https://twitter.com/', 'https://facebook.com/', 'https://googleplus.com/', 'https://linkedin.com/', 'https://instagram.com/', 'no_auth', NULL, NULL, 1, NULL, NULL, '2022-03-02 00:22:29', '2022-03-02 00:22:29'),
(13, 'Ahsan', 'ahsan@gmail.com', '1915985010', '1982-06-06', 'user.png', '$2y$10$gFjinx12JRPlWGHSujI3fu7lvFGrM9NfFFDegYAZNdc/EvFRIpQci', 'itconcern', 'English', 'Khilgaoan', 'US', 'Dhaka', '1200', '1', 'https://twitter.com/', 'https://facebook.com/', 'https://googleplus.com/', 'https://linkedin.com/', 'https://instagram.com/', 'no_auth', NULL, NULL, 1, NULL, NULL, '2022-03-02 00:22:29', '2022-03-02 00:22:29'),
(14, 'Jiniya', 'jiniya@gmail.com', '1915985377', '1985-12-12', 'user.png', '$2y$10$e2rmQtnuCCiUO1e.CVkuSO/w8e6w9KJAoN.t3ETcPDC8xhiqBmbte', 'itconcern', 'English', 'Khilgaoan', 'Netherland', 'Dhaka', '1200', '1', 'https://twitter.com/', 'https://facebook.com/', 'https://googleplus.com/', 'https://linkedin.com/', 'https://instagram.com/', 'no_auth', NULL, NULL, 1, NULL, NULL, '2022-03-02 00:22:29', '2022-03-02 00:22:29');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
