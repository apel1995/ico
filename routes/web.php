<?php

use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Route;




//live server command route start
Route::get('/clear', function(){
    Artisan::call("config:cache");
    Artisan::call("cache:clear");
    return "success";
});

Route::get('/db', function(){
    Artisan::call("migrate");
    Artisan::call("db:seed");
    return "success";
});
//live server command route end


/*
|--------------------------------------------------------------------------
| Backend Routes Start
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
require_once "admin/web.php";
/*
|--------------------------------------------------------------------------
| Backend Routes End
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/





/*
|--------------------------------------------------------------------------
| Frontend Routes Start
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
require_once "user/web.php";
/*
|--------------------------------------------------------------------------
| Frontend Routes End
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

