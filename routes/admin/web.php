<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Admin\Auth\ForgetPasswordController;
use App\Http\Controllers\Admin\Auth\LoginController;
use App\Http\Controllers\Admin\Auth\LogoutController;
use App\Http\Controllers\Admin\DashboardController;
use App\Http\Controllers\Admin\TokenPurchaseModule\TokenPurchaseController;
use App\Http\Controllers\admin\TransactionModule\TransectionController;
use App\Http\Controllers\Admin\KycModule\KycRequestController;
use App\Http\Controllers\Admin\KycModule\KycReportController;
use App\Http\Controllers\Admin\SupportTicketModule\CreateTicketController;
use App\Http\Controllers\Admin\SupportTicketModule\CustomerListController;
use App\Http\Controllers\Admin\SupportTicketModule\CommentController;


use App\Http\Controllers\Admin\ManageClientsModule\ManageClientsController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Auth::routes();

//login route start
Route::get('/adminpanel', [LoginController::class, 'login_show'])->name('login.show');
Route::post('/do-login', [LoginController::class, 'do_login'])->name('do.login');
//login route end

//logout route start
Route::get('/do-logout', [LogoutController::class, 'do_logout'])->name('do.logout');
//logout route end

//backend route group start

Route::group(['middleware' => 'admin'], function () {
    Route::get('/admin/dashboard', [DashboardController::class, 'index'])->name('dashboard');
    
    //  Apel Sarkar  Routes  Start
    Route::any('admin/report/token_report', [TokenPurchaseController::class, 'tokenReport']);
    Route::any('admin/report/transection_report', [TransectionController::class, 'transReport']);
    Route::any('admin/manage-client', [ManageClientsController::class, 'manageClient']);
    Route::any('admin/client-update', [ManageClientsController::class, 'user_profile_update']);
    Route::any('admin/client/send-mail', [ManageClientsController::class, 'send_mail']);
    Route::any('admin/client/password-reset/{id}', [ManageClientsController::class, 'password_reset']);
    Route::any('admin/client/password-change', [ManageClientsController::class, 'password_changed']);
    Route::any('admin/client/transaction-pin-reset/{id}', [ManageClientsController::class, 'transaction_pin_reset']);
    Route::any('admin/client/transaction-pin-changed', [ManageClientsController::class, 'transaction_pin_change']);
    Route::any('admin/client/comment', [ManageClientsController::class, 'do_comment']);
    Route::any('admin/client/comment-delete/{id}', [ManageClientsController::class, 'comment_delete']);

    //  Apel Sarkar  routes  Ends
    
    //settings module routes start
     Route::group(['prefix' => 'admin'], function () {
        require_once 'manage_clients_module/manage_clients.php';
    });
    //settings module routes end
    //kyc request
    Route::any('admin/report/Kyc_request', [KycRequestController::class, 'kycReport']);
    Route::any('admin/report/update', [KycRequestController::class, 'user_data_update'])->name('admin.report.update');
    Route::any('admin/report/update/approve', [KycRequestController::class, 'update_status_approve'])->name('admin.report.update.approve');
    Route::any('admin/report/update/decline', [KycRequestController::class, 'update_status_decline'])->name('admin.report.update.decline');
    //kyc report
    Route::any('admin/report/Kyc_report', [KycReportController::class, 'kycReport']);
    //Support Ticket
    Route::get('admin/support_token/create_ticket', [CreateTicketController::class, 'uploadForm']);
    Route::post('admin/support_token/create_ticket', [CreateTicketController::class, 'uploadSubmit']);
    Route::get('admin/support_token/ticket_list', [CreateTicketController::class, 'ticketList']);
    Route::get('admin/support_token/customer', [CustomerListController::class, 'customer']);
    Route::get('admin/support_token/ticket_details/{id}', [CustomerListController::class, 'fatch_customer_details']);
    Route::post('admin/support_token/ticket_details/reply/store/{id}', [CommentController::class, 'replyStore'])->name('reply.add');
    Route::any('admin/support_token/ticket_details/reply/update/', [CommentController::class, 'comment_data_update']);
    Route::any('admin/support_token/ticket_details/reply/delete/{id}', [CommentController::class, 'comment_data_delete']);
    Route::any('admin/support_token/ticket_details/delete/{id}', [CommentController::class, 'support_ticket_delete']);
    Route::any('admin/support_token/ticket_details/assign/{id}', [CommentController::class, 'support_ticket_assign']);
    Route::any('admin/support_token/delete/{id}', [CreateTicketController::class, 'support_ticket_delete']);
   

    

    

    // Route::get('admin/support_token/ticket_list', [CreateTicketController::class, 'getCategory']);
    // Route::get('admin/support_token/ticket_list', [CreateTicketController::class, 'getPost']);
  
});
//backend route group end

//view without  controller for testing
Route::get('admin/support_token/dashboard', function(){
    return view('pages/admin/modules/support_ticket/help-dashboard');
});


