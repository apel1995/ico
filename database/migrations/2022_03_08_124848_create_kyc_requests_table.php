<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kyc_requests', function (Blueprint $table) {
            $table->id();
            $table->string('client_name'); //name
            $table->enum('client_type', ['trader','ib']);
            $table->enum('doc_type', ['gas_bill','utility_bill', 'electric_bill', 'telephone_bill', 'credit_statement', 'bank_statement', 'passport', 'national_id', 'driving_license']);
            $table->string('email');
            $table->string('country');
            $table->string('address');
            $table->string('city');
            $table->string('state');
            $table->string('phone');
            $table->string('zip');
            $table->date('bithday');
            $table->date('issu_date');
            $table->date('exp_date');
            $table->string('status');
            $table->timestamps();//submitted date
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kyc_requests');
    }
};
