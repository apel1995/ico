<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserProfileSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement("DELETE FROM users");
        DB::table("users")->insert([
            [
                "id"  => 1,
                "name"  => "test  name",
                "email"  => "user@gmail.com",
                "phone"  => 1915985336,
                "image"  => "user.png",
                "password"  => Hash::make('123456789'),
                "company_name"  => "itconcern",
                "language"  => "English",
                "address"  => "Khilgaoan",
                "security"  => 1,
                "is_active"  => true,

            ]
        ]);
    }
}
         