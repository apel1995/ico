    
    
    
    <!-- Core scripts -->
    <script src="{{ asset('assets/user/js/pace.js') }}"></script>
    <script type="text/javascript" src="{{ asset('auth/js/jquery-1.11.2.min.js') }}"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script src="{{ asset('assets/user/js/ajax_form_submit.js') }}"></script>
    <script src="{{ asset('auth/js/bootstrap.js') }}"></script>
    <script src="{{ asset('assets/user/js/sidenav.js') }}"></script>
    <script src="{{ asset('assets/user/js/layout-helpers.js') }}"></script>
    <script src="{{ asset('assets/user/js/material-ripple.js') }}"></script>
    <script src="{{ asset('assets/user/js/jquery.slimscroll.js') }}"></script>
    <script src="{{ asset('assets/user/js/jquery.waves.js') }}"></script>
    <script src="{{ asset('assets/user/js/jQuery.style.switcher.js') }}"></script>
    <script src="{{ asset('assets/user/js/animate.js') }}"></script>
    <script src="{{ asset('assets/user/js/bootstrap-notify.min.js') }}"></script>
    <script src="{{ asset('assets/user/js/lc_switch.js') }}"></script>
   
    <!-- Libs -->

    <script src="{{ asset('assets/user/libs/select2/select2.js') }}"></script>
    <script src="{{ asset('assets/user/libs/bootstrap-tagsinput/bootstrap-tagsinput.js') }}"></script>

    <!-- Demo -->
    <script src="{{ asset('assets/user/js/demo.js') }}">
    </script><script src="{{ asset('assets/user/js/analytics.js') }}"></script>
    </script><script src="{{ asset('assets/user/js/pages/pages_account-settings.js') }}"></script>


    <script src="{{ asset('assets/user/libs/eve/eve.js') }}"></script>
    <script src="{{ asset('assets/user/libs/flot/flot.js') }}"></script>
    <script src="{{ asset('assets/user/libs/flot/curvedLines.js') }}"></script>
    <script src="{{ asset('assets/user/libs/chart-am4/core.js') }}"></script>
    <script src="{{ asset('assets/user/libs/chart-am4/charts.js') }}"></script>
    <script src="{{ asset('assets/user/libs/chart-am4/animated.js') }}"></script>
    
    @yield('per_page_js')
    <script type="text/javascript">
        $(document).ready(function(e) {
            $('.auth-switch input').lc_switch();
        
            // // triggered each time a field changes status          
            
            // triggered each time a field is checked
            $('body').delegate('.lcs_check', 'lcs-on', function() {
        
                if($(this).val() == 3){
                    $("#gauth").fadeIn();
                }else{
                    $("#gauth").fadeOut();
                }
            });
            
        });
        </script>
    
    <script>
        $(document).ready(function() {
            // checkCookie();
            $('#exampleModalCenter').modal();
        });

        function setCookie(cname, cvalue, exdays) {
            var d = new Date();
            d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
            var expires = "expires=" + d.toGMTString();
            document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
        }

        function getCookie(cname) {
            var name = cname + "=";
            var decodedCookie = decodeURIComponent(document.cookie);
            var ca = decodedCookie.split(';');
            for (var i = 0; i < ca.length; i++) {
                var c = ca[i];
                while (c.charAt(0) == ' ') {
                    c = c.substring(1);
                }
                if (c.indexOf(name) == 0) {
                    return c.substring(name.length, c.length);
                }
            }
            return "";
        }

        function checkCookie() {
            var ticks = getCookie("modelopen");
            if (ticks != "") {
                ticks++;
                setCookie("modelopen", ticks, 1);
                if (ticks == "2" || ticks == "1" || ticks == "0") {
                    $('#exampleModalCenter').modal();
                }
            } else {
                // user = prompt("Please enter your name:", "");
                $('#exampleModalCenter').modal();
                ticks = 1;
                setCookie("modelopen", ticks, 1);
            }
        }
    </script>
    
