            
            
            
            <!-- [ Layout sidenav ] Start -->
            <div id="layout-sidenav" class="layout-sidenav sidenav sidenav-vertical bg-white logo-dark">
                <!-- Brand demo (see assets/css/demo/demo.css) -->
                <div class="app-brand demo">
                    <span class="app-brand-logo demo">
                        <img src="{{ asset('assets/user/img/logo.png') }}" alt="Brand Logo" class="img-fluid">
                    </span>
                    <a href="{{ route('user.dashboard') }}" class="app-brand-text demo sidenav-text font-weight-normal ml-2">Dashboard</a>
                    <a href="javascript:" class="layout-sidenav-toggle sidenav-link text-large ml-auto">
                        <i class="ion ion-md-menu align-middle"></i>
                    </a>
                </div>
                <div class="sidenav-divider mt-0"></div>

                <!-- Links -->
                <ul class="sidenav-inner py-1">
                    <!-- Dashboards -->
                    <li class="sidenav-item open active">
                        <a href="{{ route('user.dashboard') }}" class="sidenav-link sidenav-toggle">
                            <i class="sidenav-icon feather icon-home"></i>
                            <div>Dashboards</div>
                            <div class="pl-1 ml-auto">
                                <div class="badge badge-danger">Hot</div>
                            </div>
                        </a>
                    </li>

                    <li class="sidenav-item">
                        <a href="javascript:" class="sidenav-link sidenav-toggle">
                            <i class="sidenav-icon feather icon-layers"></i>
                            <div>REPORTS</div>
                            <div class="pl-1 ml-auto">
                                <div class="badge badge-primary">100+</div>
                            </div>
                        </a>
                        <ul class="sidenav-menu">
                            {{-- <li class="sidenav-item">
                                <a href="{{url('/cvcvc//')}}" class="sidenav-link">
                                    <div>TOKEN REPORT</div>
                                </a>
                            </li> --}}
                            <li class="sidenav-item">
                                <a href="{{url('user/transection-report')}}" class="sidenav-link">
                                    <div>TRANSECTION REPORT</div>
                                </a>
                            </li>
                            <li class="sidenav-item">
                                <a href="w-social.html" class="sidenav-link">
                                    <div>Social</div>
                                </a>
                            </li>
                            <li class="sidenav-item">
                                <a href="w-chart.html" class="sidenav-link">
                                    <div>Chart</div>
                                </a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
            <!-- [ Layout sidenav ] End -->