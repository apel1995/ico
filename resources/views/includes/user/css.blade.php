


<link rel="icon" type="image/x-icon" href="{{ asset('assets/user/img/favicon.ico') }}">

<!-- Google fonts -->
<link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet">


    <!-- Icon fonts -->
    <link rel="stylesheet" href="{{ asset('assets/user/fonts/fontawesome.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/user/fonts/ionicons.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/user/fonts/linearicons.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/user/fonts/open-iconic.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/user/fonts/pe-icon-7-stroke.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/user/fonts/feather.css') }}">

    <!-- Core stylesheets -->
    <link rel="stylesheet" href="{{ asset('assets/user/css/bootstrap-material.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/user/css/shreerang-material.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/user/css/uikit.css') }}">

    <!-- Libs -->
    <link rel="stylesheet" href="{{ asset('assets/user/libs/perfect-scrollbar/perfect-scrollbar.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/user/libs/flot/flot.css') }}">




 <link rel="stylesheet" href="{{ asset('assets/user/libs/bootstrap-tagsinput/bootstrap-tagsinput.css') }}">

 <!-- Page -->
 <link rel="stylesheet" href="{{ asset('assets/user/css/pages/account.css') }}">
 <!---page loader css --->
 <link rel="stylesheet" href="{{ asset('auth/css/loader.css') }}">
   
 

@yield("per_page_css")
