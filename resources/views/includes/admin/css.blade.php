


    <link rel="icon" type="image/x-icon" href="{{ asset('assets/admin/img/favicon.ico') }}">

    <!-- Google fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet">


    <!-- Icon fonts -->
    <link rel="stylesheet" href="{{ asset('assets/admin/fonts/fontawesome.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/admin/fonts/ionicons.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/admin/fonts/linearicons.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/admin/fonts/open-iconic.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/admin/fonts/pe-icon-7-stroke.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/admin/fonts/feather.css') }}">

    <!-- Core stylesheets -->
    <link rel="stylesheet" href="{{ asset('assets/admin/css/bootstrap-material.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/admin/css/shreerang-material.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/admin/css/uikit.css') }}">

    <!-- Libs -->
    <link rel="stylesheet" href="{{ asset('assets/admin/libs/perfect-scrollbar/perfect-scrollbar.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/admin/libs/flot/flot.css') }}">

    <!-- BEGIN: Custom CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/admin/css/custom.css') }}">
    <!-- END: Custom CSS-->

    @yield("per_page_css")

    