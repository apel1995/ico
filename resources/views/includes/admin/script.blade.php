
<!-- Core scripts -->
<script src="{{ asset('assets/admin/js/pace.js') }}"></script>
<script src="{{ asset('assets/admin/js/jquery-3.3.1.min.js') }}"></script>
<!--<script src=" asset('assets/admin/js/jquery-3.2.1.min.js') "></script> -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
<script src="{{ asset('assets/admin/libs/popper/popper.js')}}"></script>
<script src="{{ asset('assets/admin/js/bootstrap.js') }}"></script>
<script src="{{ asset('assets/admin/js/sidenav.js')}}"></script>
<script src="{{ asset('assets/admin/js/layout-helpers.js') }}"></script>
<script src="{{ asset('assets/admin/js/material-ripple.js') }}"></script>

<!-- Libs -->
<script src="{{ asset('assets/admin/libs/perfect-scrollbar/perfect-scrollbar.js') }}"></script>

<!-- Demo -->    

<script src="{{ asset('assets/admin/js/demo.js') }}"></script>
<script src="{{ asset('assets/admin/js/analytics.js') }}"></script>

@yield('per_page_js')

    
