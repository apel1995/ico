@extends("layouts.admin.app")

@section('per_page_css')
<link href="{{ asset('assets/admin/DatePicker/date-picker/css/bootstrap-datepicker.min.css') }}"  rel="stylesheet">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.11.5/css/jquery.dataTables.min.css"/>
<link rel="stylesheet" type="text/css" href="{{ asset('assets/admin/datatable/new-custom-datatable.css')}}"/>
<link href="https://cdn.jsdelivr.net/sweetalert2/4.2.4/sweetalert2.min.css" rel="stylesheet"/>



<style>
    td.details-control {
      
        background:  url("{{ asset('assets/admin/datatable/icon/plus.png') }}") no-repeat center center;
        cursor: pointer;
    }
    tr.details td.details-control {
        background: url("{{ asset('assets/admin/datatable/icon/minus.png') }}") no-repeat center center;
    }

    a {
        color: rgb(24, 207, 115);
    }

    .switch {
  position: relative;
  display: inline-block;
  width: 60px;
  height: 34px;
}

.switch input { 
  opacity: 0;
  width: 0;
  height: 0;
}

.slider {
  position: absolute;
  cursor: pointer;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: #ccc;
  -webkit-transition: .4s;
  transition: .4s;
}

.slider:before {
  position: absolute;
  content: "";
  height: 26px;
  width: 26px;
  left: 4px;
  bottom: 4px;
  background-color: white;
  -webkit-transition: .4s;
  transition: .4s;
}

input:checked + .slider {
  background-color: #2196F3;
}

input:focus + .slider {
  box-shadow: 0 0 1px #2196F3;
}

input:checked + .slider:before {
  -webkit-transform: translateX(26px);
  -ms-transform: translateX(26px);
  transform: translateX(26px);
}

/* Rounded sliders */
.slider.round {
  border-radius: 34px;
}

.slider.round:before {
  border-radius: 50%;
}

</style>
@endsection



@section('body-content')


        <!-- [ content ] Start -->
        <div class="container-fluid flex-grow-1 container-p-y">
            <h4 class="font-weight-bold py-3 mb-0">CLIENTS MANAGEMENT</h4>
            <div class="row">

                <div class="col-md-12">
                    <div class="card d-flex w-100 mb-4">
                        <h6 class="card-header">CLIENTS FILTER</h6>
                        <div class="row no-gutters row-bordered row-border-light h-100">
                            <div class="d-flex col-md-12 align-items-center">
                                <div class="card-body">
                                    <form id="filter_form" action="" method="get">
                                        <input type="hidden" name="op" value="export">
                                        <div class="form-row">
                                            <div class="col-md-3">
                                                <input id="finance" type="text" class="form-control fv" name="finance" placeholder="Search By Finance">
                                            </div><br><br>
                
                                            <div class="col-md-3">
                                                <input id="manager" type="text" class="form-control fv" name="manager" placeholder="Account Manager">
                                            </div><br><br>
                                            <div class="col-md-3">
                                                <input id="desk_manager" type="text" class="form-control fv" name="desk_manager" placeholder="Desk Manager">
                                            </div><br><br>
                
                                            <div class="col-md-3">

                                                <select Class="form-control" name="method" id="method">
                                                    <option value="">CATEGORY</option>
                                                    <option value="btc">BTC</option>
                                                    <option value="tron">Tron</option>
                                                    <option value="solona">Solona</option>
                                                    <option value="etherum">Etherum</option>
                                                </select>
                                               
                                            </div><br><br>
                
                                           

                                        </div><br>
                                        <div class="form-row">

                                            <div class="col-md-4">
                                                <input id="info" type="text" class="form-control fv" name="name" placeholder="Email / Name / Country / Phone">
                                            </div><br><br>
                                            <div class="col-md-4">
                                                <div class="input-group" data-date="2017/01/01" data-date-format="yyyy/mm/dd">
                                                    <input id="from" type="text" class="form-control rounded dpd1 date-picker-input fv" name="from" placeholder="From Date">
                                                    <span class="px-3 py-2">To</span>
                                                    <input id="to" type="text" class="form-control rounded dpd2 date-picker-input fv" name="to" placeholder="To Date">
                                                </div>
                                            </div><br><br>

                                            <div class="col-md-4">
                                                <div class="input-group" data-date="2017/01/01" data-date-format="yyyy/mm/dd">
                                                    <input id="from" type="text" class="form-control rounded dpd1 date-picker-input fv" name="from" placeholder="MIN">
                                                    <span class="px-3 py-2">To</span>
                                                    <input id="to" type="text" class="form-control rounded dpd2 date-picker-input fv" name="to" placeholder="MAX">
                                                </div>
                                            </div><br><br>
                
                                            



                                        </div><br>

                                        <div class="col-12">
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <button id="export_btn" type="submit" class="btn btn-primary btn-block">Export To CSV</button>
                                                </div><br><br>
                                                <div class="col-md-3">
                                                    <button id="filterBtn" type="button" class="btn btn-success btn-block">FILTER</button>
                                                </div>
                                                <div class="col-md-3">
                                                    <button id="export_balance" type="button" class="btn btn-success btn-block">Export With Balance</button>
                                                </div>
                                                <div class="col-md-3">
                                                    <button id="reset" type="button" class="btn btn-success btn-block">Reset</button>
                                                </div>

                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                           
                        </div>
                    </div>
                </div>
            </div>


            <div class="row">

                <div class="col-md-12">
                    <div class="card d-flex w-100 mb-4">
                        <h6 class="card-header">CLIENTS DATA</h6>
                        <div class="row no-gutters row-bordered row-border-light h-100">
                            <div class="d-flex col-md-12 align-items-center">
                                <div class="card-body">
                                    
                                    <table id="btc_tbl" class="display table table-hover table-custom" style="width:100%">
                                        <thead>
                                            <tr>
                                                <th></th>
                                                <th>Name</th>
                                                <th>Email</th>
                                                <th>Phone</th>
                                                <th>Country</th>
                                                <th>join_date</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                           
                        </div>
                    </div>
                </div>
            </div>

        </div>

        {{-- Update Profile Modal --}}
        
        <div class="modal fade" id="client-update" tabindex="-1" role="dialog" aria-labelledby="exampleModalPreviewLabel"
        aria-hidden="true">
        <div class="modal-dialog">
            <form action="{{ url('admin/client-update') }}" id="" method="post" class="ajax-form modal-content"
                enctype="multipart/form-data">
                @csrf

                <div class="modal-header">
                    <h5 class="modal-title">Client Update Profile
                        <br>
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">×</button>
                </div>
                <div class="modal-body">

                    <input type="hidden" id="user_id" name="user_id" class="form-control">
                    <div class="form-row">
                        <div class="form-group col mb-0">
                            <label class="form-label">Name</label>
                            <input type="text" id="name" name="name" class="form-control" placeholder="Client">
                            <div class="clearfix"></div>
                        </div>
                      
                    </div>
                    <div class="form-row">
                        <div class="form-group col mb-0">
                            <label class="form-label">Post Code</label>
                            <input type="text" id="post_code" name="post_code" class="form-control" placeholder="7051">
                            <div class="clearfix"></div>
                        </div>
                        <div class="form-group col mb-0">
                            <label class="form-label">Phone</label>
                            <input type="text" id="phone" name="phone" class="form-control" placeholder="+44564453154">
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col mb-0">
                            <label class="form-label">Country</label>
                            <input type="text" id="country" name="country" class="form-control"
                                placeholder="Your country">
                            <div class="clearfix"></div>

                        </div>
                        <div class="form-group col mb-0">
                            <label class="form-label">City</label>
                            <input type="text" id="city" name="city" class="form-control"
                                placeholder="Your state">
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col">
                            <label class="form-label">Address</label>
                            <input type="textarea" id="address" name="address" class="form-control"
                                placeholder="Ching Chong Street">
                            <div class="clearfix"></div>
                        </div>
                        <div class="form-group col">
                            <label class="form-label">Email</label>
                            <input type="text" id="email" name="email" class="form-control"
                                placeholder="Email">
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" id="message" class="btn btn-primary">Update</button>
                    {{-- <button type="submit"  id="submitBtn"  class="btn btn-primary" data-btnid="submitBtn">Save</button> --}}
                </div>
            </form>
        </div>
    </div>
    {{-- send mail modal --}}
    <div class="modal fade" id="send_mail" tabindex="-1" role="dialog" aria-labelledby="exampleModalPreviewLabel"
    aria-hidden="true">
    <div class="modal-dialog">
        <form action="{{ url('admin/client/send-mail') }}" id="betafeedback" method="post" class="ajax-form modal-content"
            enctype="multipart/form-data">
            @csrf

            <div class="modal-header">
                <h5 class="modal-title">Send Mail
                    <br>
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">×</button>
            </div>
            <div class="modal-body">

                <input type="hidden" id="user_id_update" name="user_id_update" class="form-control">
            
               
              
                <div class="form-row">           
                    <div class="form-group col">
                        <label class="form-label">Email To</label>
                        <input type="text" id="email_mail" name="email_mail" class="form-control"
                            placeholder="Email">
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" id="close" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" id="notify" class="btn btn-primary">Send</button>
                {{-- <button type="submit"  id="submitBtn"  class="btn btn-primary" data-btnid="submitBtn">Save</button> --}}
            </div>
        </form>
    </div>
</div>


{{-- client password changed --}}

<div class="modal fade" id="client_password_update" tabindex="-1" role="dialog" aria-labelledby="exampleModalPreviewLabel" aria-hidden="true">
    <div class="modal-dialog">
    <form action="{{ url('admin/client/password-change') }}" id="approve_modal_form" method="post" class="ajax-form modal-content"
        enctype="multipart/form-data">
        @csrf

        <div class="modal-header">
            <h5 class="modal-title">Password changed
                <br>
            </h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">×</button>
        </div>
        <div class="modal-body">

            <input type="hidden" id="client_pass_change" name="client_pass_change" class="form-control">
        
            <div class="form-row">           
                <div class="form-group col">
                    <label class="form-label">Enter Password</label>
                    <input type="text" id="client_password" name="client_password" class="form-control"
                        placeholder="New Password" >
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" id="close" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="button" id="client_pass" data-btnid="client_pass" data-form="approve_modal_form"
                            data-validator="true" data-loading="processing..." data-callback="client_password_changed"
                            onclick="_run(this)" class="btn btn-success btn-md btn-rounded">Update</button>
        </div>
    </form>
    </div>
</div>


{{-- <----Transaction PIN Changed------> --}}

    <div class="modal fade" id="trans_pin_update" tabindex="-1" role="dialog" aria-labelledby="exampleModalPreviewLabel" aria-hidden="true">
        <div class="modal-dialog">
        <form action="{{ url('admin/client/transaction-pin-changed') }}" id="approve_modal_pin" method="post" class="ajax-form modal-content"
            enctype="multipart/form-data">
            @csrf
    
            <div class="modal-header">
                <h5 class="modal-title">Transaction PIN changed
                    <br>
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">×</button>
            </div>
            <div class="modal-body">
    
                <input type="hidden" id="transaction_pin" name="transaction_pin" class="form-control">
            
                <div class="form-row">           
                    <div class="form-group col">
                        <label class="form-label">Enter PIN</label>
                        <input type="text" id="transaction_pin_c" name="transaction_pin_c" class="form-control"
                            placeholder="New PIN" >
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" id="close" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" id="trans_pinn" data-btnid="trans_pinn" data-form="approve_modal_pin"
                                data-validator="true" data-loading="processing..." data-callback="transaction_pin_up"
                                onclick="_run(this)" class="btn btn-success btn-md btn-rounded">Update PIN</button>
            </div>
        </form>
        </div>
    </div>
    
    {{-- <----Admin Comment In User Field------> --}}
     <div class="modal modal-top fade" id="comment-modal">
        <div class="modal-dialog">
            <form action="{{ url('admin/client/comment') }}" id="approve_comment"  class="ajax-form modal-content"
            enctype="multipart/form-data">
            @csrf
            <form class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Comment Here
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">×</button>
                </div>
                <div class="modal-body">
                    <input type="hidden" id="admin_comment_kor" name="admin_comment_kor" class="form-control">
                    <div class="form-row">
                        <textarea type="text" id="tinymce-editor" name="admin_comment"  class="form-control"></textarea>
                     </div>
                </div>
                        <div class="modal-footer">                   
                    <button type="button" class="btn btn-primary"  data-validator="true" data-loading="processing..." data-callback="comment_by_admin"
                    onclick="_run(this)" class="btn btn-success btn-md btn-rounded"  data-form="approve_comment">Submit Comment</button>
                    <button type="button"class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div> 

        <!-- [ content ] End -->

@endsection


@section('per_page_js')

    
    <!--date picker-->
    
    <script src="{{ asset('assets/admin/DatePicker/date-picker/js/bootstrap-datepicker.min.js') }}"></script>
    <!--init date picker-->
    <script src="{{ asset('assets/admin/DatePicker/pickers/init-date-picker.js') }}"></script>
    <!-- common ajax -->

    <script src="{{ asset('assets/admin/js/common-ajax.js') }}"></script>
    {{-- <---Data_table_CDN----> --}}
    <script type="text/javascript" src="https://cdn.datatables.net/1.11.5/js/jquery.dataTables.min.js"></script>
    <script src="{{ asset('assets/admin/js/pages/ui_modals.js') }}"></script>
    <script src="https://cdn.jsdelivr.net/sweetalert2/4.2.4/sweetalert2.min.js"></script>
    <script src="{{asset('assets/admin/libs/markdown/markdown.js')}}"></script>
    <script src="{{asset('assets/admin/libs/bootstrap-markdown/bootstrap-markdown.js')}}"></script>

    

   

<script>



    //User profile update
  

    function update_profile(e) {
        
            let obj = $(e);
            $("#client-update").modal("show");
            $('#user_id').val(obj.data('id'));
            $('#name').val(obj.data('name'));
            $('#post_code').val(obj.data('post_code'));
            $('#phone').val(obj.data('phone'));
            $('#country').val(obj.data('country'));
            $('#city').val(obj.data('city'));
            $('#address').val(obj.data('address'));
            $('#email').val(obj.data('email'));    
        }
//End User profile update

// Send Mail
        function send_mail(e){
             let obj = $(e);
             $("#send_mail").modal("show");
             $('#user_id_update').val(obj.data('id'));
             $('#email_mail').val(obj.data('email'));   
        }

        document.getElementById('message').onclick = function(){
            swal("Successfully Updated");
        };

        document.getElementById('notify').onclick = function(){
            swal("Mail Sent");
        };


       

    // End Send Mail

    //password change hidden ID
    function client_password_changed_id(e){
             let obj = $(e);
             $('#client_pass_change').val(obj.data('id')); 
        }

    //transaction pin hidden ID
    function transaction_pin_changed(e){
        let obj = $(e);
             $('#transaction_pin').val(obj.data('id')); 
    }
  
    //do-comment
    function admin_do_comment(e){
        let obj = $(e);
             $('#admin_comment_kor').val(obj.data('id'));
    }
        function createButton(text, cb) {
        return $('<button>' + text + '</button>').on('click', cb);
        }

            

        $("#reset").click(function(){
            $(".fv").val('');
        });



        
        function format (d) {
            return d.button+'<br>'+d.extra;
            //return '<b>Transaction Hash:</b> '+Hash+ '<br> <a href="'+btcurl+Hash+'">Check Transaction</a>';   
            
        }
    
        $(document).ready(function(){
    
            var dt = $('#btc_tbl').DataTable( {
                "processing": true,
                "serverSide": true,
                "ajax": {
                    "url": "{{url('admin/manage-client?op=data_table')}}",
                    "data": function (d) {
                      return $.extend( {}, d, {
                        "name": $("#info").val(),
                        "from": $("#from").val(),
                        "to": $("#to").val(),
                      });
    
                    }
                },
    
                "columns": [
                    {
                        "class":          "details-control",
                        "orderable":      false,
                        "data":           null,
                        "defaultContent": ""
                    },
                    { "data": "name" },
                    { "data": "email" },
                    { "data": "phone" },
                    { "data": "country" },
                    { "data": "join_date" },
                ],
                
                "drawCallback": function( settings ) {
                    $("#filterBtn").html("FILTER");
                }
               
            });
    
            // Array to track the ids of the details displayed rows
            var detailRows = [];
         
            $('#btc_tbl tbody').on( 'click', 'tr td.details-control', function(){
                var tr = $(this).closest('tr');
                var row = dt.row( tr );
                var idx = $.inArray( tr.attr('id'), detailRows);
         
                if ( row.child.isShown()){
                    tr.removeClass('details');
                    row.child.hide();
         
                    // Remove from the 'open' array
                    detailRows.splice(idx, 1);
                }
                else {
                    tr.addClass( 'details' );
                    row.child( format(row.data())).show();
         
                    // Add to the 'open' array
                    if (idx === -1) {
                        detailRows.push(tr.attr('id'));
                    }
                }
            });
         
            // On each draw, loop over the `detailRows` array and show any child rows
            dt.on( 'draw', function(){
                $.each( detailRows, function (i, id){
                    $('#'+id+' td.details-control').trigger('click');
                });
            });
    
            $('#filterBtn').click(function (e) {
                $(this).html("<i class='fa fa-refresh fa-spin fa-1x fa-fw'></i>");
                dt.draw();
            });
    
        });
        
    // send mail ajax operation
        $("#betafeedback").submit(function(event) {
        event.preventDefault();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        

        formdata = new FormData($(this)[0]);

        $.ajax({
            url: $(this).attr('action'),

            contentType: false,
            processData: false,
            data: formdata,
            type: 'POST',


        });
        return false;
    });

    //password reset in manage client
    function password_reset(e){
        let obj = $(e);
        $(".ressetbtn").html('Proccessing.....');
        var id=obj.data('id');
        $.get('client/password-reset/' +id,function (data, textStatus, jqXHR) {
            swal (data.message);
            $(".ressetbtn").html('PASSWORD RESET');   
            });
        
     };

    //comment deleted by admin in manage client
    function comment_delete(e){
        let obj=$(e);
        $(".commentDelete").html('Proccessing.........');
        var id=obj.data('id');
        $.get('client/comment-delete/' +id,function (data, textStatus, jqXHR) {
            console.log('testy');
            swal (data.message);
            $(".commentDelete").html('DELETE');   
            });

    }

     //transation pin reset 
     function transaction_pin_reset(e){
         let obj=$(e);
         $(".transpin_reset").html('Proccessing.....');
         var id=obj.data('id');
         $.get('client/transaction-pin-reset/' +id, function (data,textStatus,jqXHR){
            swal(data.message);
            $(".transpin_reset").html('TRANSACTION PIN RESET');
         });
     }


   //sweet-alert noitification for password changing
    function client_password_changed(data) {
        $("#client_password_update").modal("hide");
            if (data.success) {
                swal("Great!", data.message, "success");               
            } else {
                swal("Oops! Enter Password", data.message, "error");
            }
    }
    //sweet-alert noitification for transaction pin changing
    function transaction_pin_up(data){
        $("#trans_pin_update").modal("hide");
            if (data.success) {
                swal("Great!", data.message, "success");               
            } else {
                swal("Oops! Enter PIN", data.message, "error");
            }
    }

    //sweet-alert notification for comment submit
    function comment_by_admin(data){
        $("#comment-modal").modal("hide");
            if (data.success) {
                swal("Great!", data.message, "success");               
            } else {
                swal("Oops! Enter PIN", data.message, "error");
            }
    }


</script>

<script>
    // Bootstrap Markdown
    $(function() {
        $('#tinymce-editor').markdown({
            iconlibrary: 'fa',
            footer: '<div id="md-character-footer"></div><small id="md-character-counter" class="text-muted">350 character left</small>',

            onChange: function(e) {
                var contentLength = e.getContent().length;

                if (contentLength > 350) {
                    $('#md-character-counter')
                        .removeClass('text-muted')
                        .addClass('text-danger')
                        .html((contentLength - 350) + ' character surplus.');
                } else {
                    $('#md-character-counter')
                        .removeClass('text-danger')
                        .addClass('text-muted')
                        .html((350 - contentLength) + ' character left.');
                }
            },
        });

        // Update character counter
        $('#markdown').trigger('change');


        // *******************************************************************
        // Fix icons

        $('.md-editor .fa-header').removeClass('fa fa-header').addClass('fas fa-heading');
        $('.md-editor .fa-picture-o').removeClass('fa fa-picture-o').addClass('far fa-image');
    });
</script>

   
@endsection
