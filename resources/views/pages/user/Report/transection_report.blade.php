@extends("layouts.admin.app")

@section('per_page_css')
<link href="{{ asset('assets/admin/DatePicker/date-picker/css/bootstrap-datepicker.min.css') }}"  rel="stylesheet">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.11.5/css/jquery.dataTables.min.css"/>
<link rel="stylesheet" type="text/css" href="{{ asset('assets/admin/datatable/new-custom-datatable.css')}}"/>


<style>
    td.details-control {
      
        background:  url("{{ asset('assets/admin/datatable/icon/plus.png') }}") no-repeat center center;
        cursor: pointer;
    }
    tr.details td.details-control {
        background: url("{{ asset('assets/admin/datatable/icon/minus.png') }}") no-repeat center center;
    }

    a {
        color: rgb(24, 207, 115);
    }
</style>
@endsection


@section('body-content')


        <!-- [ content ] Start -->
        <div class="container-fluid flex-grow-1 container-p-y">
            <h4 class="font-weight-bold py-3 mb-0">TOKEN REPORTS</h4>
            <div class="row">

                <div class="col-md-12">
                    <div class="card d-flex w-100 mb-4">
                        <h6 class="card-header">TOKEN FILTER</h6>
                        <div class="row no-gutters row-bordered row-border-light h-100">
                            <div class="d-flex col-md-12 align-items-center">
                                <div class="card-body">
                                    <form id="filter_form" action="" method="get">
                                        <input type="hidden" name="op" value="export">
                                        <div class="form-row">
                                            <div class="col-md-3">
                                                <input id="txid" type="text" class="form-control" name="txid" placeholder="TXN Hash">
                                            </div><br><br>
                
                                            <div class="col-md-3">
                                                <input id="status" type="text" class="form-control" name="status" placeholder="Status">
                                            </div><br><br>
                
                                            <div class="col-md-3">

                                                <select Class="form-control" name="type" id="type">
                                                    <option value="">TYPE</option>
                                                    <option value="buy">BTC</option></option>
                                                    <option value="bfw">Tron</option>
                                                    <option value="tt">Solona</option>
                                                    <option value="et">Etherum</option>
                                                </select>
                                                {{-- <input id="method" type="text" class="form-control" name="method" placeholder="Method"> --}}
                                            </div><br><br>
                
                                            <div class="col-md-3">
                                                <input id="price" type="text" class="form-control" name="price" placeholder="Price">
                                            </div><br><br>
                
                                        </div>
                                        <div class="form-row">
                                            <div class="col-md-6">
                                                <div class="input-group" data-date="2017/01/01" data-date-format="yyyy/mm/dd">
                                                    <input id="from" type="text" class="form-control rounded dpd1 date-picker-input" name="from" placeholder="From Date">
                                                    <span class="px-3 py-2">To</span>
                                                    <input id="to" type="text" class="form-control rounded dpd2 date-picker-input" name="to" placeholder="To Date">
                                                </div>
                                            </div><br><br>
                
                                            <div class="col-3">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <button id="export_btn" type="submit" class="btn btn-primary btn-block">EXPORT</button>
                                                    </div><br><br>
                                                    <div class="col-md-6">
                                                        <button id="filterBtn" type="button" class="btn btn-success btn-block">FILTER</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                           
                        </div>
                    </div>
                </div>
            </div>


            <div class="row">

                <div class="col-md-12">
                    <div class="card d-flex w-100 mb-4">
                        <h6 class="card-header">TOKEN PURCHASE REPORTS</h6>
                        <div class="row no-gutters row-bordered row-border-light h-100">
                            <div class="d-flex col-md-12 align-items-center">
                                <div class="card-body">
                                    
                                    <table id="btc_tbl" class="display table table-hover table-custom" style="width:100%">
                                        <thead>
                                            <tr>
                                                <th></th>
                                                <th>NAME</th>
                                                <th>EMAIL</th>
                                                <th>TYPE</th>
                                                <th>PRICE</th>
                                                <th>STATUS</th>
                                                <th>Created At</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                           
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <!-- [ content ] End -->

@endsection


@section('per_page_js')
    <!--date picker-->
    
    <script src="{{ asset('assets/admin/DatePicker/date-picker/js/bootstrap-datepicker.min.js') }}"></script>
    <!--init date picker-->
    <script src="{{ asset('assets/admin/DatePicker/pickers/init-date-picker.js') }}"></script>
    <!-- common ajax -->
   
    <script src="{{ asset('assets/admin/js/common-ajax.js') }}"></script>
    {{-- <---Data_table_CDN----> --}}
    <script type="text/javascript" src="https://cdn.datatables.net/1.11.5/js/jquery.dataTables.min.js"></script>


    <script>

        
        function format (d) {
            //var btcurl="https://blockchair.com/bitcoin/transaction/";
            //var eturl="https://etherscan.io/tx/";
            var Hash=d.txid;
            return d.extra+'<br><b>Transaction Hash:</b> '+Hash+ 
            '<br> <a href="'+d.eturl+'" target="_blank">Check Transaction</a>'
            '<br> <a href="'+d.btcurl+'" target="_blank">Check Transaction</a>';
        }
    
        $(document).ready(function(){
    
            var dt = $('#btc_tbl').DataTable( {
                "processing": true,
                "serverSide": true,
                "ajax": {
                    "url": "{{url('user/transection-report?op=data_table')}}",
                    "data": function (d) {
                      return $.extend( {}, d, {
                        "txid": $("#txid").val(),
                        "status": $("#status").val(),
                        "type": $("#type").val(),
                        "price": $("#price").val(),
                        "from": $("#from").val(),
                        "to": $("#to").val(),
                      });
    
                    }
                },
    
                "columns": [
                    {
                        "class":          "details-control",
                        "orderable":      false,
                        "data":           null,
                        "defaultContent": ""
                    },
                    { "data": "name" },
                    { "data": "email" },
                    { "data": "type" },
                    { "data": "price" },
                    { "data": "status" },
                    { "data": "created_at" },
                ],
                
                "drawCallback": function( settings ) {
                    $("#filterBtn").html("FILTER");
                }
               
            });
    
            // Array to track the ids of the details displayed rows
            var detailRows = [];
         
            $('#btc_tbl tbody').on( 'click', 'tr td.details-control', function(){
                var tr = $(this).closest('tr');
                var row = dt.row( tr );
                var idx = $.inArray( tr.attr('id'), detailRows);
         
                if ( row.child.isShown()){
                    tr.removeClass('details');
                    row.child.hide();
         
                    // Remove from the 'open' array
                    detailRows.splice(idx, 1);
                }
                else {
                    tr.addClass( 'details' );
                    row.child( format(row.data())).show();
         
                    // Add to the 'open' array
                    if (idx === -1) {
                        detailRows.push(tr.attr('id'));
                    }
                }
            });
         
            // On each draw, loop over the `detailRows` array and show any child rows
            dt.on( 'draw', function(){
                $.each( detailRows, function (i, id){
                    $('#'+id+' td.details-control').trigger('click');
                });
            });
    
            $('#filterBtn').click(function (e) {
                $(this).html("<i class='fa fa-refresh fa-spin fa-1x fa-fw'></i>");
                dt.draw();
            });
    
        });
        
    </script>

   
@endsection
