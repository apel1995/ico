@extends("layouts.admin.app")

@section('per_page_css')
    <!-- style for datatable and sellector -->
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.11.5/css/jquery.dataTables.min.css" />
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/admin/datatable/new-custom-datatable.css') }}" />
    <link rel="stylesheet" href="{{ asset('assets/admin/libs/bootstrap-select/bootstrap-select.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/admin/libs/bootstrap-multiselect/bootstrap-multiselect.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/admin/libs/select2/select2.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/admin/libs/bootstrap-tagsinput/bootstrap-tagsinput.css') }}">

    <!-- style for datepicker -->
    <link rel="stylesheet" href="{{ asset('assets/admin/libs/bootstrap-datepicker/bootstrap-datepicker.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/admin/libs/bootstrap-daterangepicker/bootstrap-daterangepicker.css') }}">
    <link rel="stylesheet"
        href="{{ asset('assets/admin/libs/bootstrap-material-datetimepicker/bootstrap-material-datetimepicker.css') }}">
    <link rel="stylesheet"
        href="{{ asset('assets/admin/libs/bootstrap-material-datetimepicker/bootstrap-material-datetimepicker.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/admin/libs/timepicker/timepicker.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/admin/libs/minicolors/minicolors.css') }}">

    <style>
        td.details-control {

            background: url("{{ asset('assets/admin/datatable/icon/plus.png') }}") no-repeat center center;
            cursor: pointer;
        }

        tr.details td.details-control {
            background: url("{{ asset('assets/admin/datatable/icon/minus.png') }}") no-repeat center center;
        }


        /* Content of modal div is center aligned */
        .modal-dialog-full-width {
            width: 100% !important;
            height: 100% !important;
            margin: 0 !important;
            padding: 0 !important;
            max-width: none !important;

        }

        .modal-content-full-width {
            height: auto !important;
            min-height: 100% !important;
            border-radius: 0 !important;
            background-color: #ffffff !important
        }

        .modal-header-full-width {
            border-bottom: 1px solid #ffffff !important;
        }

        .modal-footer-full-width {
            border-top: 1px solid #928c8c !important;
        }

        .img-responsive {

            margin: 0 auto;
            width: 535px;
            height: 550px;
        }

    </style>
@endsection


@section('body-content')
    <!-- [ Layout navbar ( Header ) ] End -->

    <!-- [ Layout content ] Start -->
    <div class="layout-content">

        <!-- [ content ] Start -->
        <div class="container-fluid flex-grow-1 container-p-y">
            <h4 class="font-weight-bold py-3 mb-0">Selects and tags</h4>
            <div class="text-muted small mt-0 mb-4 d-block breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#"><i class="feather icon-home"></i></a></li>
                    <li class="breadcrumb-item">Forms</li>
                    <li class="breadcrumb-item active">Selects and tags</li>
                </ol>
            </div>
            {{-- code by uchchwas --}}
            {{-- <div class=" card container">
                            <div class = "row">
                                {{-- col 1 --}}
            {{-- <div class = "col-md-4">
                                    <div class="card-body demo-vertical-spacing">
                                        <select class="selectpicker" data-style="btn-default">
                                            <option>ALL</option>
                                            <option>GAS BILL</option>
                                            <option>UTILITY BILL</option>
                                            <option>ELECTRIC BILL</option>
                                            <option>TELEPHONE BILL</option>
                                            <option>CREDIT CARD BILL</option>
                                            <option>CREDIT CARD STATEMENT</option>
                                            <option>BANK STATEMENT</option>
                                            <option>BANK CRETIFICATE</option>
                                            <option>PASSPORT COPY</option>
                                            <option>NATIONAL ID COPY</option>
                                            <option>DIRIVING LICENSE</option>
                                        
                                        </select>
                                    </div>
                                </div> --}}
            {{-- col 2 --}}
            {{-- <div class = "form-group ">
                                    <div class="col-md-4">
                                        <input type="email" class="form-control" placeholder="Email">
                                        <div class="clearfix"></div>
                                    </div>
                                </div> --}}
            {{-- col 3 --}}
            {{-- <div class = "col-md-4">
                                    <div class="card-body demo-vertical-spacing">
                                        <select class="selectpicker" data-style="btn-default">
                                            <option>Mustard</option>
                                            <option>Ketchup</option>
                                            <option>Relish</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div> --}}

            <div class="card mb-4">
                <h6 class="card-header">Filter Report</h6>
                <div class="card-body">
                    <form>
                        <div class="form-row">
                            <div class="form-group col-md-4 demo-vertical-spacing">
                                <select id="doc_type" name="doc_type" class="selectpicker" data-style="btn-default">
                                    <option value="">ALL</option>
                                    <option value="gas_bill">GAS BILL</option>
                                    <option value="utility_bill">UTILITY BILL</option>
                                    <option value="electric_bill">ELECTRIC BILL</option>
                                    <option value="telephone_bill">TELEPHONE BILL</option>
                                    <option value="credit_bill">CREDIT CARD BILL</option>
                                    <option value="credit_statement">CREDIT CARD STATEMENT</option>
                                    <option value="bank_statement">BANK STATEMENT</option>
                                    <option value="bank_certificate">BANK CERTIFICATE</option>
                                    <option value="passport">PASSPORT COPY</option>
                                    <option value="national_id">NATIONAL ID COPY</option>
                                    <option value="driving_license">DIRIVING LICENSE</option>

                                </select>
                            </div>
                            <div class="form-group col-md-4">
                                {{-- <label class="form-label">Email</label> --}}
                                <input type="text" id="client_name" name="client_name" class="form-control"
                                    placeholder="Name/Email/Subcode" data-toggle="tooltip" data-placement="top"
                                    title="Search By Client">
                                <div class="clearfix"></div>
                            </div>
                            <div class="input-daterange input-group form-group col-md-4" id="datepicker-range">
                                {{-- <label class="form-label">Password</label> --}}
                                <input type="text" class="form-control" id="created_at" name="created_at"
                                    data-toggle="tooltip" data-placement="top" title="Submit Date">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">to</span>
                                </div>
                                <input type="text" class="form-control" id="created_at" name="created_at"
                                    data-toggle="tooltip" data-placement="top" title="Submit Date">
                                <div class="clearfix"></div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-4 demo-vertical-spacing">
                                <select id="status" name="status" class="selectpicker" data-style="btn-default">
                                    <option value="">All</option>
                                    <option value="approved">APPROVED</option>
                                    <option value="pending">PENDING</option>
                                    <option value="decline">DECLINED</option>
                                </select>
                            </div>
                            <div class="input-daterange input-group form-group col-md-4" id="datepicker-range">
                                {{-- <label class="form-label">Password</label> --}}
                                <input type="text" class="form-control"  name="issue_date_srt" id="issue_date_srt"
                                    data-toggle="tooltip" data-placement="top" title="Issue Date">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">to</span>
                                </div>
                                <input type="text" class="form-control" name="issue_date_end" id="issue_date_end"
                                     data-toggle="tooltip" data-placement="top"
                                    title="Issue Date">
                                <div class="clearfix"></div>
                            </div>
                            <div class="input-daterange input-group form-group col-md-4" id="datepicker-range">
                                {{-- <label class="form-label">Password</label> --}}
                                <input type="text" class="form-control" name="exp_date" id="exp_date"
                                    data-toggle="tooltip" data-placement="top" title="Expire Date">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">to</span>
                                </div>
                                <input type="text" class="form-control" name="exp_date" id="exp_date"
                                    data-toggle="tooltip" data-placement="top" title="Expire Date">
                                <div class="clearfix"></div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-4 demo-vertical-spacing">
                                <select id="client_type" name="client_type" class="selectpicker" data-style="btn-default">
                                    <option value="">All</option>
                                    <option value="trader">TRADER</option>
                                    <option value="ib">IB</option>
                                </select>
                            </div>
                            <div class="form-group col-md-4">
                                {{-- <label class="form-label">Email</label> --}}
                                <input type="text" name="email" id="email" class="form-control"
                                    placeholder="Account/Desk Manager Name or Email" data-toggle="tooltip"
                                    data-placement="top" title="Search By Account/Desk Manager">
                                <div class="clearfix"></div>
                            </div>
                            <div class="form-group demo-inline-spacing col-md-4 d-flex justify-content-center">
                                <button id="export_btn" type="submit" class="btn btn-primary btn-block">EXPORT</button>
                                <button id="filterBtn" type="button" class="btn btn-success btn-block">FILTER</button>
                            </div>
                        </div>




                    </form>
                </div>
            </div>


            <!-- DataTable within card -->


            <div class="row">

                <div class="col-md-12">
                    <div class="card d-flex w-100 mb-4">
                        <h6 class="card-header">KYC REPORTS</h6>
                        <div class="row no-gutters row-bordered row-border-light h-100">
                            <div class="d-flex col-md-12 align-items-center">
                                <div class="card-body">
                                    <table id="btc_tbl" class="display table table-hover table-custom" style="width:100%">
                                        <thead>
                                            <tr>
                                                <th>client_name</th>
                                                <th>Client Type </th>
                                                <th>Document Type</th>
                                                <th>Issue Date</th>
                                                <th>Expire Date</th>
                                                <th>Status</th>
                                                <th>Date</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

        </div>



       
        <!-- DataTable within card -->
        {{-- end code --}}









    </div>
    <!-- [ content ] End -->



    

    {{-- <button type="submit" data-optype="approve" data-userid="3" value="1" class="btn btn-success btn-md btn-rounded">Approve</button> --}}

    <!-- Modal -->
    <!--Bootstrap modal -->




    <!-- Modal -->
    <div class="modal fade " id="modalUpdateProfile" tabindex="-1" role="dialog"
        aria-labelledby="exampleModalPreviewLabel" aria-hidden="true">
        <div class="modal-dialog modal-xl" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalPreviewLabel">Are you sure?</h5>
                    <button type="button" class="close " data-dismiss="modal" aria-label="Close">
                        <span style="font-size: 1.3em;" aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">

                    <div class="row">
                        <div class="col-md-6" id="user-img">


                        </div>

                        <div class="col-md-6">
                            {{-- <input type="text" class="" id="cname" name="client_name" value=""> --}}

                            <ul class="list-group list-group-flush">


                                <li class="list-group-item">Email : <span id="_email"> today</span></li>
                                <li class="list-group-item">Country : <span id="_country"></span></li>
                                <li class="list-group-item">Address : <span id="_address"></span></li>
                                <li class="list-group-item">State : <span id="_state"></span></li>
                                <li class="list-group-item">Phone : <span id="_phone"></span></li>
                                <li class="list-group-item">Zip : <span id="_zip"></span></li>
                                <li class="list-group-item">Date Of Birth : <span id="_bithday"></span></li>
                                <li class="list-group-item">Status : <span id="_status"></span></li>
                            </ul>
                            <br><br><br>
                            <ul class="list-group list-group-flush">
                                <li class="list-group-item">Name : <span id="c_name"></span></li>
                                <li class="list-group-item">Issue Date : <span id="_issu_date"></span></li>
                                <li class="list-group-item">Expire Date : <span id="_exp_date"></span></li>
                                <li class="list-group-item">Document Type : <span id="_doc_type"></span></li>
                                <li class="list-group-item">Issuer Country : <span id="_country1"></span></li>
                            </ul>
                        </div>
                    </div>


                </div>
                <div class="modal-footer">

                    <form action="{{ url('admin/report/update/approve') }}" id="approve_modal_form" method="post">
                        @csrf
                        <input type="hidden" id="c_id" name="c_id" class="form-control">
                        <input type="hidden" id="is_confirmed" name="is_confirmed" value="">
                        <button type="button" id="approve_btn" data-btnid="approve_btn" data-form="approve_modal_form"
                            data-validator="true" data-loading="processing..." data-callback="approveModalCallback"
                            onclick="_run(this)" class="btn btn-success btn-md btn-rounded">Approve</button>
                    </form>
                    <form action="{{ url('admin/report/update/decline') }}" id="decline_modal_form" method="post"
                        class="ajax-form">
                        @csrf
                        <input type="hidden" id="c_d_id" name="c_d_id" class="form-control">
                        <input type="hidden" id="is_decline" name="is_decline" value="">
                        <button type="button" id="decline_btn" data-btnid="decline_btn" data-form="decline_modal_form"
                            data-validator="true" data-loading="ddddrocessing..." data-callback="declineModalCallback"
                            onclick="_run(this)" class="btn btn-info btn-md btn-rounded">Decline</button>
                    </form>

                    <button type="button" class="btn btn-danger btn-md btn-rounded" data-dismiss="modal">Close</button>



                </div>
            </div>
        </div>
    </div>

    {{-- {{ route('admin.report.update),$id') }} --}}
    {{-- url('tourpackagedetails/'. $p->id) --}}
    <!-- Modal end -->
    <!-- update modal start -->
    <!-- Modal -->
    <div class="modal fade" id="updatekyc" tabindex="-1" role="dialog" aria-labelledby="exampleModalPreviewLabel"
        aria-hidden="true">
        <div class="modal-dialog">
            <form action="{{ url('admin/report/update') }}" id="" method="post" class="ajax-form modal-content"
                enctype="multipart/form-data">
                @csrf

                <div class="modal-header">
                    <h5 class="modal-title">Update Profile
                        {{-- <span class="font-weight-light">Information</span> --}}
                        <br>
                        {{-- <small class="text-muted">We need payment information to process your order.</small> --}}
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">×</button>
                </div>
                <div class="modal-body">

                    <input type="hidden" id="user_id" name="user_id" class="form-control">
                    <div class="form-row">
                        <div class="form-group col mb-0">
                            <label class="form-label">Full Name</label>
                            <input type="text" id="up_name" name="up_name" class="form-control" placeholder="Demo User">
                            <div class="clearfix"></div>
                        </div>
                        <div class="form-group col mb-0">
                            <label class="form-label">Country</label>
                            <select id="up_country" name="up_country" class="selectpicker" data-style="btn-default">
                                <option value="Andorra">Andorra</option>
                                <option value="United Arab Emirates">United Arab Emirates</option>
                                <option value="Afghanistan">Afghanistan</option>
                                <option value="Antigua and Barbuda">Antigua and Barbuda</option>
                                <option value="Albania">Albania</option>
                                <option value="Angola">Angola</option>
                                <option value="Antarctica">Antarctica</option>
                                <option value="Argentina">Argentina</option>
                                <option value="Austria">Austria</option>
                                <option value="Australia">Australia</option>
                                <option value="Azerbaijan">Azerbaijan</option>
                                <option value="Bosnia and Herzegovina">Bosnia and Herzegovina</option>
                                <option value="Barbados">Barbados</option>
                                <option value="Bangladesh">Bangladesh</option>
                                <option value="Belgium">Belgium</option>
                                <option value="Burkina Faso">Burkina Faso</option>
                                <option value="Bulgaria">Bulgaria</option>
                                <option value="Bahrain">Bahrain</option>
                                <option value="Benin">Benin</option>
                                <option value="Brunei Darussalam">Brunei Darussalam</option>
                                <option value="Bolivia">Bolivia</option>
                                <option value="Brazil">Brazil</option>
                                <option value="Bahamas">Bahamas</option>
                                <option value="Bhutan">Bhutan</option>
                                <option value="Botswana">Botswana</option>
                                <option value="Belarus">Belarus</option>
                                <option value="Belize">Belize</option>
                                <option value="Canada">Canada</option>
                                <option value="Central African Republic">Central African Republic</option>
                                <option value="Congo">Congo</option>
                                <option value="Switzerland">Switzerland</option>
                                <option value="Cote D'Ivoire">Cote D'Ivoire</option>
                                <option value="Chile">Chile</option>
                                <option value="Cameroon">Cameroon</option>
                                <option value="China">China</option>
                                <option value="Colombia">Colombia</option>
                                <option value="Costa Rica">Costa Rica</option>
                                <option value="Cuba">Cuba</option>
                                <option value="Cape Verde">Cape Verde</option>
                                <option value="Cyprus">Cyprus</option>
                                <option value="Czech Republic">Czech Republic</option>
                                <option value="Germany">Germany</option>
                                <option value="Djibouti">Djibouti</option>
                                <option value="Denmark">Denmark</option>
                                <option value="Dominica">Dominica</option>
                                <option value="Dominican Republic">Dominican Republic</option>
                                <option value="Algeria">Algeria</option>
                                <option value="Ecuador">Ecuador</option>
                                <option value="Estonia">Estonia</option>
                                <option value="Egypt">Egypt</option>
                                <option value="Eritrea">Eritrea</option>
                                <option value="Spain">Spain</option>
                                <option value="Ethiopia">Ethiopia</option>
                                <option value="Finland">Finland</option>
                                <option value="Fiji">Fiji</option>
                                <option value="Falkland Islands (Malvinas)">Falkland Islands (Malvinas)</option>
                                <option value="Micronesia, Federated States of">Micronesia, Federated States of</option>
                                <option value="France">France</option>
                                <option value="Gabon">Gabon</option>
                                <option value="Grenada">Grenada</option>
                                <option value="Georgia">Georgia</option>
                                <option value="Ghana">Ghana</option>
                                <option value="Greenland">Greenland</option>
                                <option value="Gambia">Gambia</option>
                                <option value="Guinea">Guinea</option>
                                <option value="Equatorial Guinea">Equatorial Guinea</option>
                                <option value="Greece">Greece</option>
                                <option value="Guatemala">Guatemala</option>
                                <option value="Guam">Guam</option>
                                <option value="Guinea-Bissau">Guinea-Bissau</option>
                                <option value="Guyana">Guyana</option>
                                <option value="Hong Kong">Hong Kong</option>
                                <option value="Honduras">Honduras</option>
                                <option value="Croatia">Croatia</option>
                                <option value="Haiti">Haiti</option>
                                <option value="Hungary">Hungary</option>
                                <option value="Indonesia">Indonesia</option>
                                <option value="Ireland">Ireland</option>
                                <option value="Israel">Israel</option>
                                <option value="India">India</option>
                                <option value="Iraq">Iraq</option>
                                <option value="Iran, Islamic Republic of">Iran, Islamic Republic of</option>
                                <option value="Iceland">Iceland</option>
                                <option value="Italy">Italy</option>
                                <option value="Jamaica">Jamaica</option>
                                <option value="Jordan">Jordan</option>
                                <option value="Japan">Japan</option>
                                <option value="Kenya">Kenya</option>
                                <option value="Kyrgyzstan">Kyrgyzstan</option>
                                <option value="Cambodia">Cambodia</option>
                                <option value="Kiribati">Kiribati</option>
                                <option value="Comoros">Comoros</option>
                                <option value="Korea, Democratic People's Republic of">Korea, Democratic People's Republic
                                    of</option>
                                <option value="Korea, Republic of">Korea, Republic of</option>
                                <option value="Kuwait">Kuwait</option>
                                <option value="Kazakhstan">Kazakhstan</option>
                                <option value="Lao People's Democratic Republic">Lao People's Democratic Republic</option>
                                <option value="Lebanon">Lebanon</option>
                                <option value="Saint Lucia">Saint Lucia</option>
                                <option value="Liechtenstein">Liechtenstein</option>
                                <option value="Sri Lanka">Sri Lanka</option>
                                <option value="Liberia">Liberia</option>
                                <option value="Lesotho">Lesotho</option>
                                <option value="Lithuania">Lithuania</option>
                                <option value="Luxembourg">Luxembourg</option>
                                <option value="Latvia">Latvia</option>
                                <option value="Libyan Arab Jamahiriya">Libyan Arab Jamahiriya</option>
                                <option value="Morocco">Morocco</option>
                                <option value="Moldova, Republic of">Moldova, Republic of</option>
                                <option value="Madagascar">Madagascar</option>
                                <option value="Marshall Islands">Marshall Islands</option>
                                <option value="Macedonia, the Former Yugoslav Republic of">Macedonia, the Former Yugoslav
                                    Republic of</option>
                                <option value="Mali">Mali</option>
                                <option value="Mongolia">Mongolia</option>
                                <option value="Mauritania">Mauritania</option>
                                <option value="Malta">Malta</option>
                                <option value="Mauritius">Mauritius</option>
                                <option value="Maldives">Maldives</option>
                                <option value="Malawi">Malawi</option>
                                <option value="Mexico">Mexico</option>
                                <option value="Malaysia">Malaysia</option>
                                <option value="Mozambique">Mozambique</option>
                                <option value="Namibia">Namibia</option>
                                <option value="Niger">Niger</option>
                                <option value="Nigeria">Nigeria</option>
                                <option value="Nicaragua">Nicaragua</option>
                                <option value="Netherlands">Netherlands</option>
                                <option value="Norway">Norway</option>
                                <option value="Nepal">Nepal</option>
                                <option value="Nauru">Nauru</option>
                                <option value="New Zealand">New Zealand</option>
                                <option value="Oman">Oman</option>
                                <option value="Panama">Panama</option>
                                <option value="Peru">Peru</option>
                                <option value="Papua New Guinea">Papua New Guinea</option>
                                <option value="Philippines">Philippines</option>
                                <option value="Pakistan">Pakistan</option>
                                <option value="Poland">Poland</option>
                                <option value="Palestinian Territory, Occupied">Palestinian Territory, Occupied</option>
                                <option value="Portugal">Portugal</option>
                                <option value="Paraguay">Paraguay</option>
                                <option value="Qatar">Qatar</option>
                                <option value="Romania">Romania</option>
                                <option value="Russian Federation">Russian Federation</option>
                                <option value="Rwanda">Rwanda</option>
                                <option value="Saudi Arabia">Saudi Arabia</option>
                                <option value="Solomon Islands">Solomon Islands</option>
                                <option value="Seychelles">Seychelles</option>
                                <option value="Sudan">Sudan</option>
                                <option value="Sweden">Sweden</option>
                                <option value="Singapore">Singapore</option>
                                <option value="Slovenia">Slovenia</option>
                                <option value="Slovakia">Slovakia</option>
                                <option value="Sierra Leone">Sierra Leone</option>
                                <option value="Senegal">Senegal</option>
                                <option value="Somalia">Somalia</option>
                                <option value="Suriname">Suriname</option>
                                <option value="Sao Tome and Principe">Sao Tome and Principe</option>
                                <option value="El Salvador">El Salvador</option>
                                <option value="Syrian Arab Republic">Syrian Arab Republic</option>
                                <option value="Swaziland">Swaziland</option>
                                <option value="Chad">Chad</option>
                                <option value="Togo">Togo</option>
                                <option value="Thailand">Thailand</option>
                                <option value="Tajikistan">Tajikistan</option>
                                <option value="Turkmenistan">Turkmenistan</option>
                                <option value="Tunisia">Tunisia</option>
                                <option value="Tonga">Tonga</option>
                                <option value="Turkey">Turkey</option>
                                <option value="Trinidad and Tobago">Trinidad and Tobago</option>
                                <option value="Tuvalu">Tuvalu</option>
                                <option value="Taiwan, Province of China">Taiwan, Province of China</option>
                                <option value="Tanzania, United Republic of">Tanzania, United Republic of</option>
                                <option value="Ukraine">Ukraine</option>
                                <option value="Uganda">Uganda</option>
                                <option value="United Kingdom">United Kingdom</option>
                                <option value="United States">United States</option>
                                <option value="Uruguay">Uruguay</option>
                                <option value="Uzbekistan">Uzbekistan</option>
                                <option value="Holy See (Vatican City State)">Holy See (Vatican City State)</option>
                                <option value="Saint Vincent and the Grenadines">Saint Vincent and the Grenadines</option>
                                <option value="Venezuela">Venezuela</option>
                                <option value="Viet Nam">Viet Nam</option>
                                <option value="Vanuatu">Vanuatu</option>
                                <option value="Samoa">Samoa</option>
                                <option value="Yemen">Yemen</option>
                                <option value="South Africa">South Africa</option>
                                <option value="Zambia">Zambia</option>
                                <option value="Zimbabwe">Zimbabwe</option>
                            </select>
                            {{-- <input type="text" id = "up_country" name = "up_country" class="form-control" placeholder="China"> --}}
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col mb-0">
                            <label class="form-label">Zip</label>
                            <input type="text" id="up_zip" name="up_zip" class="form-control" placeholder="7051">
                            <div class="clearfix"></div>
                        </div>
                        <div class="form-group col mb-0">
                            <label class="form-label">State</label>
                            <input type="text" id="up_state" name="up_state" class="form-control" placeholder="AA">
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col mb-0">
                            <label class="form-label">Issue Date</label>
                            <input type="text" id="up_issu_date" name="up_issu_date" class="form-control"
                                placeholder="DD/MM/YYYY">
                            <div class="clearfix"></div>

                        </div>
                        <div class="form-group col mb-0">
                            <label class="form-label">Expiry Date</label>
                            <input type="text" id="up_exp_date" name="up_exp_date" class="form-control"
                                placeholder="DD/MM/YYYY">
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col mb-0">
                            <label class="form-label">City</label>
                            <input type="text" id="up_city" name="up_city" class="form-control" placeholder="Beijing">
                            <div class="clearfix"></div>
                        </div>
                        <div class="form-group col mb-0">
                            <label class="form-label">Date Of Birth</label>
                            <input type="text" id="up_bithday" name="up_bithday" class="form-control"
                                placeholder="DD/MM/YYYY">
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col">
                            <label class="form-label">Address</label>
                            <input type="textarea" id="up_address" name="up_address" class="form-control"
                                placeholder="Ching Chong Street">
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Update</button>
                    {{-- <button type="submit"  id="submitBtn"  class="btn btn-primary" data-btnid="submitBtn">Save</button> --}}
                </div>
            </form>
        </div>
    </div>





    <!-- Modal end -->
    <!-- update modal end -->
@endsection


@section('per_page_js')
    <!--date picker-->
    <script src="{{ asset('assets/admin/DatePicker/date-picker/js/bootstrap-datepicker.min.js') }}"></script>
    <!--init date picker-->
    <script src="{{ asset('assets/admin/DatePicker/pickers/init-date-picker.js') }}"></script>
    <!-- common ajax -->

    <script src="{{ asset('assets/admin/js/common-ajax.js') }}"></script>
    {{-- <---Data_table_CDN----> --}}
    <script type="text/javascript" src="https://cdn.datatables.net/1.11.5/js/jquery.dataTables.min.js"></script>

    <!-- select button requ files -->
    <script src="{{ asset('assets/admin/libs/bootstrap-select/bootstrap-select.js') }}"></script>
    <script src="{{ asset('assets/admin/libs/bootstrap-multiselect/bootstrap-multiselect.js') }}"></script>
    <script src="{{ asset('assets/admin/libs/select2/select2.js') }}"></script>
    <script src="{{ asset('assets/admin/libs/bootstrap-tagsinput/bootstrap-tagsinput.js') }}"></script>
    <script src="{{ asset('assets/admin/js/pages/forms_selects.js') }}"></script>

    <!-- script for datepicker -->
    <script src="{{ asset('assets/admin/libs/moment/moment.js') }}"></script>
    <script src="{{ asset('assets/admin/libs/bootstrap-datepicker/bootstrap-datepicker.js') }}"></script>
    <script src="{{ asset('assets/admin/libs/bootstrap-daterangepicker/bootstrap-daterangepicker.js') }}"></script>
    <script
        src="{{ asset('assets/admin/libs/bootstrap-material-datetimepicker/bootstrap-material-datetimepicker.js') }}">
    </script>
    <script src="{{ asset('assets/admin/libs/timepicker/timepicker.js') }}"></script>
    <script src="{{ asset('assets/admin/libs/minicolors/minicolors.js') }}"></script>
    <script src="{{ asset('assets/admin/js/pages/forms_pickers.js') }}"></script>

    <!-- script for ajax form submit -->
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script src="{{ asset('assets/admin/js/jquery.validate.js') }}"></script>
    <script src="{{ asset('assets/admin/js/ajax_form_submit.js') }}"></script>


    <script>
        function format(d) {
            return '<b>Transaction Hash:</b> ' + d.txn_hash;
        }

        function clk() {
            $(this).attr("data-id");
            console.log(data - id);
        }
        $(document).ready(function() {
            var dt = $('#btc_tbl').DataTable({
                "processing": true,
                "serverSide": true,
                "ajax": {
                    "url": "{{ url('admin/report/Kyc_request?op=data_table') }}",
                    "data": function(d) {
                        return $.extend({}, d, {
                            "client_name": $("#client_name").val(),
                            "client_type": $("#client_type").val(),
                            "issue_date": $("#issue_dat").val(),
                            "issue_date": $("#issue_date").val(),
                            "exp_date": $("#exp_date").val(),
                            "doc_type": $("#doc_type").val(),
                            "status": $("#status").val(),
                            "email": $("#email").val(),
                            "created_at": $("#created_at").val(),

                        });

                    }
                },

                "columns": [
                    // {
                    //     // "class":          "details-control",
                    //     "orderable":      false,
                    //     "data":           null,
                    //     "defaultContent": ""
                    // },

                    {
                        "data": "client_name"
                    },
                    {
                        "data": "client_type"
                    },
                    {
                        "data": "doc_type"
                    },
                    {
                        "data": "issu_date"
                    },
                    {
                        "data": "exp_date"
                    },
                    {
                        "data": "status"
                    },
                    {
                        "data": "created_at"
                    },
                    {
                        "data": "action"
                    },

                ],
                "drawCallback": function(settings) {
                    $("#filterBtn").html("FILTER");
                }

            });


            $('#filterBtn').click(function(e) {
                $(this).html("<i class='fa fa-refresh fa-spin fa-1x fa-fw'></i>");
                dt.draw();
            });

        });
        $('button').click(function() {
            console.log($("a").data("id"));
        });
    </script>




    <script type="text/javascript">
        var vtid = 0;
        var op = 'verify_ad';
        var ct = 'ar';
        var va = 0;

        function process(e) {
            var obj = $(e);
            var bt = $(e).html();

            var status = obj.data('status');

            var st = 'Verified';
            if (status == 2) {
                st = 'Declined!';
            }

            if (ct == 'ib') {
                op = 'verify_ad_ib';
            } else {
                op = 'verify_ad';
            }

            obj.html('<i class="fa fa-spinner fa-pulse"></i>');

            $.ajax({
                url: "ar_clients_process.php?id=" + vtid + "&status=" + status + "&op=" + op,
                type: 'GET',
                data: {
                    note: $("#note").val(),
                    va: va
                },
                dataType: 'json',
                success: function(data) {
                    obj.html(bt);

                    if (data.success) {

                        if (status == 2) {
                            $("#modalDeclineWarning").modal("hide");
                        }
                        $("#group").modal("hide");
                        notify('success', data.message);
                        dt.ajax.reload(null, false);
                    } else {
                        notify('error', data.message);
                    }

                }
            });
        }

        function notify(type, message) {
            new PNotify({
                delay: 5000,
                title: 'Notification',
                text: message,
                addclass: 'annouce',
                type: type,
                remove: true
            });
        }

        function modalOpen(e) {
            var obj = $(e);
            $("#group").modal("hide");
            $("#" + obj.data('modal')).modal('show');

        }

        function modalClose(e) {
            var obj = $(e);
            $("#" + obj.data('modal')).modal('hide');
        }


        function madal_oc(e) {
            var obj = $(e);
            $(".ios-switch").removeClass('on');
            $(".ios-switch").addClass('off');
            $(".va").prop("checked", false);
            va = 0;
            $("#" + obj.data('modal')).modal(obj.data('mt'));
            $(".overlay_ar").fadeIn();
            if (obj.data('mt') == 'show') {
                vtid = obj.data('id');
                ct = obj.data('ct');
                $("#bank_proof").load('id_watch.php?id_file_name=' + obj.data('file') + '&ct=' + ct + '&tid=' + vtid,
                    function() {
                        $(".overlay_ar").fadeOut();
                        $(".pb").hide();
                        $(".switchbox").hide();
                        if ($("#hstatus").val() == 0) {
                            $(".pb").show();
                            $(".switchbox").show();
                        }
                    });
            }
        }

        $(".va").change(function() {
            if ($(this).prop('checked') == true) {
                va = 1;
            } else {
                va = 0;
            }
        });



        // KEC Form Update
        function createCallBack(data) {
            $('#submitBtn').prop('disabled', false);
            if (data.success) {
                notify('success', data.message);
                $("#modalUpdateProfile").modal("hide");
                dt.draw();
            } else {
                notify('error', data.message);
                $.validator("kyc-update-form", data.errors);
            }
        }


        function user_details_view(e) {
            let obj = $(e);

            $("#modalUpdateProfile").modal("show");
            // Append Values On Form
            $('#c_id').val(obj.data('id'));
            $('#c_d_id').val(obj.data('id'));
            $('#is_confirmed').val(obj.data('status'));
            $('#is_decline').val(obj.data('status'));
            $('#c_name').text(obj.data('client_name'));
            $('#_email').text(obj.data('email'));
            $('#_country').text(obj.data('country'));
            $('#_address').text(obj.data('address'));
            $('#_city').text(obj.data('city'));
            $('#_state').text(obj.data('state'));
            $('#_phone').text(obj.data('phone'));
            $('#_zip').text(obj.data('zip'));
            $('#_bithday').text(obj.data('bithday'));
            $('#_issu_date').text(obj.data('issu_date'));
            $('#_exp_date').text(obj.data('exp_date'));
            $('#_doc_type').text(obj.data('doc_type'));
            $('#_country1').text(obj.data('country'));
            $('#_status').text(obj.data('status'));
            $('#images').html(obj.data('images'));
            // document.getElementById("myImg").src='{{ asset(".'obj.data('images')'.") }}';
            $("#user-img").html('<img class="img-responsive" id="imgPreview" src="{{ asset('uploads/') }}/' + obj.data(
                'images') + '">');
            // document.getElementById("myImg").src='{{ asset("'+obj.data('images')+'") }}' 
            // $('.myClass').on('mouseenter', {param1: val1}, onMouseOver).on('mouseleave', {param2: val2}, onMouseOut);
            //  if ($("#is_confirmed").val() != null) {
            //     $("#approve_btn").removeAttr('disabled');
            //  } 
            //  if ($("#is_decline").val() != null) {
            //     $("#decline_btn").removeAttr('disabled');
            //  }

            if ($("#is_confirmed").val() == 'approved') {
                // $("#approve_btn").attr('disabled', true);
                // $("#decline_btn").attr('disabled', true);
                $("#approve_btn").hide();
                $("#decline_btn").hide();
            }



            if ($("#is_decline").val() == 'decline') {
                // $("#approve_btn").attr('disabled', true);
                // $("#decline_btn").attr('disabled', true);
                $("#approve_btn").hide();
                $("#decline_btn").hide();
            }

            // console.log(obj.data('images'));

        }

        function update_profile(e) {
            let obj = $(e);

            $("#updatekyc").modal("show");
            $('#user_id').val(obj.data('id'));
            $('#up_name').val(obj.data('client_name'));
            $('#up_country').val(obj.data('country'));
            $('#up_address').val(obj.data('address'));
            $('#up_city').val(obj.data('city'));
            $('#up_state').val(obj.data('state'));
            $('#up_zip').val(obj.data('zip'));
            $('#up_bithday').val(obj.data('bithday'));
            $('#up_issu_date').val(obj.data('issu_date'));
            $('#up_exp_date').val(obj.data('exp_date'));
            $('#up_status').val(obj.data('status'));


        }

        function approveModalCallback(data) {
            if (data.success) {
                swal("Great!", "User Approved Sucessfully!", "success");
                $("#approve_btn").hide();
                $("#decline_btn").hide();
            } else {
                swal("Oops!", "User Approval Unsuccessful!", "error");
            }
        }

        function declineModalCallback(data) {
            if (data.success) {
                swal("Great!", "User Decline Sucessfully!", "success");
                $("#approve_btn").hide();
                $("#decline_btn").hide();
            } else {
                swal("Oops!", "User Decline Unsuccessful!", "error");
            }

        }

        //tooltip js for everywhere.
        $(document).ready(function() {
            $('[data-toggle="tooltip"]').tooltip();
        });
    </script>
@endsection
