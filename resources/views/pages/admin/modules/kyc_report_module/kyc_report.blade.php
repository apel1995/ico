@extends("layouts.admin.app")

@section('per_page_css')
    <!-- style for datatable and sellector -->
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.11.5/css/jquery.dataTables.min.css" />
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/admin/datatable/new-custom-datatable.css') }}" />
    <link rel="stylesheet" href="{{ asset('assets/admin/libs/bootstrap-select/bootstrap-select.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/admin/libs/bootstrap-multiselect/bootstrap-multiselect.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/admin/libs/select2/select2.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/admin/libs/bootstrap-tagsinput/bootstrap-tagsinput.css') }}">

    <!-- style for datepicker -->
    <link rel="stylesheet" href="{{ asset('assets/admin/libs/bootstrap-datepicker/bootstrap-datepicker.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/admin/libs/bootstrap-daterangepicker/bootstrap-daterangepicker.css') }}">
    <link rel="stylesheet"
        href="{{ asset('assets/admin/libs/bootstrap-material-datetimepicker/bootstrap-material-datetimepicker.css') }}">
    <link rel="stylesheet"
        href="{{ asset('assets/admin/libs/bootstrap-material-datetimepicker/bootstrap-material-datetimepicker.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/admin/libs/timepicker/timepicker.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/admin/libs/minicolors/minicolors.css') }}">

    <style>
        td.details-control {

            background: url("{{ asset('assets/admin/datatable/icon/plus.png') }}") no-repeat center center;
            cursor: pointer;
        }

        tr.details td.details-control {
            background: url("{{ asset('assets/admin/datatable/icon/minus.png') }}") no-repeat center center;
        }


        /* Content of modal div is center aligned */
        .modal-dialog-full-width {
            width: 100% !important;
            height: 100% !important;
            margin: 0 !important;
            padding: 0 !important;
            max-width: none !important;

        }

        .modal-content-full-width {
            height: auto !important;
            min-height: 100% !important;
            border-radius: 0 !important;
            background-color: #ffffff !important
        }

        .modal-header-full-width {
            border-bottom: 1px solid #ffffff !important;
        }

        .modal-footer-full-width {
            border-top: 1px solid #928c8c !important;
        }

        .img-responsive {

            margin: 0 auto;
            width: 535px;
            height: 550px;
        }

    </style>
@endsection


@section('body-content')
    <!-- [ Layout navbar ( Header ) ] End -->

    <!-- [ Layout content ] Start -->
    <div class="layout-content">

        <!-- [ content ] Start -->
        <div class="container-fluid flex-grow-1 container-p-y">
            <h4 class="font-weight-bold py-3 mb-0">Selects and tags</h4>
            <div class="text-muted small mt-0 mb-4 d-block breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#"><i class="feather icon-home"></i></a></li>
                    <li class="breadcrumb-item">Forms</li>
                    <li class="breadcrumb-item active">Selects and tags</li>
                </ol>
            </div>
            {{-- code by uchchwas --}}
            {{-- <div class=" card container">
                            <div class = "row">
                                {{-- col 1 --}}
            {{-- <div class = "col-md-4">
                                    <div class="card-body demo-vertical-spacing">
                                        <select class="selectpicker" data-style="btn-default">
                                            <option>ALL</option>
                                            <option>GAS BILL</option>
                                            <option>UTILITY BILL</option>
                                            <option>ELECTRIC BILL</option>
                                            <option>TELEPHONE BILL</option>
                                            <option>CREDIT CARD BILL</option>
                                            <option>CREDIT CARD STATEMENT</option>
                                            <option>BANK STATEMENT</option>
                                            <option>BANK CRETIFICATE</option>
                                            <option>PASSPORT COPY</option>
                                            <option>NATIONAL ID COPY</option>
                                            <option>DIRIVING LICENSE</option>
                                        
                                        </select>
                                    </div>
                                </div> --}}
            {{-- col 2 --}}
            {{-- <div class = "form-group ">
                                    <div class="col-md-4">
                                        <input type="email" class="form-control" placeholder="Email">
                                        <div class="clearfix"></div>
                                    </div>
                                </div> --}}
            {{-- col 3 --}}
            {{-- <div class = "col-md-4">
                                    <div class="card-body demo-vertical-spacing">
                                        <select class="selectpicker" data-style="btn-default">
                                            <option>Mustard</option>
                                            <option>Ketchup</option>
                                            <option>Relish</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div> --}}

            <div class="card mb-4">
                <h6 class="card-header">Filter Report</h6>
                <div class="card-body">
                    <form>
                        <div class="form-row">
                            <div class="form-group col-md-4 demo-vertical-spacing">
                                <select id="doc_type" name="doc_type" class="selectpicker" data-style="btn-default">
                                    <option value="">ALL</option>
                                    <option value="gas_bill">GAS BILL</option>
                                    <option value="utility_bill">UTILITY BILL</option>
                                    <option value="electric_bill">ELECTRIC BILL</option>
                                    <option value="telephone_bill">TELEPHONE BILL</option>
                                    <option value="credit_bill">CREDIT CARD BILL</option>
                                    <option value="credit_statement">CREDIT CARD STATEMENT</option>
                                    <option value="bank_statement">BANK STATEMENT</option>
                                    <option value="bank_certificate">BANK CERTIFICATE</option>
                                    <option value="passport">PASSPORT COPY</option>
                                    <option value="national_id">NATIONAL ID COPY</option>
                                    <option value="driving_license">DIRIVING LICENSE</option>

                                </select>
                            </div>
                            <div class="form-group col-md-4">
                                {{-- <label class="form-label">Email</label> --}}
                                <input type="text" id="client_name" name="client_name" class="form-control"
                                    placeholder="Name/Email/Subcode" data-toggle="tooltip" data-placement="top"
                                    title="Search By Client">
                                <div class="clearfix"></div>
                            </div>
                            <div class="input-daterange input-group form-group col-md-4" id="datepicker-range">
                                {{-- <label class="form-label">Password</label> --}}
                                <input type="text" class="form-control" id="created_at" name="created_at"
                                    data-toggle="tooltip" data-placement="top" title="Submit Date">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">to</span>
                                </div>
                                <input type="text" class="form-control" id="created_at" name="created_at"
                                    data-toggle="tooltip" data-placement="top" title="Submit Date">
                                <div class="clearfix"></div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-4 demo-vertical-spacing">
                                <select id="status" name="status" class="selectpicker" data-style="btn-default">
                                    <option value="">All</option>
                                    <option value="approved">APPROVED</option>
                                    <option value="pending">PENDING</option>
                                    <option value="decline">DECLINED</option>
                                </select>
                            </div>
                            <div class="input-daterange input-group form-group col-md-4" id="datepicker-range">
                                {{-- <label class="form-label">Password</label> --}}
                                <input type="text" class="form-control" name="issue_date_srt" id="issue_date_srt"
                                    data-toggle="tooltip" data-placement="top" title="Issue Date">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">to</span>
                                </div>
                                <input type="text" class="form-control" name="issue_date_end" id="issue_date_end"
                                    name="issue_date_srt" id="issue_date_srt" data-toggle="tooltip" data-placement="top"
                                    title="Issue Date">
                                <div class="clearfix"></div>
                            </div>
                            <div class="input-daterange input-group form-group col-md-4" id="datepicker-range">
                                {{-- <label class="form-label">Password</label> --}}
                                <input type="text" class="form-control" name="exp_date" id="exp_date"
                                    data-toggle="tooltip" data-placement="top" title="Expire Date">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">to</span>
                                </div>
                                <input type="text" class="form-control" name="exp_date" id="exp_date"
                                    data-toggle="tooltip" data-placement="top" title="Expire Date">
                                <div class="clearfix"></div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-4 demo-vertical-spacing">
                                <select id="client_type" name="client_type" class="selectpicker" data-style="btn-default">
                                    <option value="">All</option>
                                    <option value="trader">TRADER</option>
                                    <option value="ib">IB</option>
                                </select>
                            </div>
                            <div class="form-group col-md-4">
                                {{-- <label class="form-label">Email</label> --}}
                                <input type="text" name="email" id="email" class="form-control"
                                    placeholder="Account/Desk Manager Name or Email" data-toggle="tooltip"
                                    data-placement="top" title="Search By Account/Desk Manager">
                                <div class="clearfix"></div>
                            </div>
                            <div class="form-group demo-inline-spacing col-md-4 d-flex justify-content-center">
                                <button id="export_btn" type="submit" class="btn btn-primary btn-block">EXPORT</button>
                                <button id="filterBtn" type="button" class="btn btn-success btn-block">FILTER</button>
                            </div>
                        </div>




                    </form>
                </div>
            </div>


            <!-- DataTable within card -->


            <div class="row">

                <div class="col-md-12">
                    <div class="card d-flex w-100 mb-4">
                        <h6 class="card-header">KYC REPORTS</h6>
                        <div class="row no-gutters row-bordered row-border-light h-100">
                            <div class="d-flex col-md-12 align-items-center">
                                <div class="card-body">
                                    <table id="btc_tbl" class="display table table-hover table-custom" style="width:100%">
                                        <thead>
                                            <tr>
                                                <th>client_name</th>
                                                <th>Client Type </th>
                                                <th>Document Type</th>
                                                <th>Issue Date</th>
                                                <th>Expire Date</th>
                                                <th>Status</th>
                                                <th>Date</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

        </div>



       
        <!-- DataTable within card -->
        {{-- end code --}}









    </div>
    <!-- [ content ] End -->



    

    {{-- <button type="submit" data-optype="approve" data-userid="3" value="1" class="btn btn-success btn-md btn-rounded">Approve</button> --}}

    <!-- Modal -->
    <!--Bootstrap modal -->




    <!-- Modal -->
    <div class="modal fade " id="modalUpdateProfile" tabindex="-1" role="dialog"
        aria-labelledby="exampleModalPreviewLabel" aria-hidden="true">
        <div class="modal-dialog modal-xl" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalPreviewLabel">Are you sure?</h5>
                    <button type="button" class="close " data-dismiss="modal" aria-label="Close">
                        <span style="font-size: 1.3em;" aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">

                    <div class="row">
                        <div class="col-md-6" id="user-img">


                        </div>

                        <div class="col-md-6">
                            {{-- <input type="text" class="" id="cname" name="client_name" value=""> --}}

                            <ul class="list-group list-group-flush">


                                <li class="list-group-item">Email : <span id="_email"> today</span></li>
                                <li class="list-group-item">Country : <span id="_country"></span></li>
                                <li class="list-group-item">Address : <span id="_address"></span></li>
                                <li class="list-group-item">State : <span id="_state"></span></li>
                                <li class="list-group-item">Phone : <span id="_phone"></span></li>
                                <li class="list-group-item">Zip : <span id="_zip"></span></li>
                                <li class="list-group-item">Date Of Birth : <span id="_bithday"></span></li>
                                <li class="list-group-item">Status : <span id="_status"></span></li>
                            </ul>
                            <br><br><br>
                            <ul class="list-group list-group-flush">
                                <li class="list-group-item">Name : <span id="c_name"></span></li>
                                <li class="list-group-item">Issue Date : <span id="_issu_date"></span></li>
                                <li class="list-group-item">Expire Date : <span id="_exp_date"></span></li>
                                <li class="list-group-item">Document Type : <span id="_doc_type"></span></li>
                                <li class="list-group-item">Issuer Country : <span id="_country1"></span></li>
                            </ul>
                        </div>
                    </div>


                </div>
                <div class="modal-footer">

            

                    <button type="button" class="btn btn-danger btn-md btn-rounded" data-dismiss="modal">Close</button>



                </div>
            </div>
        </div>
    </div>
<!-- Modal end -->

@endsection


@section('per_page_js')
    <!--date picker-->
    <script src="{{ asset('assets/admin/DatePicker/date-picker/js/bootstrap-datepicker.min.js') }}"></script>
    <!--init date picker-->
    <script src="{{ asset('assets/admin/DatePicker/pickers/init-date-picker.js') }}"></script>
    <!-- common ajax -->

    <script src="{{ asset('assets/admin/js/common-ajax.js') }}"></script>
    {{-- <---Data_table_CDN----> --}}
    <script type="text/javascript" src="https://cdn.datatables.net/1.11.5/js/jquery.dataTables.min.js"></script>

    <!-- select button requ files -->
    <script src="{{ asset('assets/admin/libs/bootstrap-select/bootstrap-select.js') }}"></script>
    <script src="{{ asset('assets/admin/libs/bootstrap-multiselect/bootstrap-multiselect.js') }}"></script>
    <script src="{{ asset('assets/admin/libs/select2/select2.js') }}"></script>
    <script src="{{ asset('assets/admin/libs/bootstrap-tagsinput/bootstrap-tagsinput.js') }}"></script>
    <script src="{{ asset('assets/admin/js/pages/forms_selects.js') }}"></script>

    <!-- script for datepicker -->
    <script src="{{ asset('assets/admin/libs/moment/moment.js') }}"></script>
    <script src="{{ asset('assets/admin/libs/bootstrap-datepicker/bootstrap-datepicker.js') }}"></script>
    <script src="{{ asset('assets/admin/libs/bootstrap-daterangepicker/bootstrap-daterangepicker.js') }}"></script>
    <script
        src="{{ asset('assets/admin/libs/bootstrap-material-datetimepicker/bootstrap-material-datetimepicker.js') }}">
    </script>
    <script src="{{ asset('assets/admin/libs/timepicker/timepicker.js') }}"></script>
    <script src="{{ asset('assets/admin/libs/minicolors/minicolors.js') }}"></script>
    <script src="{{ asset('assets/admin/js/pages/forms_pickers.js') }}"></script>

    <!-- script for ajax form submit -->
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script src="{{ asset('assets/admin/js/jquery.validate.js') }}"></script>
    <script src="{{ asset('assets/admin/js/ajax_form_submit.js') }}"></script>


    <script>
        function format(d) {
            return '<b>Transaction Hash:</b> ' + d.txn_hash;
        }

        function clk() {
            $(this).attr("data-id");
            console.log(data - id);
        }
        $(document).ready(function() {
            var dt = $('#btc_tbl').DataTable({
                "processing": true,
                "serverSide": true,
                "ajax": {
                    "url": "{{ url('admin/report/Kyc_request?op=data_table') }}",
                    "data": function(d) {
                        return $.extend({}, d, {
                            "client_name": $("#client_name").val(),
                            "client_type": $("#client_type").val(),
                            "issue_date": $("#issue_dat").val(),
                            "issue_date": $("#issue_date").val(),
                            "exp_date": $("#exp_date").val(),
                            "doc_type": $("#doc_type").val(),
                            "status": $("#status").val(),
                            "email": $("#email").val(),
                            "created_at": $("#created_at").val(),

                        });

                    }
                },

                "columns": [
                    // {
                    //     // "class":          "details-control",
                    //     "orderable":      false,
                    //     "data":           null,
                    //     "defaultContent": ""
                    // },

                    {
                        "data": "client_name"
                    },
                    {
                        "data": "client_type"
                    },
                    {
                        "data": "doc_type"
                    },
                    {
                        "data": "issu_date"
                    },
                    {
                        "data": "exp_date"
                    },
                    {
                        "data": "status"
                    },
                    {
                        "data": "created_at"
                    },
                    {
                        "data": "action"
                    },

                ],
                "drawCallback": function(settings) {
                    $("#filterBtn").html("FILTER");
                }

            });


            $('#filterBtn').click(function(e) {
                $(this).html("<i class='fa fa-refresh fa-spin fa-1x fa-fw'></i>");
                dt.draw();
            });

        });
        $('button').click(function() {
            console.log($("a").data("id"));
        });
    </script>




    <script type="text/javascript">
        var vtid = 0;
        var op = 'verify_ad';
        var ct = 'ar';
        var va = 0;

        function process(e) {
            var obj = $(e);
            var bt = $(e).html();

            var status = obj.data('status');

            var st = 'Verified';
            if (status == 2) {
                st = 'Declined!';
            }

            if (ct == 'ib') {
                op = 'verify_ad_ib';
            } else {
                op = 'verify_ad';
            }

            obj.html('<i class="fa fa-spinner fa-pulse"></i>');

            $.ajax({
                url: "ar_clients_process.php?id=" + vtid + "&status=" + status + "&op=" + op,
                type: 'GET',
                data: {
                    note: $("#note").val(),
                    va: va
                },
                dataType: 'json',
                success: function(data) {
                    obj.html(bt);

                    if (data.success) {

                        if (status == 2) {
                            $("#modalDeclineWarning").modal("hide");
                        }
                        $("#group").modal("hide");
                        notify('success', data.message);
                        dt.ajax.reload(null, false);
                    } else {
                        notify('error', data.message);
                    }

                }
            });
        }

        function notify(type, message) {
            new PNotify({
                delay: 5000,
                title: 'Notification',
                text: message,
                addclass: 'annouce',
                type: type,
                remove: true
            });
        }

        function modalOpen(e) {
            var obj = $(e);
            $("#group").modal("hide");
            $("#" + obj.data('modal')).modal('show');

        }

        function modalClose(e) {
            var obj = $(e);
            $("#" + obj.data('modal')).modal('hide');
        }


        function madal_oc(e) {
            var obj = $(e);
            $(".ios-switch").removeClass('on');
            $(".ios-switch").addClass('off');
            $(".va").prop("checked", false);
            va = 0;
            $("#" + obj.data('modal')).modal(obj.data('mt'));
            $(".overlay_ar").fadeIn();
            if (obj.data('mt') == 'show') {
                vtid = obj.data('id');
                ct = obj.data('ct');
                $("#bank_proof").load('id_watch.php?id_file_name=' + obj.data('file') + '&ct=' + ct + '&tid=' + vtid,
                    function() {
                        $(".overlay_ar").fadeOut();
                        $(".pb").hide();
                        $(".switchbox").hide();
                        if ($("#hstatus").val() == 0) {
                            $(".pb").show();
                            $(".switchbox").show();
                        }
                    });
            }
        }

        $(".va").change(function() {
            if ($(this).prop('checked') == true) {
                va = 1;
            } else {
                va = 0;
            }
        });



        // KEC Form Update
        function createCallBack(data) {
            $('#submitBtn').prop('disabled', false);
            if (data.success) {
                notify('success', data.message);
                $("#modalUpdateProfile").modal("hide");
                dt.draw();
            } else {
                notify('error', data.message);
                $.validator("kyc-update-form", data.errors);
            }
        }


        function user_details_view(e) {
            let obj = $(e);

            $("#modalUpdateProfile").modal("show");
            // Append Values On Form
            $('#c_id').val(obj.data('id'));
            $('#c_d_id').val(obj.data('id'));
            $('#is_confirmed').val(obj.data('status'));
            $('#is_decline').val(obj.data('status'));
            $('#c_name').text(obj.data('client_name'));
            $('#_email').text(obj.data('email'));
            $('#_country').text(obj.data('country'));
            $('#_address').text(obj.data('address'));
            $('#_city').text(obj.data('city'));
            $('#_state').text(obj.data('state'));
            $('#_phone').text(obj.data('phone'));
            $('#_zip').text(obj.data('zip'));
            $('#_bithday').text(obj.data('bithday'));
            $('#_issu_date').text(obj.data('issu_date'));
            $('#_exp_date').text(obj.data('exp_date'));
            $('#_doc_type').text(obj.data('doc_type'));
            $('#_country1').text(obj.data('country'));
            $('#_status').text(obj.data('status'));
            $('#images').html(obj.data('images'));
            // document.getElementById("myImg").src='{{ asset(".'obj.data('images')'.") }}';
            $("#user-img").html('<img class="img-responsive" id="imgPreview" src="{{ asset('uploads/') }}/' + obj.data(
                'images') + '">');
            // document.getElementById("myImg").src='{{ asset("'+obj.data('images')+'") }}' 
            // $('.myClass').on('mouseenter', {param1: val1}, onMouseOver).on('mouseleave', {param2: val2}, onMouseOut);
            //  if ($("#is_confirmed").val() != null) {
            //     $("#approve_btn").removeAttr('disabled');
            //  } 
            //  if ($("#is_decline").val() != null) {
            //     $("#decline_btn").removeAttr('disabled');
            //  }

            if ($("#is_confirmed").val() == 'approved') {
                // $("#approve_btn").attr('disabled', true);
                // $("#decline_btn").attr('disabled', true);
                $("#approve_btn").hide();
                $("#decline_btn").hide();
            }



            if ($("#is_decline").val() == 'decline') {
                // $("#approve_btn").attr('disabled', true);
                // $("#decline_btn").attr('disabled', true);
                $("#approve_btn").hide();
                $("#decline_btn").hide();
            }

            // console.log(obj.data('images'));

        }

        function update_profile(e) {
            let obj = $(e);

            $("#updatekyc").modal("show");
            $('#user_id').val(obj.data('id'));
            $('#up_name').val(obj.data('client_name'));
            $('#up_country').val(obj.data('country'));
            $('#up_address').val(obj.data('address'));
            $('#up_city').val(obj.data('city'));
            $('#up_state').val(obj.data('state'));
            $('#up_zip').val(obj.data('zip'));
            $('#up_bithday').val(obj.data('bithday'));
            $('#up_issu_date').val(obj.data('issu_date'));
            $('#up_exp_date').val(obj.data('exp_date'));
            $('#up_status').val(obj.data('status'));


        }

        function approveModalCallback(data) {
            if (data.success) {
                swal("Great!", "User Approved Sucessfully!", "success");
                $("#approve_btn").hide();
                $("#decline_btn").hide();
            } else {
                swal("Oops!", "User Approval Unsuccessful!", "error");
            }
        }

        function declineModalCallback(data) {
            if (data.success) {
                swal("Great!", "User Decline Sucessfully!", "success");
                $("#approve_btn").hide();
                $("#decline_btn").hide();
            } else {
                swal("Oops!", "User Decline Unsuccessful!", "error");
            }

        }

        //tooltip js for everywhere.
        $(document).ready(function() {
            $('[data-toggle="tooltip"]').tooltip();
        });
    </script>
@endsection
