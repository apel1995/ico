@extends("layouts.admin.app")

@section('per_page_css')
    <link rel="stylesheet" href="{{ asset('assets/admin/libs/bootstrap-markdown/bootstrap-markdown.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/admin/libs/photoswipe/photoswipe.css') }}">
@endsection


@section('body-content')
    <!-- [ Layout sidenav ] End -->
    <!-- [ Layout container ] Start -->
    <div class="layout-content">

        <!-- [ content ] Start -->
        <div class="container-fluid flex-grow-1 container-p-y">
            <h4 class="font-weight-bold py-3 mb-0">Ticket list</h4>
            <div class="text-muted small mt-0 mb-4 d-block breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#"><i class="feather icon-home"></i></a></li>
                    <li class="breadcrumb-item"><a href="#!">Helpdesk</a></li>
                    <li class="breadcrumb-item active"><a href="#!">Ticket list</a></li>
                </ol>
            </div>

            <div class="row help-desk">
                <div class="col-xl-8 col-lg-12">
                    
                        <form action="">
                            
                            <div class="form-group">
                                <input type="text" name="q" placeholder="Find or Search Members...!" class="form-control"/>
                                <input type="submit" class="btn btn-primary" value="Search"/>
                            </div>
                        </form>
                    
                    <div class="card">
                        <div class="card-body">
                            <nav class="navbar justify-content-between p-0 align-items-center shadow-none">
                                <h5 class="my-2">Ticket List</h5>
                                <div class="btn-group btn-group-toggle" data-toggle="buttons">
                                    <label
                                        onclick="$('.help-desk').removeClass('md-view').removeClass('large-view').addClass('sm-view')"
                                        class="btn waves-effect waves-light btn-primary">
                                        <input type="radio" name="options" id="option1" checked> <i
                                            class="feather icon-align-justify m-0"></i>
                                    </label>
                                    <label
                                        onclick="$('.help-desk').removeClass('sm-view').removeClass('large-view').addClass('md-view')"
                                        class="btn waves-effect waves-light btn-primary">
                                        <input type="radio" name="options" id="option2"> <i
                                            class="feather icon-menu m-0"></i>
                                    </label>
                                    <label
                                        onclick="$('.help-desk').removeClass('md-view').removeClass('sm-view').addClass('large-view')"
                                        class="btn waves-effect waves-light btn-primary active">
                                        <input type="radio" name="options" id="option3"> <i
                                            class="feather icon-grid m-0"></i>
                                    </label>
                                </div>
                            </nav>
                        </div>
                    </div>
                    @foreach($create_tickets as $post)
                    <div class="ticket-block">
                        <div class="row">
                            <div class="col-sm-auto">
                                <img class="media-object wid-60 img-radius mb-3" src="{{asset('assets/admin/img/user/avatar-1.jpg')}}"
                                    alt="Generic placeholder image ">
                            </div>
                            <div class="col">
                                <div class="card example-popover" data-toggle="modal" data-target="#modals-slide"
                                    data-toggle="popover" data-placement="right" data-html="true"
                                    title="<img src='{{asset('assets/admin/img/user/avatar-1.jpg')}}' class='wid-20 rounded mr-1 img-fluid'><p class='d-inline-block mb-0 ml-2'>You replied</p>"
                                    data-content="hello Yogen dra,you need to create "
                                    toolbar-options="div only once in a page in your code, this div fill found every 'td' ...">
                                    <div class="row no-gutters row-bordered row-border-light h-100">
                                        <div class="d-flex col">
                                            <div class="card-body">
                                                <h5 class="mb-0">{{ $post -> customer }}</h5>
                                                <p class="my-1 text-muted"><i class="feather icon-lock mr-1 f-14"></i>{{ $post -> subject }}</p>
                                                <ul class="list-inline mt-2 mb-0 hid-sm">
                                                    <li class="list-inline-item my-1"><img src="{{asset('assets/admin/img/ticket/p1.jpg')}}"
                                                            alt="" class="wid-20 rounded mr-1">{{ $post->category }}</li>
                                                    <li class="list-inline-item my-1"><img
                                                            src="{{asset('assets/admin/img/user/avatar-5.jpg')}}" alt=""
                                                            class="wid-20 rounded mr-1">{{ $post->agent_name }}</li>
                                                    <li class="list-inline-item my-1"><i
                                                            class="feather icon-calendar mr-1 f-14"></i>{{ $post->created_at->diffForHumans() }} on
                                                            {{ $post->created_at->format('d, M') }}
                                                    </li>
                                                    <li class="list-inline-item my-1"><i
                                                            class="feather icon-message-square mr-1 f-14"></i>{{ $total_comment }}</li>
                                                    {{-- <li> <p class="mb-0">{{ $post->Description }}</p></li> --}}
                                                </ul>
                                                <div class="card  my-3 p-3 hid-md">
                                                    
                                                    <p class="mb-0">{!! $post->Description !!}</p>
                                                </div>
                                                <div class="mt-2">
                                                    <a href="{{  url('admin/support_token/ticket_details/'. $post->id) }}" class="mr-3 text-muted"><i
                                                            class="feather icon-eye mr-1"></i>View Ticket</a>
                                                            <a type = "submit" data-parent_id={{ $post->id }} onclick = "delete_token(this)" ><i class="m-r-5 feather icon-trash-2"></i>Delete</a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="d-flex col-sm-auto">
                                            <div class="card-body d-flex align-items-center">
                                                <ul class="list-unstyled mb-0">
                                                    <li><a href="#" data-toggle="tooltip" data-placement="top" title=""
                                                            data-original-title="tooltip on top" class="active"><i
                                                                class="feather icon-star text-warning"></i></a></li>
                                                    <li><a href="#" data-toggle="tooltip" data-placement="top" title=""
                                                            data-original-title="tooltip on top"><i
                                                                class="feather icon-circle text-muted"></i></a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>      
                    </div>
                    @endforeach
                    {{-- pagination --}}
                    
                   
                    <div class="container">
                        <div class="d-flex justify-content-center">
                        {{ $create_tickets->links() }} 
                        </div>
                    </div>
                   
                    
                  
                    

                   
                </div>
                
                <div class="col-xl-4 col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <h5>Ticket Categories</h5>
                        </div>
                        @foreach($categories as $cat)
                            <div class="list-group list-group-flush list-pills">
                                    <a href="#!" class="list-group-item list-group-item-action">
                                        <span class="f-w-500"><img src="{{asset('assets/admin/img/ticket/p1.jpg')}}" alt=""
                                                class="wid-20 rounded m-r-10 img-fluid"></span>
                                                {{ $cat->category }}
                                        {{-- <div class="float-right">
                                            <span class="badge badge-pill badge-danger">2</span>
                                            <span class="badge badge-pill badge-success">5</span>
                                        </div> --}}
                                    </a>
                            </div>
                        @endforeach
                    </div>
                    <div class="card">
                        <div class="card-header">
                            <h5>Support Agent</h5>
                        </div>
                        @foreach($support_agent as $agent)
                                <div class="list-group list-group-flush list-pills">
                                    
                                    <a href="#!" class="list-group-item list-group-item-action">
                                        <span class="f-w-500"><img src="{{asset('assets/admin/img/user/avatar-5.jpg')}}" alt=""
                                                class="wid-20 rounded m-r-10 img-fluid"></span>
                                                {{ $agent->name }}
                                        {{-- <div class="float-right">
                                            <span class="badge badge-pill badge-danger">1</span>
                                            <span class="badge badge-pill badge-success">3</span>
                                        </div> --}}
                                    </a>
                                    
                                </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
        <!-- [ content ] End -->


    </div>
    <!-- [ Layout wrapper] End -->

@endsection

@section('per_page_js')
    <script src="{{ asset('assets/admin/libs/markdown/markdown.js') }}"></script>
    <script src="{{ asset('assets/admin/libs/bootstrap-markdown/bootstrap-markdown.js') }}"></script>
    <script src="{{ asset('assets/admin/libs/photoswipe/photoswipe.js') }}"></script>
    <script type="text/javascript">
        $('.example-popover').popover({
            container: 'body',
            trigger: 'hover'
        })
        // Bootstrap Markdown
        $(function() {
            $('#bs-markdown').markdown({
                iconlibrary: 'fa',
                footer: '<div id="md-character-footer"></div><small id="md-character-counter" class="text-muted">350 character left</small>',

                onChange: function(e) {
                    var contentLength = e.getContent().length;

                    if (contentLength > 350) {
                        $('#md-character-counter')
                            .removeClass('text-muted')
                            .addClass('text-danger')
                            .html((contentLength - 350) + ' character surplus.');
                    } else {
                        $('#md-character-counter')
                            .removeClass('text-danger')
                            .addClass('text-muted')
                            .html((350 - contentLength) + ' character left.');
                    }
                },
            });
            $('#markdown').trigger('change');
            $('.md-editor .fa-header').removeClass('fa fa-header').addClass('fas fa-heading');
            $('.md-editor .fa-picture-o').removeClass('fa fa-picture-o').addClass('far fa-image');
        });
        // Photoswipe
        $(function() {
            initPhotoSwipeFromDOM('#photoswipe-example');
        });
    </script>
    <script>
                function delete_token(e){
                    let obj = $(e);
                    var p_id = obj.data('parent_id');
                    $.get('ticket_details/delete/' +p_id,function (data, textStatus, jqXHR) {
                    swal ("Delete Token Sucessfully");
                    window.location.href = "{{url('admin/support_token/ticket_list') }}";
                    });
                    location.reload();
                    console.log(p_id);

                }
    </script>
@endsection
