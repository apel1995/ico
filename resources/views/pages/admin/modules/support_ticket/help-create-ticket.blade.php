@extends("layouts.admin.app")

@section('per_page_css')
<link rel="stylesheet" href="{{ asset('assets/admin/libs/bootstrap-markdown/bootstrap-markdown.css') }}">

@endsection


@section('body-content')

                <!-- [ Layout content ] Start -->
                <div class="layout-content">

                    <!-- [ content ] Start -->
                    <div class="container-fluid flex-grow-1 container-p-y">
                        <h4 class="font-weight-bold py-3 mb-0">Create ticket</h4>
                        <div class="text-muted small mt-0 mb-4 d-block breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="#"><i class="feather icon-home"></i></a></li>
                                <li class="breadcrumb-item"><a href="#!">Helpdesk</a></li>
                                <li class="breadcrumb-item active"><a href="#!">Create ticket</a></li>
                            </ol>
                        </div>
                        {{-- for validation --}}
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                            <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                            </ul>
                            </div>
                            @endif
                        {{-- end validaton --}}

                        <div class="row">
                            <div class="col-sm-12">
                                <div class="card">
                                    <div class="card-body">
                                        <form action="/admin/support_token/create_ticket" method="post" enctype="multipart/form-data">
                                            {{ csrf_field() }}
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label>Customer</label>
                                                        <select class="mb-3 form-control" name = "customer" id = "customer">
                                                            <option value="">Default select</option>
                                                            <option value="Customer1">Customer 1</option>
                                                            <option value="Customer2">Customer 2</option>
                                                            <option value="Customer3">Customer 3</option>
                                                            <option value="Customer4">Customer 4</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label>Select Priority Level</label>
                                                        <select class="mb-3 form-control" name = "category" id = "category">
                                                            <option value="">Default select</option>
                                                            <option value="normal">Normal Priority</option>
                                                            <option value="high">High Priority/option>
                                                            <option value="critical">Critical Priority</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="subject">Subject</label>
                                                <input type="text" class="form-control" id="subject" name= "subject" placeholder="Password">
                                            </div>
                                            <div class="form-group">
                                                <label for="Description">Description</label>
                                                <textarea id="myTextarea" name ="Description"></textarea>
                                                <label class="file btn waves-effect waves-light btn-outline-primary mt-3 file-btn" for="flup">
                                                    <i class="feather icon-paperclip"></i>Add Atachment
                                           
                                                    <input type="file" class="form-control" name="photos[]" multiple />
                                                </label>
                                            </div>
                                            <button type="submit" class="btn waves-effect waves-light btn-primary">Submit</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- [ content ] End -->

                    <!-- [ Layout footer ] Start -->


@endsection




@section('per_page_js') 

<script src="{{ asset('assets/admin/libs/markdown/markdown.js') }}"></script>
<script src="{{ asset('assets/admin/libs/bootstrap-markdown/bootstrap-markdown.js') }}"></script>
<script
    type="text/javascript"
    src='https://cdn.tiny.cloud/1/no-api-key/tinymce/5/tinymce.min.js'
    referrerpolicy="origin">
</script>
<script type="text/javascript">
    tinymce.init({
    selector: '#myTextarea',
    width: 1000,
    height: 300,
    plugins: [
    'advlist autolink link image lists charmap print preview hr anchor pagebreak',
    'searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking',
    'table emoticons template paste help'
    ],
    toolbar: 'undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | ' +
    'bullist numlist outdent indent | link image | print preview media fullscreen | ' +
    'forecolor backcolor emoticons | help',
    menu: {
    favs: {title: 'My Favorites', items: 'code visualaid | searchreplace | emoticons'}
    },
    menubar: 'favs file edit view insert format tools table help',
    content_css: 'css/content.css'
    });
</script>
@endsection