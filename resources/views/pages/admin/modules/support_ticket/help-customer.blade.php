@extends("layouts.admin.app")

@section('per_page_css')
<link rel="stylesheet" href="{{ asset('assets/admin/libs/bootstrap-markdown/bootstrap-markdown.css') }}">

@endsection


@section('body-content')
                <!-- [ Layout content ] Start -->
                <div class="layout-content">

                    <!-- [ content ] Start -->
                    <div class="container-fluid flex-grow-1 container-p-y">
                        <h4 class="font-weight-bold py-3 mb-0">Customers</h4>
                        <div class="text-muted small mt-0 mb-4 d-block breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="#"><i class="feather icon-home"></i></a></li>
                                <li class="breadcrumb-item"><a href="#!">Helpdesk</a></li>
                                <li class="breadcrumb-item active"><a href="#!">Customers</a></li>
                            </ol>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="card shadow-none">
                                    <div class="card-header">
                                        <div class="float-right">
                                            <button type="button" class="btn btn-sm btn-primary m-0" data-toggle="modal" data-target="#exampleModal">
                                                New Customer
                                            </button>
                                            <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                <div class="modal-dialog" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title" id="exampleModalLabel"><i class="feather icon-user mr-1"></i>Add Customer</h5>
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                        <form>
                                                            <div class="modal-body text-left">
                                                                <small id="emailHelp" class="form-text text-muted mb-2 mt-0">We'll never share your email with anyone else.</small>
                                                                <div class="form-group">
                                                                    <label>First Name</label>
                                                                    <input type="email" class="form-control" id="fname" aria-describedby="emailHelp" placeholder="Enter email">
                                                                </div>
                                                                <div class="form-group">
                                                                    <label>Last Name</label>
                                                                    <input type="email" class="form-control" id="lname" aria-describedby="emailHelp" placeholder="Enter email">
                                                                </div>
                                                                <div class="form-group">
                                                                    <label>Email address</label>
                                                                    <input type="email" class="form-control" id="emial" aria-describedby="emailHelp" placeholder="Enter email">
                                                                </div>
                                                                <div class="form-group">
                                                                    <label>Password</label>
                                                                    <input type="password" class="form-control" id="passwd" placeholder="Password">
                                                                </div>
                                                                <div class="form-group">
                                                                    <label>Confirm Password</label>
                                                                    <input type="password" class="form-control" id="cnpasswd" placeholder="Password">
                                                                </div>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn waves-effect waves-light btn-secondary" data-dismiss="modal">Close</button>
                                                                <button type="button" class="btn waves-effect waves-light btn-primary">Save changes</button>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <h5>Customers</h5>
                                    </div>
                                    <div class="card-body shadow border-0">
                                        <div class="table-responsive">
                                            <table class="table table-hover">
                                                <thead>
                                                    <tr>
                                                        <th class="border-top-0">Name</th>
                                                        <th class="border-top-0">Email</th>
                                                        <th class="border-top-0">Account</th>
                                                        <th class="border-top-0">Last Update</th>
                                                        <th class="border-top-0"></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach($create_tickets as $details)
                                                    <tr>
                                                        <td>{{ $details->customer }}</td>
                                                        <td><a href="#">mark@mark.com</a></td>
                                                        <td>N/A</td>
                                                        <td>{{ $details->updated_at->format('M d, Y') }}</td>
                                                        <td>
                                                            
                                                                <a href="{{  url('admin/support_token/ticket_details/'. $details->id) }}" class="text-muted"><i class="feather icon-edit mr-1"></i></a>
                                                                <a href="#" class="text-muted"><i class="feather icon-trash-2"></i></a>
                                                            
                                                        </td>
                                                    </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                            <div class="pagination-block text-center">
                                                <nav aria-label="Page navigation example" class="d-inline-block">
                                                    <ul class="pagination">
                                                        <li class="page-item"><a class="page-link" href="#">Previous</a></li>
                                                        <li class="page-item"><a class="page-link" href="#">1</a></li>
                                                        <li class="page-item"><a class="page-link" href="#">2</a></li>
                                                        <li class="page-item"><a class="page-link" href="#">3</a></li>
                                                        <li class="page-item"><a class="page-link" href="#">Next</a></li>
                                                    </ul>
                                                </nav>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- [ content ] End -->


 @endsection

 @section('per_page_js')   
 <script src="{{ asset('assets/admin/libs/markdown/markdown.js') }}"></script>
 <script src="{{ asset('assets/admin/libs/bootstrap-markdown/bootstrap-markdown.js') }}"></script>                 
 @endsection