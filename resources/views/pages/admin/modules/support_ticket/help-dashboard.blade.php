@extends("layouts.admin.app")

@section('per_page_css')


@endsection


@section('body-content')
   
                <!-- [ Layout navbar ( Header ) ] End -->

                <!-- [ Layout content ] Start -->
                <div class="layout-content">

                    <!-- [ content ] Start -->
                    <div class="container-fluid flex-grow-1 container-p-y">
                        <h4 class="font-weight-bold py-3 mb-0">Helpdesk dashboard</h4>
                        <div class="text-muted small mt-0 mb-4 d-block breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="#"><i class="feather icon-home"></i></a></li>
                                <li class="breadcrumb-item"><a href="#!">Helpdesk</a></li>
                                <li class="breadcrumb-item active"><a href="#!">Dashboard</a></li>
                            </ol>
                        </div>
                        <div class="row">
                            <!-- support-section start -->
                            <div class="col-xl-4 col-md-6">
                                <div class="card support-bar">
                                    <div class="card-body pb-0">
                                        <h2 class="m-0">350</h2>
                                        <span class="text-c-purple">Support Requests</span>
                                        <p class="mb-3 mt-3">Total number of support requests that come in.</p>
                                    </div>
                                    <div id="support-chart" style="height:100px;width:100%;"></div>
                                    <div class="card-footer bg-info text-white">
                                        <div class="row text-center">
                                            <div class="col border-right">
                                                <h4 class="m-0 text-white">10</h4>
                                                <span>Open</span>
                                            </div>
                                            <div class="col border-right">
                                                <h4 class="m-0 text-white">5</h4>
                                                <span>Running</span>
                                            </div>
                                            <div class="col">
                                                <h4 class="m-0 text-white">3</h4>
                                                <span>Solved</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- support-section end -->
                            <!-- support1-section start -->
                            <div class="col-xl-4 col-md-6">
                                <div class="card support-bar">
                                    <div class="card-body pb-0">
                                        <h2 class="m-0">500</h2>
                                        <span class="text-c-blue">Agent Response</span>
                                        <p class="mb-3 mt-3">Total number of support requests that come in.</p>
                                    </div>
                                    <div id="support-chart1" style="height:100px;width:100%;"></div>
                                    <div class="card-footer bg-primary text-white">
                                        <div class="row text-center">
                                            <div class="col border-right">
                                                <h4 class="m-0 text-white">50</h4>
                                                <span>Open</span>
                                            </div>
                                            <div class="col border-right">
                                                <h4 class="m-0 text-white">75</h4>
                                                <span>Running</span>
                                            </div>
                                            <div class="col">
                                                <h4 class="m-0 text-white">30</h4>
                                                <span>Solved</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- support-section1 end -->
                            <!-- support2-section start -->
                            <div class="col-xl-4 col-md-12">
                                <div class="card support-bar">
                                    <div class="card-body pb-0">
                                        <h2 class="m-0">800</h2>
                                        <span class="text-c-green">Support Resolved</span>
                                        <p class="mb-3 mt-3">Total number of support requests that come in.</p>
                                    </div>
                                    <div id="support-chart2" style="height:100px;width:100%;"></div>
                                    <div class="card-footer bg-success text-white">
                                        <div class="row text-center">
                                            <div class="col border-right">
                                                <h4 class="m-0 text-white">80</h4>
                                                <span>Open</span>
                                            </div>
                                            <div class="col border-right">
                                                <h4 class="m-0 text-white">60</h4>
                                                <span>Running</span>
                                            </div>
                                            <div class="col">
                                                <h4 class="m-0 text-white">90</h4>
                                                <span>Solved</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- support-section2 end -->
                            <!-- customer-section start -->
                            <div class="col-xl-8 col-md-6">
                                <div class="card">
                                    <div class="card-body">
                                        <h6>Customer Satisfaction</h6>
                                        <span>It takes continuous effort to maintain high customer satisfaction levels.Internal and external quality measures are often tied together.,as the opinion...</span>
                                        <span class="text-c-blue d-block">Learn more..</span>
                                        <div class="row d-flex justify-content-center align-items-center">
                                            <div class="col">
                                                <div id="satisfaction-chart" style="height:200px;"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- rating-section start -->
                                <div class="row">
                                    <div class="col-xl-6 col-md-12">
                                        <div class="card social-res-card">
                                            <div class="card-header">
                                                <h5>Facebook Source</h5>
                                            </div>
                                            <div class="card-body">
                                                <p class="m-b-10">Page Profile</p>
                                                <div class="progress m-b-25">
                                                    <div class="progress-bar bg-c-blue" style="width:25%"></div>
                                                </div>
                                                <p class="m-b-10">Favorite</p>
                                                <div class="progress m-b-25">
                                                    <div class="progress-bar bg-c-blue" style="width:85%"></div>
                                                </div>
                                                <p class="m-b-10">Like Story</p>
                                                <div class="progress">
                                                    <div class="progress-bar bg-c-blue" style="width:65%"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xl-6 col-md-12">
                                        <div class="card social-res-card">
                                            <div class="card-header">
                                                <h5>Twitter Source</h5>
                                            </div>
                                            <div class="card-body">
                                                <p class="m-b-10">Wall Profile</p>
                                                <div class="progress m-b-25">
                                                    <div class="progress-bar bg-c-red" style="width:85%"></div>
                                                </div>
                                                <p class="m-b-10">Favorite</p>
                                                <div class="progress m-b-25">
                                                    <div class="progress-bar bg-c-red" style="width:25%"></div>
                                                </div>
                                                <p class="m-b-10">Like Tweets</p>
                                                <div class="progress">
                                                    <div class="progress-bar bg-c-red" style="width:65%"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- rating-section end -->
                            </div>
                            <!-- customer-section end -->
                            <!-- latest-section start -->
                            <div class="col-xl-4 col-md-6">
                                <div class="card ui-feed mb-4">
                                    <h5 class="card-header">Feeds</h5>
                                    <div class="card-body">
                                        <div class="row mb-4">
                                            <div class="col-auto pr-0">
                                                <i class="fa fa-bell bg-primary feed-icon "></i>
                                            </div>
                                            <div class="col">
                                                <h6 class="mb-1">You have 3 pending tasks.</h6>
                                            </div>
                                        </div>
                                        <div class="row mb-4">
                                            <div class="col-auto pr-0">
                                                <i class="fa fa-shopping-cart bg-danger feed-icon "></i>
                                            </div>
                                            <div class="col">
                                                <h6 class="mb-1">New order received</h6>
                                            </div>
                                        </div>
                                        <div class="row mb-4">
                                            <div class="col-auto pr-0">
                                                <i class="fas fa-file bg-success feed-icon "></i>
                                            </div>
                                            <div class="col">
                                                <h6 class="mb-1">You have 3 pending tasks.</h6>
                                            </div>
                                        </div>
                                        <div class="row mb-4">
                                            <div class="col-auto pr-0">
                                                <i class="fa fa-bell bg-primary feed-icon "></i>
                                            </div>
                                            <div class="col">
                                                <h6 class="mb-1">You have 3 pending tasks.</h6>
                                            </div>
                                        </div>
                                        <div class="row mb-4">
                                            <div class="col-auto pr-0">
                                                <i class="fa fa-shopping-cart bg-danger feed-icon "></i>
                                            </div>
                                            <div class="col">
                                                <h6 class="mb-1">New order received</h6>
                                            </div>
                                        </div>
                                        <div class="row mb-4">
                                            <div class="col-auto pr-0">
                                                <i class="fas fa-file bg-success feed-icon "></i>
                                            </div>
                                            <div class="col">
                                                <h6 class="mb-1">You have 3 pending tasks.</h6>
                                            </div>
                                        </div>
                                        <div class="row mb-4">
                                            <div class="col-auto pr-0">
                                                <i class="fa fa-bell bg-primary feed-icon "></i>
                                            </div>
                                            <div class="col">
                                                <h6 class="mb-1">You have 3 pending tasks.</h6>
                                            </div>
                                        </div>
                                        <div class="row mb-0">
                                            <div class="col-auto pr-0">
                                                <i class="fa fa-shopping-cart bg-warning feed-icon "></i>
                                            </div>
                                            <div class="col">
                                                <h6 class="mb-4">New order received</h6>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-body">
                                        <div class="row align-items-center m-l-0">
                                            <div class="col-auto">
                                                <i class="icon feather icon-book f-30 text-primary"></i>
                                            </div>
                                            <div class="col-auto">
                                                <h6 class="text-muted m-b-10">Tickets Answered</h6>
                                                <h2 class="m-b-0">379</h2>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- latest-section end -->
                        </div>
                    </div>
                    <!-- [ content ] End -->


@endsection

@section('per_page_js') 



<script src="{{ asset('assets/admin/libs/apexcharts/apexcharts.min.js') }}"></script>
<script src="{{ asset('assets/admin/libs/datatables/datatables.js')}}"></script>

<script>
    'use strict';
    $(document).ready(function() {

        // [ support-chart ] start
        $(function() {
            var options1 = {
                chart: {
                    type: 'area',
                    height: 100,
                    sparkline: {
                        enabled: true
                    }
                },
                colors: ["#55a3f4"],
                stroke: {
                    curve: 'smooth',
                    width: 2,
                },
                series: [{
                    name: 'series1',
                    data: [0, 20, 10, 45, 30, 55, 20, 30, 0]
                }],
                tooltip: {
                    fixed: {
                        enabled: false
                    },
                    x: {
                        show: false
                    },
                    y: {
                        title: {
                            formatter: function(seriesName) {
                                return ''
                            }
                        }
                    },
                    marker: {
                        show: false
                    }
                }
            }
            new ApexCharts(document.querySelector("#support-chart"), options1).render();
        });
        // [ support-chart ] end
        // [ support-chart1 ] start
        $(function() {
            var options1 = {
                chart: {
                    type: 'area',
                    height: 100,
                    sparkline: {
                        enabled: true
                    }
                },
                colors: ["#ff4a00"],
                stroke: {
                    curve: 'smooth',
                    width: 2,
                },
                series: [{
                    name: 'series1',
                    data: [0, 20, 10, 45, 30, 55, 20, 30, 0]
                }],
                tooltip: {
                    fixed: {
                        enabled: false
                    },
                    x: {
                        show: false
                    },
                    y: {
                        title: {
                            formatter: function(seriesName) {
                                return ''
                            }
                        }
                    },
                    marker: {
                        show: false
                    }
                }
            }
            new ApexCharts(document.querySelector("#support-chart1"), options1).render();
        });
        // [ support-chart1 ] end
        // [ support-chart2 ] start
        $(function() {
            var options1 = {
                chart: {
                    type: 'area',
                    height: 100,
                    sparkline: {
                        enabled: true
                    }
                },
                colors: ["#62d493"],
                stroke: {
                    curve: 'smooth',
                    width: 2,
                },
                series: [{
                    name: 'series1',
                    data: [0, 20, 10, 45, 30, 55, 20, 30, 0]
                }],
                tooltip: {
                    fixed: {
                        enabled: false
                    },
                    x: {
                        show: false
                    },
                    y: {
                        title: {
                            formatter: function(seriesName) {
                                return ''
                            }
                        }
                    },
                    marker: {
                        show: false
                    }
                }
            }
            new ApexCharts(document.querySelector("#support-chart2"), options1).render();
        });
        // [ support-chart2 ] end

        // [ satisfaction-chart ] start
        $(function() {
            var options = {
                chart: {
                    height: 260,
                    type: 'pie',
                },
                series: [66, 50, 40, 30],
                labels: ["Very Poor", "Satisfied", "Very Satisfied", "Poor"],
                legend: {
                    show: true,
                    offsetY: 50,
                },
                theme: {
                    monochrome: {
                        enabled: true,
                        color: '#ff4a00',
                    }
                },
                responsive: [{
                    breakpoint: 768,
                    options: {
                        chart: {
                            height: 320,

                        },
                        legend: {
                            position: 'bottom',
                            offsetY: 0,
                        }
                    }
                }]
            }
            var chart = new ApexCharts(document.querySelector("#satisfaction-chart"), options);
            chart.render();
        });
        // [ satisfaction-chart ] end

        // [ latest-scroll ] start
       
        var px = new PerfectScrollbar('.latest-scroll', {
            wheelSpeed: .5,
            swipeEasing: 0,
            wheelPropagation: 1,
            minScrollbarLength: 40,
        });
        // [ latest-scroll ] end
    });
</script>
@endsection