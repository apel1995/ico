@extends("layouts.admin.app")

@section('per_page_css')
    <link rel="stylesheet" href="{{ asset('assets/admin/libs/select2/select2.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/admin/libs/bootstrap-markdown/bootstrap-markdown.css') }}">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">
    <link rel="stylesheet" href="{{ asset('assets/admin/libs/photoswipe/photoswipe.css') }}">
@endsection


@section('body-content')
    <!-- [ Layout content ] Start -->
    <div class="layout-content">

        <!-- [ content ] Start -->
        <div class="container-fluid flex-grow-1 container-p-y">
            <h4 class="font-weight-bold py-3 mb-0">Ticket detail</h4>
            <div class="text-muted small mt-0 mb-4 d-block breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#"><i class="feather icon-home"></i></a></li>
                    <li class="breadcrumb-item"><a href="#!">Helpdesk</a></li>
                    <li class="breadcrumb-item active"><a href="#!">Ticket detail</a></li>
                </ol>
            </div>
            <div class="row">
                <div class="col-lg-8">
                    <div class="card">
                        <div class="card-header">
                            <h5><i class="feather icon-lock mr-1"></i>Private Ticket #1831786</h5>
                        </div>
                        <div class="card-body topic-name">
                            <div class="row align-items-center">
                                <div class="col-md-8">
                                    <h4 class="d-inline-block mb-0">{{ $details->subject }}</h4>
                                </div>
                                <div class="col-md-4">
                                    <div class="btn-star">
                                        <a href="#!" class="btn waves-effect waves-light btn-primary btn-sm">Mark as
                                            unread</a>
                                        <a href="#!"><i class="feather icon-star f-20 text-muted"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="bg-light-alt p-3">
                            <div class="row align-items-center">
                                <div class="col-md-12 btn-page">
                                    <button type="button" class="btn btn-secondary text-uppercase" data-toggle="modal"
                                        data-target="#comment"><i class="mr-2 feather icon-message-square"></i>Post a
                                        reply</button>
                                    <button type="button" class="btn btn-secondary text-uppercase"><i
                                            class="mr-2 feather icon-edit"></i>Post a Note</button>
                                    <button type="button" class="btn btn-secondary text-uppercase"><i
                                            class="mr-2 feather icon-user-check"></i>Customer Notes</button>
                                </div>
                            </div>
                        </div>
                        {{-- modal start --}}
                        {{-- ticket replay --}}
                        <div class="modal fade" id="comment">
                            <div class="modal-dialog">
                                <form class="modal-content"
                                    action="{{ url('admin/support_token/ticket_details/reply/store/' . $details->id) }}"
                                    method="post">
                                    @csrf
                                    <div class="modal-header">
                                        <h5 class="modal-title">Post A
                                            <span class="font-weight-light">Replay</span>
                                            <br>

                                        </h5>
                                        <button type="button" class="close" data-dismiss="modal"
                                            aria-label="Close">×</button>
                                    </div>
                                    <div class="modal-body">
                                        <div class="form-row">
                                            <div class="form-group col">
                                                <label class="form-label">Replay</label>
                                                <textarea id="myTextarea" class="form-control" name="cmt" value="" style="min-width: 100%"></textarea>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="modal-footer">


                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>


                                        <button type="submit" class="btn btn-primary">Replay</button>

                                    </div>
                                </form>
                            </div>
                        </div>
                        {{-- modal end --}}
                        {{-- customer messsage --}}

                        <div class="card-body hd-detail hdd-user border-bottom bg-light-alt">
                            <div class="row">
                                <div class="col-auto text-center">
                                    <img class="media-object wid-60 img-radius mb-2"
                                        src="{{ asset('assets/admin/img/user/avatar-4.jpg') }}"
                                        alt="Generic placeholder image ">
                                    <p>1 Ticket</p>
                                </div>
                                <div class="col">
                                    <div class="comment-top">
                                        <h4>User<span class="badge badge-secondary ml-2">{{ $details->customer }}</span> <small
                                                class="text-muted f-w-400"> Question</small></h4>
                                        <p>{{ $details->created_at->diffForHumans() }} on
                                            {{ $details->created_at->format('d, M') }}</p>
                                    </div>

                                    <div class="comment-content">
                                        {!! $details->Description !!}
                                    </div>

                                    <div id="photoswipe-example" class="row" itemscope
                                    itemtype="http://schema.org/ImageGallery">
                                        
                                @foreach(json_decode ($attachments) as $item => $filename )
                                {{-- <p>hello{{  $attach['filename'] }}</p> --}}
                                            <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject"
                                                class="col-sm-3">
                                                {{-- {{ base64_decode() }} --}}

                                                <a href="{{ asset('files/') }}" itemprop="contentUrl" data-size="1920x1280"><img
                                                        src="{{  asset('files/') }}" itemprop="thumbnail" alt="Image description"></a>
                                            </figure>
                                @endforeach            
                                      
                                    </div>
                                    <button type="button" class="btn waves-effect waves-light btn-outline-primary btn-sm"><i
                                            class="fas fa-thumbs-up mr-1"></i> Like</button>
                                </div>
                                <div class="col-auto pl-0 col-right">
                                    <div class="card-body text-center">
                                        <i class="feather icon-flag f-20 position-absolute"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {{-- comment --}}
                        @foreach ($cmt_info as $cmt)
                        <div class="card-body hd-detail hdd-admin border-bottom">
                            <div class="row">
                                <div class="col-auto text-center">
                                    <img class="media-object wid-60 img-radius mb-2"
                                        src="{{ asset('assets/admin/img/user/avatar-5.jpg') }}"
                                        alt="Generic placeholder image ">
                                    <p><i class="fas fa-thumbs-up mr-1 text-primary"></i>4</p>
                                </div>
                                <div class="col">
                                    <div class="comment-top">
                                        {{-- {{ auth('admin')->user()->name }} --}}
                                        <h4>Admin<span class="badge badge-secondary ml-2"> {{ $cmt->user_name }} </span><small
                                                class="text-muted f-w-400"> replied</small></h4>
                                        
                                            <p>{{ $cmt->updated_at->diffForHumans() }} on
                                                {{ $cmt->updated_at->format('d, M') }}</p>
                                       
                                    </div>
                                    <div class="comment-content">
                                       
                                                {!! $cmt->comment !!}
                                    </div>
                                    <pre>
                                               
                                            </pre>
                                </div>
                                {{-- <button type="button" class="" data-toggle="modal"
                                                data-target="#updatecmt"  data-user_id={{ $cmt->id }} onclick = "get_user_id(this)" >modify </button> --}}

                                <div class="col-auto pl-0 col-right">
                                    <div class="card-body text-center">
                                        <ul class="list-unstyled mb-0 edit-del">
                                            <li class="d-inline-block f-20 mr-1"><a type="button" class="" data-toggle="modal"
                                                 data-user_id={{ $cmt->id }} onclick = "get_user_id(this)"><i class="feather icon-edit-2 text-muted">
                                                      </i></a>
                                            </li>
                                            <form action = "{{ asset('admin/support_token/ticket_details/reply/delete/'.$cmt->id)}}">
                                                    <button type="button" onclick ="archiveFunction()">
                                                        delete
                                                    </button>
                                            </form>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>


                    @endforeach
                    
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="card hdd-right-inner">
                     <form action="{{ asset('admin/support_token/ticket_details/assign/'.$details->id) }}" method="post" id = "assign_details"  >
                         @csrf
                        <div class="card-header">
                            <h5>Ticket Details</h5>
                        </div>
                        <div class="card-body">
                            <div class="alert alert-success d-block text-center text-uppercase"><i
                                    class="feather icon-check-circle mr-2"></i>Verified Purchase</div>
                            <div class="select-block">
                                <input type ="hidden" id = "get_id" name = "get_id"/>
                                <select class="js-status-multiple col-sm-12" id = "status" name = "status">
                                    <option></option>
                                    <option value = "open">Open</option>
                                    <option value = "close">Close</option>
                                    <option value = "closeforever">Closed Forever</option>
                                </select>
                                <select class="js-assigned-multiple col-sm-12" id = "agent_name" name = "agent_name">
                                      <option></option>
                                  @foreach($support_agent as $agent)
                                      <option value="{{ $agent->name }}">{{ $agent->name }}</option>
                                  @endforeach
                                </select>
                                <select class="js-category-multiple col-sm-12" id = "category_name" name = "category_name">
                                       <option></option>
                                    @foreach($categories as $cat)
                                       <option value="{{ $cat->category }}">{{ $cat->category }}</option>
                                    @endforeach
                                
                                   
                                </select>
                            </div>
                        </div>
                        <ul class="list-group list-group-flush">
                            <li class="list-group-item">
                                <div class="media align-items-center">
                                    <label class="mb-0 wid-100">Customer</label>
                                    <div class="media-body">
                                        <p class="mb-0"><img
                                                src="{{ asset('assets/admin/img/user/avatar-5.jpg') }}" alt=""
                                                class="wid-20 rounded mr-1 img-fluid"><a href="#">{{ $details->customer }}</a></p>
                                    </div>
                                </div>
                            </li>
                            <li class="list-group-item">
                                <div class="media align-items-center">
                                    <label class="mb-0 wid-100">Contact</label>
                                    <div class="media-body">
                                        <p class="mb-0"><i class="feather icon-mail mr-1"></i><a
                                                href="#">mail@mail.com</a></p>
                                    </div>
                                </div>
                            </li>
                            <li class="list-group-item">
                                <div class="media align-items-center">
                                    <label class="mb-0 wid-100">Category</label>
                                    <div class="media-body">
                                        <p class="mb-0"><img
                                                src="{{ asset('assets/admin/img/ticket/p1.jpg') }}" alt=""
                                                class="wid-20 rounded mr-1 img-fluid"><a href="#">{{ $details->category }}</a></p>
                                    </div>
                                </div>
                            </li>
                            <li class="list-group-item">
                                <div class="media align-items-center">
                                    <label class="mb-0 wid-100">Assigned</label>
                                    <div class="media-body">
                                        <p class="mb-0"><img
                                                src="{{ asset('assets/admin/img/user/avatar-4.jpg') }}" alt=""
                                                class="wid-20 rounded mr-1 img-fluid"><a href="#">{{ $details->agent_name }}</a></p>
                                    </div>
                                </div>
                            </li>
                            <li class="list-group-item">
                                <div class="media align-items-center">
                                    <label class="mb-0 wid-100">Created</label>
                                    <div class="media-body">
                                        <p class="mb-0"><i class="feather icon-calendar mr-1"></i><label
                                                class="mb-0">Date  {{ $details->created_at->format('d, M, Y') }}</label></p>
                                    </div>
                                </div>
                            </li>
                        
                            <li class="list-group-item py-3">
                                <button type="submit" class="btn btn-primary"><i
                                        class="m-r-5 feather icon-thumbs-up" id = "assign_btn"></i>Assign</button>
                                   
                            
                            
                                @foreach($cmt_info as $cmt)
                                    <button type="button" data-parent_id={{ $cmt->parent_id }} onclick = "delete_token(this)" class="btn btn-danger"><i class="m-r-5 feather icon-trash-2"></i>Delete</button>
                                @break;
                               @endforeach
                            </li>
                        </ul>
                    </div>
                
                </div>
            </form>
            </div>
        </div>
        <!-- update modal start -->
        <!-- Modal -->
        {{-- comment modify --}}
        <div class="modal fade" id="updatecmt" tabindex="-1" role="dialog" aria-labelledby="exampleModalPreviewLabel" aria-hidden="true">
            <div class="modal-dialog">
                <form class="modal-content" action="{{ url('admin/support_token/ticket_details/reply/update/') }}" id="form-cmt-modify" method="post" >
                    @csrf
                    <div class="modal-header">
                        <h5 class="modal-title">Modify The
                            <span class="font-weight-light">Replay</span>
                            <br>

                        </h5>
                        <button type="button" class="close" data-dismiss="modal"
                            aria-label="Close">×</button>
                    </div>
                    <div class="modal-body">
                        <div class="form-row">
                            <div class="form-group col">
                                <label class="form-label">Replay</label>
                                <input type="hidden" id = "usr_id_modify" name="usr_id_modify"/>
                                <textarea id="tinymce-editor" class="form-control" name="cmt_modify" value="" style="min-width: 100%"></textarea>
                                <div class="clearfix"></div>
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">


                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>


                        <button type="submit" class="btn btn-primary">Replay</button>

                    </div>
                </form>
            </div>
        </div>  
        {{-- modal end --}}
                        
        <!-- [ content ] End -->
    @endsection

    @section('per_page_js')
        <script src="{{ asset('assets/admin/libs/select2/select2.js') }}"></script>

        <script type="text/javascript">
            function formatState(state) {
                if (!state.id) {
                    return state.text;
                }
                var baseUrl = "assets/img/product/";
                var $state = $(
                    '<span><img src="' + baseUrl + '/' + state.element.value.toLowerCase() +
                    '.jpg" class="img-flag wid-20"/> ' + state.text + '</span>'
                );
                return $state;
            };

            function formatState1(state) {
                if (!state.id) {
                    return state.text;
                }
                var baseUrl = "assets/img/user/";
                var $state = $(
                    '<span><img src="' + baseUrl + '/' + state.element.value.toLowerCase() +
                    '.jpg" class="img-flag wid-20"/> ' + state.text + '</span>'
                );
                return $state;
            };


            $(".js-status-multiple").select2({
                placeholder: "Ticket Status"

            });
            $(".js-assigned-multiple").select2({
                placeholder: "Assigned To",
                templateSelection: formatState1
            });
            $(".js-category-multiple").select2({
                placeholder: "Category",
                templateSelection: formatState
            });
        </script>


        <script>
            function replayModalCallback(data) {


                if (data.success) {
                    swal("Great!", "Replay Sucessfully!", "success");
                } else {
                    swal("Oops!", "Replay Unsuccessful!", "error");
                }
            }
        </script>
        <script src="{{ asset('assets/admin/libs/markdown/markdown.js') }}"></script>
        <script src="{{ asset('assets/admin/libs/bootstrap-markdown/bootstrap-markdown.js') }}"></script>
        <script
        type="text/javascript"
        src='https://cdn.tiny.cloud/1/no-api-key/tinymce/5/tinymce.min.js'
        referrerpolicy="origin">
    </script>
    <script type="text/javascript">
            tinymce.init({
            selector: '#myTextarea',
            width: 500,
            height: 300,
            plugins: [
            'advlist autolink link image lists charmap print preview hr anchor pagebreak',
            'searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking',
            'table emoticons template paste help'
            ],
            toolbar: 'undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | ' +
            'bullist numlist outdent indent | link image | print preview media fullscreen | ' +
            'forecolor backcolor emoticons | help',
            menu: {
            favs: {title: 'My Favorites', items: 'code visualaid | searchreplace | emoticons'}
            },
            menubar: 'favs file edit view insert format tools table help',
            content_css: 'css/content.css'
            });
    </script>
    <script>
              function get_user_id(e){
                    let obj = $(e);
                    //var email=obj.data('email');
                    var id=obj.data('user_id');
                     
                    $('#usr_id_modify').val(obj.data('user_id'));
                    // $('#delete_token').val(obj.data('parent_id'))
                    // console.log(obj.data('parent_id'));
                    $("#updatecmt").modal("show");
  
                    // var mod_text = document.getElementById("cmt-modify").value;
                    
                    // console.log(mod_text);
                };
                function delete_token(e){
                    let obj = $(e);
                    var p_id = obj.data('parent_id');
                    $.get('delete/' +p_id,function (data, textStatus, jqXHR) {
                    swal ("Delete Token Sucessfully");
                    window.location.href = "{{url('admin/support_token/ticket_list') }}";
                    });
                    console.log(p_id);

                }
           
        </script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
        <script>
            function archiveFunction() {
            event.preventDefault(); // prevent form submit
            var form = event.target.form; // storing the form
                    swal({
            title: "Are you sure?",
            text: "You want to delete this comment",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, Delete it!",
            cancelButtonText: "No, cancel please!",
            closeOnConfirm: false,
            closeOnCancel: false
            },
            function(isConfirm){
            if (isConfirm) {
                form.submit();          // submitting the form when user press yes
            } else {
                swal("Cancelled", "Your Comment is safe :)", "error");
            }
            });
            }
        </script>
@endsection
