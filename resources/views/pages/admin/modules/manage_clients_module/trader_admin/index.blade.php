@extends("layouts.admin.app")

@section('per_page_css')

<link rel="stylesheet" href="{{ asset('assets/admin/libs/datatables/datatables.css') }}">
<link rel="stylesheet" href="{{ asset('assets/admin/libs/select2/select2.css') }}">
<link rel="stylesheet" href="{{ asset('assets/admin/css/new-custom-datatable.css') }}">


<style>
       td.details-control {
        background: url({{ asset('assets/admin/img/datatable/plus.png') }}) no-repeat center center;
        cursor: pointer;
    }
    tr.details td.details-control {
        background: url({{ asset('assets/admin/img/datatable/minus.png') }}) no-repeat center center;
        
    }
    .account{
        text-transform: uppercase;
    }
    
</style>
@endsection

@section('body-content')

        <!-- [ content ] Start -->
        <div class="container-fluid flex-grow-1 container-p-y">
            <h4 class="font-weight-bold py-3 mb-0">Client List</h4>
            <div class="text-muted small mt-0 mb-4 d-block breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#"><i class="feather icon-home"></i></a></li>
                    <li class="breadcrumb-item" >Dashboard</li>
                    <li class="breadcrumb-item active" >Client List</li>
                </ol>
            </div> 

            {{-- filter report start --}}
            <div class="card">
                <div class="card-header">Filter Report</div>
                <div class="card-body">
                    <div class="row">
                        {{-- left side filter  start --}}
                        
                        <div class="col-md-3 col-sm-8">
                            <div class="select-block">
                                <select class="js-status-multiple col-sm-12">
                                    <option></option>
                                    <option>Open</option>
                                    <option>Close</option>
                                    <option>CLosed Forever</option>
                                </select>
                                <select class="js-assigned-multiple col-sm-12">
                                    <option></option>
                                    <option value="avatar-5">Jack Pall</option>
                                    <option value="avatar-4">Liza Mac</option>
                                    <option value="avatar-3">Lina Hop</option>
                                    <option value="avatar-2">Sam Hunk</option>
                                    <option value="avatar-1">Jhon White</option>
                                </select>
                                <select class="js-category-multiple col-sm-12">
                                    <option></option>
                                    <option value="prod-1">Able Admin</option>
                                    <option value="prod-2">Guru Dash</option>
                                    <option value="prod-3">Able pro</option>
                                    <option value="prod-4">Able Dash</option>
                                    <option value="prod-5">Dash Able</option>
                                </select>
                            </div>
                        </div>
                        {{-- left side filter  ends --}}


                        {{-- middle side filter  start --}}
                        

                        <div class="col-md-9 col-sm-8">
                            <div class="form-row">

                                 {{-- middle side filter  start --}}
                                <div class="form-group col-md-6 col-sm-8">
                                    <div class="row">
                                        <div class="col">
                                          <input type="text" class="form-control" placeholder="Min">  
                                        </div><span>-To-</span>
                                        <div class="col">
                                          <input type="text" class="form-control" placeholder="Max">
                                        </div>
                                      </div>

                                      <div class="form-group">
                                          <input type="text"  class="form-control" placeholder="Search By Wallet">
                                      </div>
                                      <div class="form-group">
                                          <input type="text"  class="form-control" placeholder="Search By Transaction  ID">
                                      </div>
                                </div>

                                 {{-- middle side filter ends --}}


                                 {{-- middle side filter ends --}}                         

                                <div class="form-group col-md-6 col-sm-8">
                                    <div class="row">
                                        <div class="col">
                                          <input type="date" class="form-control" placeholder="Min">  
                                        </div>
                                        <span>-To-</span>
                                        <div class="col">
                                          <input type="date" class="form-control" placeholder="Max">
                                        </div>

                                    </div>
                                    <div class="form-group">
                                        <input type="text"  class="form-control" placeholder="Search By Affiliate User">
                                    </div>
                                    <div class="form-group">
                                            <div class="form-group">
                                                <input type="text"  class="form-control" placeholder="Search By Email Or Name Or Country...">
                                            </div>
                                    </div>
                                </div>

                                {{-- middle side filter ends --}}
                            </div>
                        </div>                   
                    </div> 

                    {{-- button row  start --}}
                    
                    <div class="row">
                        <div class="col-md-3  clearfix">
                            <button  class="btn  btn-primary btn-block">Search</button>
                        </div>
                        <div class="col-md-3  clearfix">
                            <button  class="btn  btn-primary  btn-block">Export To CSV</button>
                        </div>
                        <div class="col-md-3 clearfix">
                            <button  class="btn  btn-primary  btn-block">Export With  Balance</button>
                        </div>
                        <div class="col-md-3  clearfix">
                            <button  class="btn  btn-primary  btn-block">Reset</button>
                        </div>
                    </div> 
                    {{-- button row  ends --}}

                </div>                    
            </div>
            {{-- filter report ends --}}

            <!-- DataTable within card -->

            <div class="card">
                <div class="card-header">
                    Client List
                  </div>
                <div class="card-body">                    
                    <div class="table-responsive">
                        <table id="report-table" class="table table-bordered table-striped mb-0 display">
                            <thead>
                                <tr>
                                    <th>View</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Phone</th>
                                    <th>Country</th>
                                    <th>Join Date</th>
                                    
                                </tr>
                              
                            </thead>
                        </table>
                        
                    </div>
                </div>
            </div>

        </div>
        <!-- [ content ] End -->

    @endsection

    @section('per_page_js')
    <!------per page js start----->
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    
    
    <script src="{{ asset('assets/admin/js/datatable/jquery.validate.js') }}"></script>
    <script src="{{ asset('assets/admin/js/datatable/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/admin/js/datatable/dataTables.bootstrap4.min.js') }}"></script>
    
    <script src="{{  asset('assets/admin/js/ajax_form_submit.js') }}"></script>
    <script src="{{ asset('assets/admin/js/custom-script.min.js') }}"></script>
    {{-- libs --}}
    <script src="{{ asset('assets/admin/libs/select2/select2.js') }}"></script>
    {{-- select  box  script start --}}

<script type="text/javascript">
    function formatState(state) {
        if (!state.id) {
            return state.text;
        }
        var baseUrl = "assets/img/product/";
        var $state = $(
            '<span><img src="' + baseUrl + '/' + state.element.value.toLowerCase() + '.jpg" class="img-flag wid-20"/> ' + state.text + '</span>'
        );
        return $state;
    };

    function formatState1(state) {
        if (!state.id) {
            return state.text;
        }
        var baseUrl = "assets/img/user/";
        var $state = $(
            '<span><img src="' + baseUrl + '/' + state.element.value.toLowerCase() + '.jpg" class="img-flag wid-20"/> ' + state.text + '</span>'
        );
        return $state;
    };

    $(".js-status-multiple").select2({
        placeholder: "--Search  By Finance--"

    });
    $(".js-assigned-multiple").select2({
        placeholder: "--Search By Category--",
        templateSelection: formatState1
    });
    $(".js-category-multiple").select2({
        placeholder: "--Search By Verification Status--",
        templateSelection: formatState
    });
</script>
{{-- select  box  script ends --}}

{{-- datatable script  start --}}
<script>
    // DataTable start    
    function format (data) {
            return '<b>Name:</b> '+data.name + '<br>Email :' + data.email ;
           
        }
    $(function(){    
     
       var  table  = $('#report-table').DataTable({
               
                processing: true,
                serverSide: true,
                ajax: "{{ route('manage.clients.data') }}",
                order: [[0, 'asc']],
                columns: [{
                    "class":      'details-control',
                    "orderable":      false,
                    "searchable":     false,
                    "data":           null,
                    "defaultContent": ''
                    },
                    {
                        data: 'name',
                        name: 'name'
                    },
                    {
                        data: 'email',
                        name: 'email'
                    },
                    {
                        data: 'phone',
                        name: 'phone'
                    },
                    {
                        data: 'country',
                        name: 'country'
                    },
                    {
                        data: 'join_date',
                        name: 'join_date'
                    },
               
                ]
            });    

        // Add event listener for opening and closing details          
       
            // Array to track the ids of the details displayed rows
            var detailRows = [];
         
            $('#report-table tbody').on( 'click', 'tr td.details-control', function(){
                var tr = $(this).closest('tr');
                var row = table.row( tr );
                var idx = $.inArray( tr.attr('id'), detailRows);
         
                if ( row.child.isShown()){
                    tr.removeClass('details');
                    row.child.hide();
         
                    // Remove from the 'open' array
                    detailRows.splice(idx, 1);
                }
                else {
                    tr.addClass( 'details' );
                    row.child( format(row.data())).show();
         
                    // Add to the 'open' array
                    if (idx === -1) {
                        detailRows.push(tr.attr('id'));
                    }
                }
            });
         
            // On each draw, loop over the `detailRows` array and show any child rows
            table.on( 'draw', function(){
                $.each( detailRows, function (i, id){
                    $('#'+id+' td.details-control').trigger('click');
                });
            });
    
            $('#filterBtn').click(function (e) {
                $(this).html("<i class='fa fa-refresh fa-spin fa-1x fa-fw'></i>");
                table.draw();
            });
    
        });
        
    </script>

{{-- datatable script  ends --}}

@endsection
<!------per page js ends----->