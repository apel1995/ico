<!DOCTYPE html>

<html lang="en" class="default-style layout-fixed layout-navbar-fixed">

<head>
    <title>Admin Panel</title>

    @include('includes.user.meta')
    @include('includes.user.css')
</head>

<body>
    <!-- [ Preloader ] Start -->
    <div class="page-loader">
        <div class="bg-primary"></div>
    </div>
    <!-- [ Preloader ] End -->

    <!-- [ Layout wrapper ] Start -->
    <div class="layout-wrapper layout-2">
        <div class="layout-inner">
          @include('includes.user.leftsidebar')
            <!-- [ Layout container ] Start -->
            <div class="layout-container">
             @include('includes.user.navbar')

                <!-- [ Layout content ] Start -->
                <div class="layout-content">

                   @yield('body-content')

                 @include('includes.user.footer')
                </div>
                <!-- [ Layout content ] Start -->
            </div>
            <!-- [ Layout container ] End -->
        </div>
        <!-- Overlay -->
        <div class="layout-overlay layout-sidenav-toggle"></div>
    </div>
    <!-- [ Layout wrapper] End -->

    {{-- @include('includes.includes.welcome_modal') --}}
    @include('includes.user.script')
</body>

</html>
